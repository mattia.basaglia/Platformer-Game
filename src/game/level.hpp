/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef LEVEL_HPP
#define LEVEL_HPP

#include <string>
#include <SFML/Graphics/Drawable.hpp>
#include "game/layer.hpp"
#include "game/quad_tree.hpp"
#include "misc/color.hpp"
#include "misc/object_source.hpp"

namespace game {

class Object;

/**
 * \brief Abstract base class for levels
 */
class Level : public sf::Drawable, public misc::Object_Source
{
    class Level_Private *d;

    PROPERTY_MAGIC_INHERIT(Level,misc::Object_Source)

protected:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    void for_each_object( const std::function<void(const std::string& name,misc::Mirror*)>& func ) const override;

    misc::Mirror* get_child(const std::string& name) const override;
public:
    /**
     * \brief Constructor
     * \param id    Level identifier (path relative to data)
     */
    Level(const std::string& id);
    Level(const Level&) = delete;
    virtual ~Level();

    /**
     * \brief Level id
     *
     * File name relative to the data directory
     */
    std::string id() const;

    /**
     * \brief Play background music
     */
    void play_music();
    /**
     * \brief Add music to the list of background music tracks
     * \param id    music file id (path relative to data)
     */
    void add_music(const std::string& id);

    /**
     * \brief Pause current music
     */
    void pause_music();

    /**
     * \brief Execute a logic step
     */
    virtual void step();

    /**
     * \brief Enter the level
     * \param from  id of the level we are coming from
     */
    virtual void load(const std::string& from);

    /**
     * \brief Exit the level
     */
    virtual void unload();

    /**
     * \brief Add an object to the level
     * \param object    Object to be added (takes ownership)
     * \param layer     Owner layer ( if nullptr the first available layer is used )
     * \pre \c layer is euther \b nullptr or a pointer to a layer owned by the level
     */
    void add_object(Object* object,Object_Layer* layer=nullptr);

    /**
     * \brief Add an object to the level
     * \param object    Object to be added (takes ownership)
     * \param parent    Object that spawned the new object
     */
    void add_object(Object* object,Object* parent);

    /**
     * \brief Removes an object from the level and deletes it
     * \pre object  Must be in the level
     */
    void remove_object(Object* object);

    /**
     * \brief Update data structures when object moves
     */
    void update_object(Object* object);

    /**
     * \brief Get list with all the objects
     */
    const std::vector<Object*>& objects() const;

    /**
     * \brief Get list with all the objects of the given type
     */
    std::vector<Object*> objects(const std::string& type) const;
    /**
     * \brief Get list with all the objects of the given type (or derived types
     */
    std::vector<Object*> objects(int type) const;

    /**
     * \brief Get object by name
     * \note  If there are multiple objects with the same name, the first
     *  to be found is returned
     */
    game::Object* find_object(const std::string& name) const;

    /**
     * \brief Get collision-checking data structure
     */
    const Quad_Tree& collision() const;

    /**
     * \brief Create a layer from a tiled file
     * \param id    File id (path relative to data)
     * \note The map should have a property called \b leveltype set to an appropriate value
     */
     static Level* create(const std::string& id);

    /**
     * \brief Register a level type for the factory
     */
    static void register_level_type(const std::string& name, const std::function<Level*(const std::string&)> &create);

    /**
     * \brief Get level size
     */
    geo::Size size() const;

    const misc::Color& background_color() const;
};

/**
 * \brief Register level class for level factory
 * \param Class Class derived from game::Level
 * \param name  Unique string used to identify the level type
 * \note  Use in the implementation file
 */
#define REGISTER_LEVEL(Class,name) \
    struct REGISTER_LEVEL_##Class { REGISTER_LEVEL_##Class() { \
        game::Level::register_level_type(name,[](const std::string&id){return new Class(id);}); \
    } } static REGISTER_LEVEL_OBJECT_##Class;



} // namespace game


#endif // LEVEL_HPP
