/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "video.hpp"
#include "game/game_manager.hpp"
#include "media/media_manager.hpp"
#include "game/user_settings.hpp"
#include "game/level.hpp"
#include "misc/geometry_convert.hpp"

using namespace game;

DECLARE_PROPERTY(Video,title,title,set_title)
DECLARE_PROPERTY_GETTER(Video,shaders,shaders)
DECLARE_PROPERTY_GET_FUNCTOR(Video,width,[](const Video&vid){return misc::Value((long)vid.window().getSize().x);})
DECLARE_PROPERTY_GET_FUNCTOR(Video,height,[](const Video&vid){return misc::Value((long)vid.window().getSize().y);})


DECLARE_METHOD_VOID_NOARG(Video,restart)
DECLARE_METHOD_FUNCTOR(Video,shader_param,[](Video& video, const eval::Context::Arguments& args) -> misc::Value
{
        if ( args.size() < 3 )
            return misc::Value();

        sf::Shader* shader = media::Media_Manager::instance().shader((std::string)args[0]);

        if ( !shader )
            return misc::Value();

        std::string param = (std::string)args[1];

        if ( args.size() == 3 ) // shader param value
        {
            if ( ((std::string)args[2])[0] == '#' )
                shader->setParameter(param,misc::Color((std::string)args[2])); // vec4
            else
                shader->setParameter(param,args[2].to<float>()); // float
        }
        else if ( args.size() == 4 )
                shader->setParameter(param,args[2].to<float>(),args[3].to<float>()); // vec2
        else if ( args.size() == 5 )
                shader->setParameter(param,args[2].to<float>(),args[3].to<float>(),args[4].to<float>()); // vec3
        else if ( args.size() == 6 )
                shader->setParameter(param,args[2].to<float>(),args[3].to<float>(),args[4].to<float>(),args[5].to<float>()); // vec4

        return misc::Value();
})
DECLARE_METHOD_FUNCTOR(Video,shader,[](Video& video, const eval::Context::Arguments& args) -> misc::Value
{
    std::vector<std::string> shads;
    for ( const auto& a : args )
        shads.push_back((std::string)a);
    media::Media_Manager::instance().set_shaders(shads);
    return misc::Value();
})
DECLARE_METHOD_FUNCTOR(Video,screen_x,[](Video& video, const eval::Context::Arguments& args) -> misc::Value
{
    long x = args.size() < 1 ? 0 : (long)args[0];
    long y = args.size() < 2 ? 0 : (long)args[1];
    return (long)video.map_to_pixel(x,y).x;
})
DECLARE_METHOD_FUNCTOR(Video,screen_y,[](Video& video, const eval::Context::Arguments& args) -> misc::Value
{
    long x = args.size() < 1 ? 0 : (long)args[0];
    long y = args.size() < 2 ? 0 : (long)args[1];
    return (long)video.map_to_pixel(x,y).y;
})
DECLARE_METHOD_FUNCTOR(Video,screen_length,[](Video& video, const eval::Context::Arguments& args) -> misc::Value
{
    float w = args.size() < 1 ? 0 : args[0].to<float>();
    geo::Line line (video.map_to_pixel(0,0),
                    video.map_to_pixel(geo::Polar_Vector(w,0).point()) );
    return misc::Value::from(line.length());
})

/// \pimpl{Video}
struct game::Video_Private
{
    sf::RenderTexture           render_buffer[2];///< Render buffers
    sf::Sprite                  render_sprite;   ///< Render buffer sprite
    sf::RenderWindow            render_window;   ///< Render target
    bool                        use_shaders=true;///< Whether to use shaders
	Camera                      camera;          ///< Part of the level to be displayed
	std::string                 title;           ///< Window Title
	sf::View                    ui_view;         ///< View used for the UI
};

Video::Video()
    : d ( new Video_Private )
{}
Video::~Video()
{
    delete d;
}

void Video::restart()
{
    int w = Game_Manager::instance().settings().get("video.width",800);
    int h = Game_Manager::instance().settings().get("video.height",600);
    d->render_window.create(sf::VideoMode(w,h),d->title,sf::Style::Close);
    d->render_buffer[0].create(w,h);
    d->render_buffer[1].create(w,h);
    d->render_sprite.setTexture(d->render_buffer[0].getTexture(),true);
    if ( int framerate_limit = Game_Manager::instance().settings().get("video.framerate_limit",0) )
        d->render_window.setFramerateLimit(framerate_limit);
    d->use_shaders = Game_Manager::instance().settings().get("video.shaders",sf::Shader::isAvailable());
}

void Video::init()
{
    restart();
    d->camera = d->render_window.getDefaultView();
}


Camera& Video::camera() const
{
    return d->camera;
}


void Video::render(Level* current_level)
{
    sf::RenderTarget* target = &d->render_window;

    std::vector<sf::Shader*> shaders;
    if ( d->use_shaders )
    {
        shaders = media::Media_Manager::instance().active_shaders();
        if ( !shaders.empty() )
            target = &d->render_buffer[0];
    }

    target->clear(current_level->background_color());
    target->setView(d->camera);
    target->draw(*current_level);
    target->setView(target->getDefaultView());


    if ( !shaders.empty() )
    {

        d->render_buffer[0].display();

        d->render_sprite.setTexture(d->render_buffer[0].getTexture());
        for ( unsigned i = 0; i < shaders.size(); i++ )
        {
            d->render_buffer[(i+1)%2].draw(d->render_sprite,shaders[i]);
            d->render_buffer[(i+1)%2].display();
            d->render_sprite.setTexture(d->render_buffer[(i+1)%2].getTexture());
        }

        d->render_window.draw(d->render_sprite);
    }
}


geo::Point Video::map_from_pixel(long x, long y) const
{
    return geo::conv::point( d->render_window.mapPixelToCoords (sf::Vector2i(x, y), d->camera ) );
}

geo::Point Video::map_to_pixel(long x, long y) const
{
    return geo::conv::point( d->render_window.mapCoordsToPixel(sf::Vector2f(x,y), d->camera) );
}

geo::Point Video::map_to_pixel(const geo::Point& p) const
{
    return map_to_pixel(p.x,p.y);
}

void Video::step()
{
    d->camera.step();
}

sf::RenderWindow& Video::window() const
{
    return d->render_window;
}

const std::string& Video::title() const
{
    return d->title;
}

void Video::set_title(const std::string& title)
{
    d->title = title;
    d->render_window.setTitle(title);
}

bool Video::shaders() const
{
    return d->use_shaders && sf::Shader::isAvailable();
}


void Video::begin_ui()
{
    d->ui_view.setSize(d->camera.getSize());
    d->ui_view.setCenter(d->camera.getSize().x/2,d->camera.getSize().y/2);
    d->render_window.setView(d->ui_view);
}

void Video::end_ui()
{
    d->render_window.setView(d->render_window.getDefaultView());
}
