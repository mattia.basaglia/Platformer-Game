/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef GAME_LEVEL_HPP
#define GAME_LEVEL_HPP

#include "game/level.hpp"

namespace game {

class Object;

/**
 * \brief A playable level
 */
class Game_Level : public Level
{
public:
    Game_Level(const std::string& id);
    void load(const std::string& from) override;
};



/**
 * \brief Map level
 *
 * Has the same structure but a different logic from a normal game layer
 * \todo move to own file
 */
class Map_Level : public Game_Level
{
public:
    Map_Level(const std::string& id);
    void step() override;
    void load(const std::string& from) override;
};


} // namespace game
#endif // GAME_LEVEL_HPP
