/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "game/layer.hpp"
#include "game/game_manager.hpp"
#include "config.hpp"
#include "media/media_manager.hpp"
#include "misc/string_utils.hpp"
#include "eval/parser.hpp"
#include <cmath>
#include "misc/geometry_convert.hpp"
#include "eval/script_snippet.hpp"

using namespace game;

DECLARE_PROPERTY(Layer,condition,condition,set_condition)
DECLARE_PROPERTY(Layer,parallax,parallax_speed,set_parallax_speed)

/// \pimpl{Layer}
struct game::Layer_Private
{
    std::string name;
    bool visible = true;
    double parallax_speed = 0;
    eval::Script_Snippet condition;

    Layer_Private(const std::string& name)
        : name ( name )
    {}
};

Layer::Layer(const std::string& name)
    : d(new Layer_Private(name))
{}

PIMPL_STRUCTORS_INH(Layer,misc::Object_Source)

const std::string& Layer::name() const
{
    return d->name;
}

void Layer::load(const std::string&)
{
    d->visible = d->condition.empty() ||
                 d->condition.evaluate(Game_Manager::instance().get_context());
}

bool Layer::visible() const
{
    return d->visible;
}

geo::Point Layer::parallax_offset() const
{
    return Game_Manager::instance().video().camera().offset() * d->parallax_speed;
}

double Layer::parallax_speed() const
{
    return d->parallax_speed;
}

void Layer::set_parallax_speed(double speed)
{
    d->parallax_speed = speed;
}

const std::string& Layer::condition() const
{
    return d->condition.get_source();
}

void Layer::set_condition ( const std::string& cond )
{
    d->condition.set_source(cond);
}

/// \pimpl{Tile_Layer}
struct game::Tile_Layer_Private
{
    int width;
    int height;
    int tile_width;
    int tile_height;
    std::map<media::Tile_Position,sf::Sprite> tiles;
    std::shared_ptr<sf::RenderTexture> baked;
    sf::Sprite baked_sprite;

    Tile_Layer_Private(int width, int height, int tile_width, int tile_height)
        : width(width), height(height), tile_width(tile_width), tile_height(tile_height)
    {}
};

Tile_Layer::Tile_Layer(const std::string& name,
                       int width, int height, int tile_width, int tile_height)
    : Layer(name), d(new Tile_Layer_Private(width,height,tile_width,tile_height) )
{}
PIMPL_STRUCTORS_INH(Tile_Layer,Layer)

void Tile_Layer::step()
{
    if ( d->baked )
    {
        auto r = Game_Manager::instance().video().camera().game_area();
        auto p = parallax_offset();
        r.translate(-p);
        sf::IntRect ir(r.x,r.y,r.width+1,r.height+1);
        d->baked_sprite.setTextureRect(ir);
        d->baked_sprite.setPosition(ir.left+p.x,ir.top+p.y);
    }
}

void Tile_Layer::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    if ( d->baked )
    {
        target.draw(d->baked_sprite,states);
    }
    else
    {
        states.transform.translate( geo::conv::point(parallax_offset()) );
        for ( auto p: d->tiles )
            target.draw(p.second,states);
    }
}

void Tile_Layer::bake()
{
    if ( !d->baked )
    {
        sf::RenderTexture* bakery = new sf::RenderTexture;
        bakery->create(d->width*d->tile_width,d->height*d->tile_height);
        bakery->clear(misc::Color(0,0,0,0));
        for ( auto p: d->tiles )
            bakery->draw(p.second,sf::RenderStates::Default);
        bakery->display();
        d->baked_sprite.setTexture(bakery->getTexture());
        d->baked.reset(bakery);
        d->tiles.clear(); /// \todo clear?
    }
}

void Tile_Layer::load(const std::string& from)
{
    Layer::load(from);
    //if ( property("bake") )
        bake();
}

void Tile_Layer::set_tile(media::Tile_Position position, sf::Sprite sprite)
{
    sprite.setPosition(sf::Vector2f(
        media::tile_x(position)*d->tile_width,
        media::tile_y(position)*d->tile_height
    ));
    /// \todo maybe Tile_Position can be removed altogether since it's not used to
    /// perform any sort of optimization
    /// (replace it with a geo::Point for "position" in this function and fix the caller)
    /// Note that by doing that it would need checking for tiles in the same position
    /// But it should never occur so it's not a big deal
    d->tiles[position] = sprite;
}

DECLARE_PROPERTY_SETTER(Image_Layer,tile_x,set_tiling_x)
DECLARE_PROPERTY_SETTER(Image_Layer,tile_y,set_tiling_y)

/// \pimpl{Image_Layer}
struct game::Image_Layer_Private
{
    /**
     * \brief Way of tiling the image
     */
    enum Tiling
    {
        NO     = 0,///< Don't tile (single image)
        BEFORE = 1,///< Tile upwards/leftwards
        AFTER  = 2,///< Tile downwards/rightwards
        BOTH   = 3 ///< Tile in both directions
    };

    geo::Point pos;
    sf::Sprite sprite;
    Tiling tiling_x = NO;
    Tiling tiling_y = NO;

    Image_Layer_Private(const std::string& image_id, const geo::Point& pos)
        : pos(pos)
        {
            sprite.setTexture(media::Media_Manager::instance().image(image_id));
            sprite.setPosition(pos.x,pos.y);
        }

    /**
     * \brief Draw a y-tiled slice
     */
    void draw_slice(sf::RenderTarget& target, sf::RenderStates states,
                    const geo::Point& origin, const geo::Rectangle& cam_rect)
    {

        geo::Rectangle rect (origin.x,origin.y, sprite.getLocalBounds().width,sprite.getLocalBounds().height);

        sprite.setPosition(origin.x,origin.y);
        target.draw(sprite,states);

        if ( tiling_y & BEFORE )
        {
            while ( rect.top() > cam_rect.top() )
            {
                rect.y -= rect.height;
                sprite.setPosition(rect.x,rect.y);
                target.draw(sprite,states);
            }
        }

        if ( tiling_y & AFTER )
        {
            rect.y = origin.y;
            while ( rect.bottom() < cam_rect.bottom() )
            {
                rect.y += rect.height;
                sprite.setPosition(rect.x,rect.y);
                target.draw(sprite,states);
            }
        }
    }


    /**
     * \brief Convert a string into a tiling value
     *
     * Conversions:
     *  * NO
     *      * (empty string)
     *      * no
     *      * false
     *      * 0
     *  * BEFORE
     *      * up
     *      * left
     *  * AFTER
     *      * down
     *      * right
     *  * BOTH
     *      * both
     *      * yes
     *      * true
     *      * 1
     */
    Tiling string2tiling ( const std::string& tiling )
    {
        if ( tiling == "up" || tiling == "left" )
            return BEFORE;
        if ( tiling == "down" || tiling == "right" )
            return AFTER;
        if ( tiling == "1" || tiling == "yes" || tiling == "true" || tiling == "both" )
            return BOTH;
        return NO;
    }
};

Image_Layer::Image_Layer(const std::string& name, const std::string& image_id, const geo::Point& pos )
    : Layer(name), d ( new Image_Layer_Private(image_id,pos))
{}

PIMPL_STRUCTORS_INH(Image_Layer,Layer)

void Image_Layer::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    geo::Point origin = d->pos + parallax_offset();
    geo::Rectangle rect (origin.x,origin.y, d->sprite.getLocalBounds().width,d->sprite.getLocalBounds().height);
    geo::Rectangle cam_rect = Game_Manager::instance().video().camera().game_area();

    d->draw_slice(target,states,origin,cam_rect);

    if ( d->tiling_x & Image_Layer_Private::BEFORE )
    {
        while ( rect.left() > cam_rect.left() )
        {
            rect.x -= rect.width;
            d->draw_slice(target,states,rect.top_left(),cam_rect);
        }
    }

    if ( d->tiling_x & Image_Layer_Private::AFTER )
    {
        rect.x = origin.x;
        while ( rect.right() < cam_rect.right() )
        {
            rect.x += rect.width;
            d->draw_slice(target,states,rect.top_left(),cam_rect);
        }
    }
}

void Image_Layer::set_tiling_x(const std::string& tiling)
{
    d->tiling_x = d->string2tiling(tiling);
}

void Image_Layer::set_tiling_y(const std::string& tiling)
{
    d->tiling_y = d->string2tiling(tiling);
}

/// \pimpl{Object_Layer}
struct game::Object_Layer_Private
{
    std::list<game::Object*> objects;
};

Object_Layer::Object_Layer(const std::string& name)
    : Layer(name), d ( new Object_Layer_Private() )
{}

PIMPL_STRUCTORS_INH(Object_Layer,Layer)

Object* Object_Layer::get_child(const std::string& name) const
{
    for ( auto o : d->objects )
        if ( o->name() == name )
            return o;
    return nullptr;
}

void Object_Layer::add_object(game::Object* object)
{
    if ( object )
        d->objects.push_back(object);
}

void Object_Layer::remove_object(Object* object)
{
    auto it = std::find(d->objects.begin(),d->objects.end(),object);
    if ( it != d->objects.end() )
        d->objects.erase(it);
}

void Object_Layer::step()
{
    for ( auto o : d->objects )
        o->step();
}

void Object_Layer::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    /// \todo parallax?
    for ( auto o : d->objects )
        target.draw(*o,states);
}

const std::list<Object*>& Object_Layer::objects() const
{
    return d->objects;
}

void Object_Layer::clear()
{
    for ( auto o : d->objects )
        delete o;
    d->objects.clear();
}

void Object_Layer::for_each_object( const std::function<void(const std::string& name,misc::Mirror*)>& func ) const
{
    for ( auto o : d->objects )
        func(o->name(),o);
}

