/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "game/level.hpp"

#include "game/layer.hpp"
#include "game/game_manager.hpp"
#include "media/media_manager.hpp"
#include "media/tileset.hpp"
#include "misc/logger.hpp"
#include "misc/ptree_utils.hpp"
#include "eval/parser.hpp"

#include "config.hpp"

#include <list>

#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include <SFML/Audio/Music.hpp>
#include <SFML/Graphics.hpp>


using namespace game;

namespace{
    DECLARE_PROPERTY_GETTER(Level,id,id)
}

/// \pimpl{Level}
struct game::Level_Private
{
    static std::map<std::string,std::function<Level*(const std::string&)>>& factory()
    {
        static std::map<std::string,std::function<Level*(const std::string&)>> singleton;
        return singleton;
    }

    std::string                      id;       ///< Level identifier (path relative to data)
    std::vector<std::string>         playlist; ///< Playlist of background music
    std::vector<std::string>::iterator musindex;///< Index of the currently playing music
    sf::Music                        music;    ///< Object that plays the background music
    unsigned map_width  = 0;
    unsigned map_height = 0;
    unsigned tile_width = 0;
    unsigned tile_height= 0;
    misc::Properties                properties;     ///< Misc properties
    std::vector<Layer*>             layers;         ///< All layers
    std::map<int,media::Tileset*>   indices;        ///< Indices used when creating layers
    Quad_Tree                       qtree;          ///< Collision tree
    std::list<Object*>              dead_objects;   ///< Objects to be removed
    misc::Color                     background_color;///< Background color
    eval::Parse_Tree*               on_load=nullptr; ///< Code to execute at startup
    eval::Parse_Tree*               on_finish=nullptr; ///< Code to execute at the end

    /**
     * \brief Get a specific tile for the level
     * \param index     global tile index
     * \see set_tileset_index()
     * \return Sprite corresponding to the given tile
     */
    sf::Sprite get_tile(int index)
    {
        auto next = indices.begin();
        auto current = next;
        ++next;
        while(next != indices.end() && next->first <= index)
        {
            ++next;
            ++current;
        }
        return current->second->get_tile(index-current->first);

    }

    /**
     * \brief Get a specific tileset for the level
     * \param index     global tile index
     * \return Tileset for that index
     */
    media::Tileset* get_tileset(long& index)
    {
        auto next = indices.begin();
        auto current = next;
        ++next;
        while(next != indices.end() && next->first <= index)
        {
            ++next;
            ++current;
        }
        if ( current == indices.end() || index < current->first )
            return nullptr;
        index -= current->first;
        return current->second;
    }

    Level_Private(const std::string& id) : id (id), qtree(geo::Rectangle()) {}
};

Level::Level(const std::string& id)
    : d(new Level_Private(id))
{
}

Level::~Level()
{
    for(auto l : d->layers)
    {
        l->clear();
        delete l;
    }
    delete d->on_load;
    delete d->on_finish;
    delete d;
}

std::string Level::id() const
{
    return d->id;
}

void Level::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    for(auto l : d->layers)
    {
        if ( l->visible() )
            target.draw(*l,states);
    }
#ifdef DEBUG
    if ( Game_Manager::instance().storage()["debug"] )
        d->qtree.draw(target,states);
#endif // DEBUG
}

void Level::pause_music()
{
    if ( d->music.getStatus() == sf::SoundSource::Playing )
    {
        d->music.pause();
    }
}

void Level::play_music()
{
    if ( d->music.getStatus() == sf::SoundSource::Paused )
    {
        d->music.play();
    }
    else if ( !d->playlist.empty() && d->music.getStatus() != sf::SoundSource::Playing )
    {
        using namespace boost::filesystem;
        Logger() << "Background music:" << basename(*d->musindex) <<
            '('+basename(path(*d->musindex).parent_path())+')';

        d->music.openFromFile(*d->musindex);
        d->music.play();

        ++d->musindex;
        if ( d->musindex == d->playlist.end() )
            d->musindex = d->playlist.begin();
    }
}

void Level::add_music(const std::string& id)
{
    std::string mus = media::Media_Manager::instance().data_file(id);
    if ( ! mus.empty() )
    {
        d->playlist.push_back(mus);
        if ( d->playlist.size() == 1 )
            d->musindex = d->playlist.begin();
    }
}


const std::vector<Object*>& Level::objects() const
{
    return d->qtree.get_objects();
}

std::vector<Object*> Level::objects(const std::string& type) const
{
    std::vector<Object*> list;
    for ( auto o : d->qtree )
    {
        if ( o->type() == type )
            list.push_back(o);
    }
    return list;
}

std::vector<Object*> Level::objects(int type) const
{
    std::vector<Object*> list;
    for ( Object* o : d->qtree )
    {
        if ( o->has_type_id(type) )
            list.push_back(o);
    }
    return list;
}

game::Object* Level::find_object(const std::string& name) const
{
    for ( auto o : d->qtree )
        if ( o->name() == name )
            return o;
    // search all objects, there may be some not part of the qtree
    for ( auto l : d->layers )
        if ( Object_Layer* ol = dynamic_cast<Object_Layer*>(l) )
            if ( Mirror* o = ol->find_child(name) )
                return reinterpret_cast<Object*>(o);

    return nullptr;
}

misc::Mirror* Level::get_child(const std::string& name) const
{
    if ( game::Object* o = find_object(name) )
        return o;

    std::string root, item;
    misc::split_identifier(name,root,item);
    if ( root == "layer" && !item.empty() )
        for ( auto l : d->layers )
            if ( l->name() == item )
                return l;

    return nullptr;
}


void Level::step()
{
    play_music();

    for(auto l : d->layers)
    {
        l->step();
    }

    if ( ! d->dead_objects.empty() )
    {
        d->dead_objects.sort();
        d->dead_objects.unique();
        for ( auto it = d->dead_objects.begin(); it != d->dead_objects.end(); )
        {
            d->qtree.remove(*it);
            (*it)->layer()->remove_object(*it);
            if ( (*it)->can_destroy() )
            {
                delete (*it);
                it = d->dead_objects.erase(it);
            }
            else
                ++it;
        }
    }
}

void Level::unload()
{
    for(auto o : objects())
    {
        o->trigger_event("level_end");
    }
    d->on_finish && d->on_finish->evaluate(Game_Manager::instance().get_context());
}

void Level::load(const std::string& from)
{
    d->on_load && d->on_load->evaluate(Game_Manager::instance().get_context());
    for(auto l : d->layers)
    {
        l->load(from);
    }
    for ( auto o : d->qtree )
        d->qtree.update(o);
}

void Level::register_level_type(const std::string& name, const std::function<Level*(const std::string&)> &create)
{
    Level_Private::factory()[name] = create;
}


void Level::add_object(Object* object,Object_Layer* layer)
{
    if ( layer == nullptr )
    {
        for ( auto rit = d->layers.rbegin(); rit != d->layers.rend(); ++rit )
            if ( (layer = dynamic_cast<Object_Layer*>(*rit)) )
                break;

        if ( layer == nullptr )
        {
            layer = new Object_Layer("__automatic_layer__");
            d->layers.push_back(layer);
        }
    }

    object->set_level(this);
    object->set_layer(layer);
    layer->add_object(object);
    d->qtree.insert(object);
}

void Level::add_object(Object* object,Object* parent)
{
    add_object(object,parent?parent->layer():nullptr);
}


const Quad_Tree& Level::collision() const
{
    return d->qtree;
}


void Level::update_object(Object* object)
{
    d->qtree.update(object);
}


void Level::remove_object(Object* object)
{
    d->dead_objects.push_back(object);
    d->qtree.remove(object);
}


geo::Size Level::size() const
{
    return d->qtree.get_area().size();
}

const misc::Color& Level::background_color() const
{
    return d->background_color;
}

void Level::for_each_object( const std::function<void(const std::string& name,misc::Mirror*)>& func ) const
{
    for ( auto o : objects() )
    {
        func(o->name(),o);
    }

    for ( auto o : d->layers )
        func("layer."+o->name(),o);
}

Level* Level::create(const std::string& id)
{
    using namespace boost::property_tree;
    ptree pt;
    Level* lev = nullptr;

    try {

        read_xml(media::Media_Manager::instance().data_file(id),pt,xml_parser::trim_whitespace|xml_parser::no_comments);

        // Create a level of the proper type
        std::string level_type;
        for ( auto p : pt.get_child("map.properties") )
        {
            if ( p.first == "property" && p.second.get<std::string>("<xmlattr>.name","") == "leveltype" )
            {
                level_type = p.second.get("<xmlattr>.value","");
                break;
            }
        }

        if ( level_type.empty() || !Level_Private::factory().count(level_type) )
        {
            Logger::error("Unknown leveltype \""+level_type+"\" in level "+id);
            lev = new Level(id);
        }
        else
            lev = Level_Private::factory()[level_type](id);


        std::string base_path = boost::filesystem::path(
            media::Media_Manager::instance().data_file(id)).parent_path().string();

        // Basic tile info
        lev->d->map_width = pt.get<unsigned>("map.<xmlattr>.width");
        lev->d->map_height = pt.get<unsigned>("map.<xmlattr>.height");
        lev->d->tile_width = pt.get<unsigned>("map.<xmlattr>.tilewidth");
        lev->d->tile_height = pt.get<unsigned>("map.<xmlattr>.tileheight");
        lev->d->qtree.clear(geo::Rectangle(0,0,
                           lev->d->map_width*lev->d->tile_width,
                           lev->d->map_height*lev->d->tile_height));

        misc::get_properties(pt.get_child("map"),lev->d->properties);
        lev->d->background_color = (std::string)pt.get("map.<xmlattr>.backgroundcolor","#000000");


        for ( auto p : pt.get_child("map.properties") )
        {
            if ( p.first == "property" )
            {
                std::string name = p.second.get("<xmlattr>.name","");
                if ( name == "music" )
                    lev->add_music(p.second.get("<xmlattr>.value",""));
                else if ( name == "on_load" )
                    lev->d->on_load = eval::Parser().compile(p.second.get("<xmlattr>.value",""));
                else if ( name == "on_finish" )
                    lev->d->on_finish = eval::Parser().compile(p.second.get("<xmlattr>.value",""));
            }
        }

        // Load tilesets
        for ( auto i : pt.get_child("map") )
            if ( i.first == "tileset" )
            {
                int index = i.second.get("<xmlattr>.firstgid",0);
                media::Tileset* ts = media::Media_Manager::instance()
                    .load_tileset(i.second,base_path);
                if ( ts )
                    lev->d->indices[index] = ts;

            }

        // Load layers
        for ( auto i : pt.get_child("map") )
        {
            Layer* layer = nullptr;
            std::string layer_name = i.second.get("<xmlattr>.name","");
            if ( i.first == "layer" )
            {
                if ( Game_Manager::instance().has_object_library(layer_name) )
                {
                    Object_Layer* ol = new Object_Layer(layer_name);
                    layer = ol;

                    unsigned x = 0;
                    unsigned y = 0;
                    for ( auto tile : i.second.get_child("data") )
                    {
                        misc::Properties properties;
                        properties["x"] = x * lev->d->tile_width;
                        properties["y"] = y * lev->d->tile_height;

                        long gid = tile.second.get("<xmlattr>.gid",0);
                        if ( media::Tileset* ts = lev->d->get_tileset(gid) )
                        {
                            properties["tileset"] = ts->id();
                            properties["tileset.tile"] = gid;
                            Game_Manager::instance().object_library(layer_name)
                                .get_properties(ts->id(),gid,properties,true);
                            if ( properties.count("type") )
                                if ( Object* object = Object::create(properties) )
                                    lev->add_object(object,ol);
                        }

                        if ( ++x >= lev->d->map_width ) { y++; x = 0; }
                    }

                }
                else
                {
                    // normal layer
                    Tile_Layer *tl = new Tile_Layer(
                        layer_name,
                        i.second.get("<xmlattr>.width",lev->d->map_width),
                        i.second.get("<xmlattr>.height",lev->d->map_height),
                        lev->d->tile_width,
                        lev->d->tile_height
                    );
                    unsigned x = 0;
                    unsigned y = 0;
                    for ( auto tile : i.second.get_child("data") )
                    {
                        int gid = tile.second.get("<xmlattr>.gid",0);
                        if ( gid > 0 )
                            tl->set_tile(media::tile_pos(x,y),lev->d->get_tile(gid));
                        if ( ++x >= lev->d->map_width )
                        {
                            y++;
                            x = 0;
                        }
                    }

                    layer = tl;
                }
            }
            else if ( i.first == "imagelayer")
            {
                std::string img = i.second.get("image.<xmlattr>.source","");
                img = media::Media_Manager::instance().data_id(base_path+"/"+img);
                layer = new Image_Layer(
                    layer_name,
                    img,
                    geo::Point (
                        i.second.get("<xmlattr>.x",0),
                        i.second.get("<xmlattr>.y",0)
                    )
                );
            }
            else if ( i.first == "objectgroup" )
            {
                Object_Layer* ol = new Object_Layer(layer_name);
                layer = ol;
                for ( auto ptobject : i.second )
                    if ( ptobject.first == "object" )
                    {
                        misc::Properties properties;
                        if ( auto attr = ptobject.second.get_child_optional("<xmlattr>") )
                        {
                            misc::tree_to_properties(*attr,properties);

                            if ( properties.count("gid") )
                            {
                                long gid = (long)properties["gid"];
                                if ( media::Tileset* ts = lev->d->get_tileset(gid) )
                                {
                                    properties["tileset"] = ts->id();
                                    properties["tileset.tile"] = gid;
                                    Game_Manager::instance().object_library()
                                        .get_properties(ts->id(),gid,properties,false);
                                }
                            }
                        }
                        misc::get_properties(ptobject.second,properties);
                        if ( Object* object = Object::create(properties) )
                        {
                            object->set_y(object->y()-object->height());
                            lev->add_object(object,ol);
                        }
                    }
            }

            if ( layer )
            {
                layer->set_properties(misc::get_properties(i.second));
                lev->d->layers.push_back(layer);
            }
        }

        lev->d->indices.clear();
    }
    catch (const std::exception& ex)
    {
        delete lev;
        lev = nullptr;
        Logger::err() << "Error while loading" << id << ':' << ex.what();
    }
    return lev;
}
