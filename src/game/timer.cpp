/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "timer.hpp"
#include "game/game_manager.hpp"

using namespace game;

void Timer::step()
{
    if ( time_left > misc::duration::zero() )
    {
        time_left -= Game_Manager::instance().frame_time();
        if ( time_left <= misc::duration::zero() )
        {
            time_left = misc::duration::zero();
            if (ring) ring();
        }
    }
}

bool Timer::ready() const
{
    return time_left <= misc::duration::zero();
}

void Timer::start(const misc::duration& time)
{
    time_left = time;
}

void Timer::delay(const misc::duration& time)
{
    time_left += time;
}
