/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef GAME_TIMER_HPP
#define GAME_TIMER_HPP

#include <functional>
#include "misc/time.hpp"

namespace game {

/**
 * \brief Class handling timeouts
 */
class Timer
{
public:
    misc::duration time_left; ///< How much time still has to pass
    std::function<void()> ring; ///< Action performed when the time comes

    explicit Timer(const misc::duration& time = misc::duration::zero(),
                   const std::function<void()>& ring = std::function<void()>() )
    : time_left(time),ring(ring) {}

    /**
     * \brief Decrease the timer based on the Game_Manager frame time and call ring if needed
     */
    void step();

    /**
     * \brief Whether the timer has reached zero
     */
    bool ready() const;

    /**
     * \brief Set the timer to ring in the given time amount
     */
    void start(const misc::duration& time);

    /**
     * \brief Delay the timer by the given about
     */
    void delay(const misc::duration& time);
};

} // namespace game

#endif // GAME_TIMER_HPP
