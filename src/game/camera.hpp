/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <SFML/Graphics/View.hpp>
#include "misc/geometry.hpp"
#include "misc/mirror.hpp"

namespace game {

/**
 * \brief Portion of the level to be seen
 */
class Camera : public sf::View, public misc::Mirror
{
PROPERTY_MAGIC(Camera)

public:
    using sf::View::View;
    Camera(const sf::View& v) : sf::View(v) {}
    Camera(){}

    geo::Point starting_position;///<Starting position (used to get offsets)
    geo::Point target;          ///< Point to be followed
    double distance_max = 0;    ///< Max distance from target for slow following
    double distance_min = 0;    ///< Min distance from target for slow following
    double speed_min = 0;       ///< Speed used when slowly following the target
    double speed_max = 0;       ///< Maximum speed for following the target

    /**
     * \brief Move the camera as appropriate
     */
    void step();

    /**
     * \brief center X
     */
    float x() const;
    /**
     * \brief center Y
     */
    float y() const;
    /**
     * \brief set center X
     */
    void set_x(float x);
    /**
     * \brief set center Y
     */
    void set_y(float y);
    /**
     * \brief Set the center of the camera
     */
     void set_center(const geo::Point& p);
     /**
      * \brief Center point
      */
     geo::Point center() const;

    /**
     * \brief Width of the displayed game area
     */
    float width() const;
    /**
     * \brief Height of the displayed game area
     */
    float height() const;
    /**
     * \brief Set width of the displayed game area
     */
    void set_width(float w);
    /**
     * \brief Set height of the displayed game area
     */
    void set_height(float h);

    /**
     * \brief Get the displayed game area
     */
    geo::Rectangle game_area() const;

    /**
     * \brief Set center and initial position
     */
    void set_starting_position(const geo::Point& p);

    /**
     * \brief Get offset from starting position
     */
    geo::Point offset() const;

};


} // namespace camera
#endif // CAMERA_HPP
