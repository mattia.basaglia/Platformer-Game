/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "quad_tree.hpp"

using namespace game;
using namespace geo;

const int Quad_Tree::max_children = 8;
const int Quad_Tree::max_depth = 4;


void Quad_Tree::unsplit()
{
    if ( !leaf() )
        for ( auto& c : child )
        {
            delete c;
            c = nullptr;
        }
}

void Quad_Tree::split()
{
    if ( !leaf() ) return;

    Rectangle child_area(area.x,area.y,area.width/2,area.height/2);
    child[0] = new Quad_Tree(child_area,depth+1);
    child_area.x = area.x + area.width/2;
    child[1] = new Quad_Tree(child_area,depth+1);
    child_area.y = area.y + area.height/2;
    child[3] = new Quad_Tree(child_area,depth+1);
    child_area.x = area.x;
    child[2] = new Quad_Tree(child_area,depth+1);

    for ( auto o: objects )
        for ( auto c : child )
            c->insert(o);
}

bool Quad_Tree::inside_area(Object* obj) const
{
    return area.intersects(obj->bounding_rect());
}

void Quad_Tree::remove(const std::vector<Object*>::reverse_iterator& it)
{
    if ( !leaf() )
    {
        if ( objects.size() - 1 <= max_children )
            unsplit();
        else
            for ( auto c : child )
                c->remove(*it);
    }
    objects.erase(--it.base());
}

Quad_Tree::Quad_Tree( const Rectangle& area, int depth )
    : area(area),depth(depth)
{}

Quad_Tree::Quad_Tree( const Rectangle& area )
    : area(area)
{}

Quad_Tree::~Quad_Tree() { clear(); }

void Quad_Tree::update(Object* obj)
{
    auto it = std::find(objects.rbegin(),objects.rend(),obj);
    if ( !inside_area(obj)  )
    {
        if ( it != objects.rend() )
            remove(it); // obj is here but it should be removed
    }
    else if ( it == objects.rend() )
    {
        insert(obj); // obj not found but should be here
    }
    else if ( !leaf() )
    {
        // since obj has moved, move it to the end so it may be more efficient to remove it later
        std::swap(*it,objects.back());
        // obj found and should be here
        for ( auto c : child )
            c->update(obj);
    }
}

void Quad_Tree::remove ( Object* obj )
{
    auto it = std::find(objects.rbegin(),objects.rend(),obj);
    if ( it == objects.rend() )
        return;
    remove(it);
}

bool Quad_Tree::insert(Object* obj)
{
    if ( !inside_area(obj) )
        return false;

    if ( leaf() )
    {
        if ( objects.size() <= max_children || depth >= max_depth )
        {
            objects.push_back(obj);
            return true;
        }
        split();
    }

    for ( auto c : child )
        c->insert(obj);

    objects.push_back(obj);

    return true;
}

void Quad_Tree::clear()
{
    objects.clear();
    unsplit();
}

void Quad_Tree::clear(geo::Rectangle new_area)
{
    clear();
    area = new_area;
}

Object* Quad_Tree::object_at(float x, float y) const
{
    return object_at ( Point(x,y) );
}

Object* Quad_Tree::object_at(const Point& p) const
{
    if ( !area.contains(p) )
        return nullptr;

    if ( leaf() )
    {
        for ( auto o : objects )
            if ( Rectangle(o->bounding_rect()).contains(p) )
                return o;
    }
    else
    {
        for ( auto c : child )
            if ( auto o = c->object_at(p) )
                return o;
    }
    return nullptr;
}

const geo::Rectangle& Quad_Tree::get_area() const
{
    return area;
}

#ifdef DEBUG
#include <SFML/Graphics.hpp>
#include "media/media_manager.hpp"
#include "game/game_manager.hpp"
#include "misc/geometry_convert.hpp"
#include "media/ui/shaded_text.hpp"
void Quad_Tree::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    if ( !area.intersects(game::Game_Manager::instance().video().camera().game_area()) )
        return;

    misc::Color mycolor(255*(float(depth)/max_depth),0,255);
    sf::RectangleShape rs;
    mycolor.a = 64;

    if ( depth == 0 && Game_Manager::instance().storage()["debug"] > 8 )
    {
        rs.setOutlineThickness(0);
        rs.setFillColor(mycolor);
        for ( Object* o : get_objects<Object>(game::Game_Manager::instance().video().camera().game_area()) )
        {
            rs.setPosition(geo::conv::point(o->bounding_rect().top_left()));
            rs.setSize(geo::conv::size(o->bounding_rect().size()));
            target.draw(rs,states);
        }
    }

    if ( !leaf() )
        for ( auto c : child )
            c->draw(target,states);

    media::ui::Shaded_Text text;
    geo::Rectangle tr;

    if ( Game_Manager::instance().storage()["debug"] > 6 )
    {
        text.set_color(misc::Color(255-mycolor.r,255-mycolor.g,255-mycolor.b));
        text.set_font(media::Media_Manager::instance().font(""));
        text.set_size(24);
        text.set_text(std::to_string(objects.size()));
        tr = text.local_bounding_rect();
        text.setOrigin(tr.center().x,tr.center().y);
        text.setPosition(area.center().x, area.center().y );
        target.draw(text,states);
    }

    if ( Game_Manager::instance().storage()["debug"] > 7 )
    {
        text.set_size(16);
        text.set_text(std::to_string(depth));
        tr = text.local_bounding_rect();
        text.setOrigin(tr.center().x,tr.center().y);
        text.setPosition(area.center().x, area.center().y+16 );
        target.draw(text,states);
    }

    rs.setPosition(area.x,area.y);
    rs.setSize(sf::Vector2f(area.width,area.height));
    rs.setFillColor(sf::Color::Transparent);
    rs.setOutlineColor(mycolor);
    rs.setOutlineThickness(max_depth-depth+1);
    target.draw(rs,states);

}

#endif // DEBUG
