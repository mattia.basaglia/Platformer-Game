/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef LAYER_HPP
#define LAYER_HPP

#include "media/tileset.hpp"
#include <SFML/Graphics/Drawable.hpp>
#include "game/object/object.hpp"
#include "misc/game_value.hpp"
#include "misc/boilerplate.hpp"
#include <list>

namespace game {

/**
 * \brief Abstract class for level layers
 *
 * A layer is a self-contained render unit.
 */
class Layer : public sf::Drawable, public misc::Object_Source
{
    PROPERTY_MAGIC(Layer)

    class Layer_Private *d; ///< Pimpl
protected:
    /**
     * \brief Get offset from the origin caused by parallax
     */
    geo::Point parallax_offset() const;

    void for_each_object( const std::function<void(const std::string& name,misc::Mirror*)>& func ) const override {}
    misc::Mirror* get_child(const std::string& name) const override { return nullptr; }

public:
    Layer(const std::string& name);
    DECLARE_STRUCTORS(Layer)

    /**
     * \brief Clean up allocated resources
     */
    virtual void clear() {}

    /**
     * \brief Ensure that the level is ready to be drawn
     * \param from  Level we are coming from
     * \note Evaluates visibility based on the property condition (evaluated by Game_Manager)
     */
    virtual void load(const std::string& from);

    /**
     * \brief Get whether the level is to be rendered
     */
    bool visible() const;

    /**
     * \brief perform a logic step on dynamic layers
     */
    virtual void step(){}

    /**
     * \brief Get the layer name
     */
    const std::string& name() const;

    /**
     * \brief Speed at which it moves with the camera
     */
    double parallax_speed() const;

    /**
     * \brief Set speed at which it moves with the camera
     */
    void set_parallax_speed(double speed);

    /**
     * \brief Get the condition to evaluate at load time to decide whether it should be rendered
     */
    const std::string& condition() const;

    /**
     * \brief Set the condition to evaluate at load time to decide whether it should be rendered
     */
    void set_condition ( const std::string& cond );

};

/**
 * \brief A layer made up of static tiles
 */
class Tile_Layer : public Layer
{
    class Tile_Layer_Private *d;

    PROPERTY_MAGIC_INHERIT(Tile_Layer,Layer)

protected:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

public:
    /**
     * \brief Constructor
     * \param name          Layer name
     * \param width         Number of horizontal tiles
     * \param height        Number of vertical tiles
     * \param tile_width    Tile width
     * \param tile_height   Tile height
     */
    Tile_Layer(const std::string& name, int width, int height, int tile_width, int tile_height);

    DECLARE_STRUCTORS(Tile_Layer)

    /**
     * \brief Render layer to a single image
     */
    void bake();

    /**
     * \brief Set a tile in the layer
     */
    void set_tile(media::Tile_Position position, sf::Sprite sprite );

    void load(const std::string&) override;
    void step() override;
};

/**
 * \brief Background image
 */
class Image_Layer : public Layer
{
    class Image_Layer_Private *d;

    PROPERTY_MAGIC_INHERIT(Image_Layer,Layer)
protected:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

public:

    Image_Layer(const std::string& name, const std::string& image_id, const geo::Point& pos );

    DECLARE_STRUCTORS(Image_Layer)

    /**
     * \brief Set tiling specification for the X coordinate
     * \param tiling    Tiling string, accepted values:
     *      * \b left
     *      * \b right
     *      * \b 1 \b yes \b true \b both
     *      * (Anything else disables tiling)
     */
    void set_tiling_x(const std::string& tiling);

    /**
     * \brief Set tiling specification for the Ycoordinate
     * \param tiling    Tiling string, accepted values:
     *      * \b up
     *      * \b down
     *      * \b 1 \b yes \b true \b both
     *      * (Anything else disables tiling)
     */
    void set_tiling_y(const std::string& tiling);

};

/**
 * \brief Object Layer
 */
class Object_Layer : public Layer
{
    class Object_Layer_Private *d;

    PROPERTY_MAGIC_INHERIT(Object_Layer,Layer)
protected:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    void for_each_object( const std::function<void(const std::string& name,misc::Mirror*)>& func ) const override;

    /**
     * \brief Get object by name
     * \return Pointer to the matching object or \b nullptr if the object has not been found
     */
    Object* get_child(const std::string& name) const override;
public:
    Object_Layer(const std::string& name);
    DECLARE_STRUCTORS(Object_Layer)

    /**
     * \brief Remove and delete all objects
     */
    virtual void clear() override;

    /**
     * \brief Add an object to the layer
     * \note Does not take ownership \see clear()
     */
    void add_object(Object* object);
    /**
     * \brief Remove an object from the layer
     */
    void remove_object(Object* object);

    void step() override;
    /**
     * \brief Get objects
     */
    const std::list<Object*>& objects() const;

};

} // namespace game


#endif // LAYER_HPP
