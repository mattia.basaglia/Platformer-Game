/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef GAME_MANAGER_HPP
#define GAME_MANAGER_HPP

#include "game/game_data.hpp"
#include "misc/game_value.hpp"
#include "misc/time.hpp"
#include "game/video.hpp"
#include "eval/context.hpp"
#include <vector>
#include <string>
#include <map>

/**
 * \brief Namespace for the game logic
 */
namespace game {

class Level;
class Object_Library;
class User_Settings;
class Timer;

/**
 * \brief Game manager
 */
class Game_Manager : public misc::Mirror
{

	PROPERTY_MAGIC(Game_Manager)

private:
	/// Pimpl
	class Game_Manager_Private* d;

	Game_Manager();
	Game_Manager( const Game_Manager& ) = delete;
	~Game_Manager();

public:
	/**
	 * \brief Singleton instance
	 */
	static Game_Manager& instance();


/**
 * \name Execution
 * Methods managing the game execution
 * \{
 */
	/**
	 * \brief initialize the game engine
	 * \return \c true if successful
	 */
	bool init();

	/**
	 * \brief Run the game
	 * \return Exit code (0 if OK)
	 */
	int run();

	/**
	 * \brief Begin a new game
	 * \param game_file	File with the game description
	 * \return \c false on failure
	 */
	bool start_game(const std::string& game_file);

	/**
	 * \brief Get currently active level
	 */
	Level* current_level();

	/**
	 * \brief Get current game metada
	 */
	const Game_Data& current_game() const;

	/**
	 * \brief Pause/resume the game
	 */
	void pause(bool pause=true);

	/**
	 * \brief whether the game is paused
	 */
	bool is_paused() const;

	/**
	 * \brief Quit the game
	 */
	void quit();

	/**
	 * \brief Load a level from its id
	 * \param id	Level id (path relative to data)
	 * \return \c false on failure
	 */
	bool goto_level(const std::string& id);

	/**
	 * \brief Save game status
	 * \param filename	File to save to (absolute path)
	 */
	void save(const std::string& filename);

	/**
	 * \brief Load game from file
	 * \param filename	File to load from (absolute path)
	 */
	void load(const std::string& filename);

	/**
	 * \brief Get the video object
	 */
	Video& video();
///\}

/**
 * \name Status
 * Methods monitoring game information
 * \{
 */
	/**
	 * \brief Time since the beginning
	 */
	misc::duration time() const;

	/**
	 * \brief The time it took to render the previous frame
	 */
	const misc::duration& frame_time() const;

	/**
	 * \brief Frames per second
	 */
	long fps() const;

	/**
	 * \brief Get the (read-only) storage
	 */
	const misc::Properties& storage() const;

	/**
	 * \brief Get user settings
	 */
	User_Settings& settings();

///\}

/**
 * \name Scripting
 * Methods handling scripts and commands
 * \{
 */

	/**
	 * \brief Install a context function
	 */
	void add_function ( const std::string& name, const eval::Context::Function& cmd );

	/**
	 * \brief Get a context with all the game properties
	 */
	const eval::Context& get_context();


    /**
     * \brief Schedule a function call
     * \param timer	A timer to add to the queue
     *
     * The timer will advance at every non-paused frame, once it rings, it
     * will execute its function (as usual) and will be removed from the queue.
     */
    void schedule(const game::Timer& timer);

/// \}

/**
 * \name Objects
 * Methods managing objects
 * \{
 */
	/**
	 * \brief Get the generic object library
	 */
	Object_Library& object_library();

    /**
     * \brief Get library for a specific layer
     */
    Object_Library& object_library(const std::string& layer_name);

    /**
     * \brief Whether it has object definitions for a specific layer
     */
    bool has_object_library(const std::string& layer_name) const;
///\}

};

} // namespace game


#endif // GAME_MANAGER_HPP
