/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "game/camera.hpp"
#include "game/game_manager.hpp"
#include "game/level.hpp"
#include "misc/geometry_convert.hpp"
#include <SFML/Audio/Listener.hpp>

using namespace game;

DECLARE_PROPERTY_ATTRIBUTE_RENAME_EX(Camera,"distance.max",distance_max,distance_max)
DECLARE_PROPERTY_ATTRIBUTE_RENAME_EX(Camera,"distance.min",distance_min,distance_min)
DECLARE_PROPERTY_ATTRIBUTE_RENAME_EX(Camera,"speed.max",speed_max,speed_max)
DECLARE_PROPERTY_ATTRIBUTE_RENAME_EX(Camera,"speed.min",speed_min,speed_min)
DECLARE_PROPERTY(Camera,x,x,set_x)
DECLARE_PROPERTY(Camera,y,y,set_y)
DECLARE_PROPERTY(Camera,width,width,set_width)
DECLARE_PROPERTY(Camera,height,height,set_height)



float Camera::x() const
{
    return getCenter().x;
}
float Camera::y() const
{
    return getCenter().y;
}

void Camera::set_x(float x)
{
    setCenter(x,y());
}

void Camera::set_y(float y)
{
    setCenter(x(),y);
}

float Camera::width() const
{
    return getSize().x;
}

float Camera::height() const
{
    return getSize().y;
}

void Camera::set_width(float w)
{
    setSize(w,height());
}

void Camera::set_height(float h)
{
    setSize(width(),h);
}

geo::Rectangle Camera::game_area() const
{
    return geo::Rectangle(x()-width()/2,y()-height()/2,width(),height());
}


void Camera::set_center(const geo::Point& p)
{
    setCenter(geo::conv::point(p));
    sf::Listener::setPosition(p.x, p.y, 0);
}

geo::Point Camera::center() const
{
    return geo::conv::point(getCenter());
}

void Camera::step()
{

    geo::Line camline ( center(), target );
    if ( camline.length() > distance_min )
    {
        double speed_factor = std::min(1.0,
           ( camline.length() - distance_min ) / ( distance_max - distance_min ) );
        camline.set_length((speed_factor*(speed_max-speed_min)+speed_min)/Game_Manager::instance().fps());
        set_center(camline.p2);

    }

    // fit within boundaries
    geo::Rectangle vp = game_area();
    Level* current_level = Game_Manager::instance().current_level();
    if ( vp.left() < 0 )
        set_x(vp.width/2);
    else if ( vp.right() > current_level->size().width )
        set_x(current_level->size().width - vp.width/2);
    if ( vp.top() < 0 )
        set_y(vp.height/2);
    else if ( vp.bottom() > current_level->size().height )
        set_y(current_level->size().height - vp.height/2);
}

void Camera::set_starting_position(const geo::Point& p)
{
    set_center(p);
    starting_position = p;
    target = p;
}


geo::Point Camera::offset() const
{
    return center() - starting_position;
}
