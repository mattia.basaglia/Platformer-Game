/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef QUAD_TREE_HPP
#define QUAD_TREE_HPP

#include <vector>
#include "game/object/object.hpp"
#include "config.hpp"

namespace game {

class Object;

/**
 * \brief Data structure to quickly locate ovelapping objects
 */
class Quad_Tree
{
public:
    typedef std::vector<Object*>        container;      ///< Internal container
    typedef container::iterator         iterator;       ///< Iterator
    typedef container::const_iterator   const_iterator; ///< Constant iterator

private:
    static const int max_children;  ///< Maximum number of children before a split occurs
    static const int max_depth;     ///< Maximum tree depth

    /// Children, if null the node is a leaf
    Quad_Tree* child[4] = { nullptr, nullptr, nullptr, nullptr };
    /// Area covered by the tree
    geo::Rectangle area;
    /// Objects contained within this tree
    container objects;
    /// Node depth
    int depth = 0;

    /// Remove all the children
    void unsplit();

    /// Create the four children
    void split();

    /**
     * \brief Constructor for internal nodes
     */
    Quad_Tree( const geo::Rectangle& area, int depth );

    /**
     * \brief Whether the object is inside the tree area
     */
    bool inside_area(Object* obj) const;

    /**
     * \brief Remove an object that has already been checked to be in the tree
     */
    void remove(const std::vector<Object*>::reverse_iterator& it);

    /**
     * \brief Whether the node is a leaf
     */
    bool leaf() const { return !child[0]; }

public:
    /**
     * \brief Constructor
     * \param area  Area covered by the tree
     */
    explicit Quad_Tree( const geo::Rectangle& area );
    Quad_Tree(const Quad_Tree&) = delete;
    ~Quad_Tree();

    /**
     * \brief Insert or remove obj as needed
     */
    void update(Object* obj);

    /**
     * \brief Remove an object from the tree
     */
    void remove ( Object* obj );

    /**
     * \brief Insert an object to the tree
     */
    bool insert(Object* obj);

    /**
     * \brief Remove all the objects
     */
    void clear();

    /**
     * \brief Remove all the objects and set a new area to cover
     */
    void clear(geo::Rectangle new_area);

    /**
     * \brief Get objects inside a rectangle region
     * \param region    Region to be searched
     * \tparam ObjectT  Type derived from game::Object
     */
    template<class ObjectT>
        std::vector<ObjectT*> get_objects(const geo::Rectangle& region) const
    {
        std::vector<ObjectT*> colliding;
        if ( !region.intersects(area) )
            return colliding;

        if ( leaf() )
        {
            for ( Object* o : objects )
                if ( region.intersects(o->bounding_rect()) )
                    if ( ObjectT* obj = o->cast<ObjectT*>() )
                        colliding.push_back(obj);
        }
        else
        {
            for ( auto c : child )
            {
                auto cobj = c->get_objects<ObjectT>(region);
                colliding.reserve(colliding.size()+cobj.size());
                colliding.insert(colliding.end(),cobj.begin(),cobj.end());
            }
        }
        std::sort(colliding.begin(),colliding.end());
        auto last = std::unique(colliding.begin(), colliding.end());
        colliding.erase(last, colliding.end());
        return colliding;
    }

    /**
     * \brief Get objects matching a condition inside a rectangle region
     * \param region    Region to be searched
     * \param condition Functor taking a pointer to \c ObjectT and returning \c bool
     * \tparam ObjectT  Type derived from game::Object
     */
    template<class ObjectT>
        std::vector<ObjectT*> get_objects_if(const geo::Rectangle& region,
                            const std::function<bool (const ObjectT*)>& condition ) const
    {
        std::vector<ObjectT*> colliding;
        if ( !region.intersects(area) )
            return colliding;

        if ( leaf() )
        {
            for ( Object* o : objects )
            {
                ObjectT* ot = o->cast<ObjectT*>();
                if ( ot && region.intersects(o->bounding_rect()) && condition(ot) )
                    colliding.push_back(ot);
            }
        }
        else
        {
            for ( auto c : child )
            {
                auto cobj = c->get_objects_if<ObjectT>(region,condition);
                colliding.reserve(colliding.size()+cobj.size());
                colliding.insert(colliding.end(),cobj.begin(),cobj.end());
            }
        }
        std::sort(colliding.begin(),colliding.end());
        auto last = std::unique(colliding.begin(), colliding.end());
        colliding.erase(last, colliding.end());
        return colliding;
    }


    /**
     * \brief Get whether there are objects inside a rectangle region
     * \param region    Region to be searched
     * \tparam ObjectT  Type derived from game::Object
     */
    template<class ObjectT>
        bool has_objects(const geo::Rectangle& region) const
    {
        if ( !region.intersects(area) )
            return false;

        if ( leaf() )
        {
            for ( Object* o : objects )
                if ( region.intersects(o->bounding_rect()) && o->cast<ObjectT*>() )
                        return true;
        }
        else
        {
            for ( auto c : child )
            {
                if ( c->has_objects<ObjectT>(region) )
                    return true;
            }
        }
        return false;
    }

    /**
     * \brief Get whether there are objects matching a condition inside a rectangle region
     * \param region    Region to be searched
     * \param condition Functor taking a pointer to \c ObjectT and returning \c bool
     * \tparam ObjectT  Type derived from game::Object
     */
    template<class ObjectT>
        bool has_objects_if(const geo::Rectangle& region,
                            const std::function<bool (const ObjectT*)>& condition ) const
    {
        if ( !region.intersects(area) )
            return false;

        if ( leaf() )
        {
            for ( Object* o : objects )
            {
                ObjectT* ot = o->cast<ObjectT*>();
                if ( ot && region.intersects(o->bounding_rect()) && condition(ot) )
                    return true;
            }
        }
        else
        {
            for ( auto c : child )
            {
                if ( c->has_objects_if<ObjectT>(region,condition) )
                    return true;
            }
        }
        return false;
    }

    /**
     * \brief Get objects inside a circular area
     * \param point     Center of the region
     * \param radius    Radius
     * \param exclude   Don't match this object
     */
    std::vector<Object*> get_objects(const geo::Point& point, float radius, Object* exclude = nullptr) const;

    /**
     * \brief Get objects inside a circular area
     * \param region    Region to be searched
     * \param exclude   Don't match this object
     * \todo template + _if
     */
    std::vector<Object*> get_objects(const geo::Circle& region, Object* exclude = nullptr) const;

    /**
     * \brief Get the minimal subset of \c region containing the same objects as \c region
     * \param region    Region to be scanned
     * \tparam ObjectT  Type derived from game::Object
     */
    template<class ObjectT>
        geo::Rectangle collision_rectangle(const geo::Rectangle& region) const
    {
        geo::Rectangle rect;

        if ( !region.intersects(area) )
            return rect;

        if ( leaf() )
        {
            for ( auto o : objects )
                if ( region.intersects(o->bounding_rect()) && o->has_type_id(ObjectT::static_type_id()) )
                        rect.unite ( region.intersection(o->bounding_rect()) );
        }
        else
        {
            for ( auto c : child )
                rect.unite(c->collision_rectangle<ObjectT>(region));
        }

        return rect;
    }
    /**
     * \brief Get the minimal subset of \c region containing the same objects as \c region that match a condition
     * \param region    Region to be scanned
     * \param condition Functor taking a pointer to \c ObjectT and returning \c bool
     * \tparam ObjectT  Type derived from game::Object
     */
    template<class ObjectT>
        geo::Rectangle collision_rectangle_if(const geo::Rectangle& region,
                                              const std::function<bool (const ObjectT*)>& condition) const
    {
        geo::Rectangle rect;

        if ( !region.intersects(area) )
            return rect;

        if ( leaf() )
        {
            for ( auto o : objects )
            {
                ObjectT* ot = o->cast<ObjectT*>();
                if ( ot && region.intersects(o->bounding_rect()) && condition(ot) )
                    rect.unite ( region.intersection(o->bounding_rect()) );
            }
        }
        else
        {
            for ( auto c : child )
                rect.unite(c->collision_rectangle_if<ObjectT>(region,condition));
        }

        return rect;
    }

    /**
     * \brief Get the first object at the given position
     */
    Object* object_at(float x, float y) const;

    /**
     * \brief Get the first object at the given position
     */
    Object* object_at(const geo::Point& p) const;

    iterator       begin()      { return objects.begin(); }
    const_iterator begin() const{ return objects.begin(); }
    const_iterator cbegin()const{ return objects.cbegin();}
    iterator       end()        { return objects.end();   }
    const_iterator end() const  { return objects.end();   }
    const_iterator cend() const { return objects.cend();  }
    /**
     * \brief Get a list of objects contained in the tree
     */
    const container& get_objects() const { return objects; }

#ifdef DEBUG
    /**
     * \brief Show a representation of the data structure
     */
    void draw(sf::RenderTarget& target, sf::RenderStates states ) const;
#endif // DEBUG

    /**
     * \brief Get the covered area
     */
    const geo::Rectangle& get_area() const;
};


} // namespace game
#endif // QUAD_TREE_HPP
