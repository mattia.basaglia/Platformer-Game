/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "game/game_manager.hpp"

#include "game/object/object.hpp"
#include "game/level.hpp"
#include "game/game_library.hpp"
#include "game/user_settings.hpp"
#include "game/layer.hpp"

#include "media/media_manager.hpp"
#include "media/ui/shaded_text.hpp"

#include "misc/logger.hpp"
#include "misc/string_utils.hpp"
#include "misc/ptree_utils.hpp"
#include "misc/geometry_convert.hpp"

#include "eval/parser.hpp"
#include "eval/library.hpp"

#include "config.hpp"

#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>


using namespace game;
using namespace media;


DECLARE_PROPERTY_GETTER(Game_Manager,fps,fps)
DECLARE_PROPERTY(Game_Manager,paused,is_paused,pause)
DECLARE_PROPERTY_GET_FUNCTOR(Game_Manager,time,[](const Game_Manager&gm){return gm.time().count();})

DECLARE_METHOD_VOID_NOARG(Game_Manager,quit)

/// \pimpl{Game_Manager}
struct game::Game_Manager_Private
{
    std::vector<std::string>    game_files;     ///< List of game descriptors
    Game_Data                   game;           ///< Current game
    misc::Properties            storage;        ///< Persistent values
    Level*                      current_level;  ///< Current Level
#ifdef DEBUG
    ui::Shaded_Text_Input       current_command;///< Command being currently typed
#endif
    bool                        paused = false; ///< Whether to suspend the game
    misc::time                  start_time;     ///< Time since when the game engine has started
    misc::duration              frame_time;     ///< Time spent to render the last frame
    long                        fps = 60;       ///< Frames per second
	Object_Library              object_library; ///< Library of automatic objects
    std::map<std::string,Object_Library>layer_library;///< Layer-specific automatic objects
    eval::Context               context;        ///< Script context
    User_Settings               settings;       ///< User settings
    Video                       video;          ///< Video object
    Object_Layer                ui;             ///< User Interface
    std::list<game::Timer>      timers;         ///< Scheduled functions


    Game_Manager_Private()
        : current_level(nullptr),start_time(misc::now()), ui("ui")
    {
}

	void pause(bool pause)
	{
	    paused = pause;
	    if ( pause && current_level )
            current_level->pause_music();
	}

    /**
     * \brief Handle window events
     */
    void handle_events()
    {
        sf::Event ev;
        while (video.window().pollEvent(ev))
        {

#ifdef DEBUG
            if ( current_command.active() )
                commandmode_input(ev);
#endif // DEBUG

            switch ( ev.type )
            {
                case sf::Event::Closed:
                    Game_Manager::instance().quit();
                    break;
                case sf::Event::LostFocus:
                    pause(true);
                    break;
                case sf::Event::GainedFocus:
                    pause(false);
                    break;
#ifdef DEBUG
                case sf::Event::MouseButtonPressed:
                {
                    geo::Point mpos = video.map_from_pixel(ev.mouseButton.x, ev.mouseButton.y);
                    eval::Context ctx(&context);
                    ctx.set_temporary("mouse.x",misc::Value::from(mpos.x));
                    ctx.set_temporary("mouse.y",misc::Value::from(mpos.y));
                    if ( current_level )
                    {
                        if ( Object* o = current_level->collision().object_at(mpos.x,mpos.y) )
                            ctx.set_object("mouse_object",o);
                    }
                    eval::Parser().evaluate((std::string)storage["debug.mouse"],ctx);

                    break;
                }
                case sf::Event::KeyReleased:
                    if ( !current_command.active() && ev.key.code == sf::Keyboard::Key::BackSlash ) /// \todo keyboard bind manager
                    {
                        current_command.set_active(true);
                    }
                    break;
#endif // DEBUG
                default:
                    break;
            }
        }


        if ( !paused
#ifdef DEBUG
            && !current_command.active()
#endif // DEBUG
            )
        {
            video.step();
            current_level->play_music();
            current_level->step();

            if ( !timers.empty() )
                for ( auto it = timers.begin(); it != timers.end(); )
                {
                    it->step();
                    if ( it->ready() )
                        it = timers.erase(it);
                    else
                        ++it;
                }
        }
        ui.step();
    }

    /**
     * \brief Render a frame to the window
     */
    void render()
    {
        video.render(current_level);

        // ui
        video.begin_ui();
        video.window().draw(ui);

#ifdef DEBUG
        if ( current_command.active() )
        {
            current_command.setPosition(1,video.camera().height()-current_command.size()-1);
            video.window().draw(current_command);
        }
#endif // DEBUG

        video.end_ui();
        video.window().display();
    }
#ifdef DEBUG
    /**
     * \brief Hanle input when in command mode
     */
    void commandmode_input(const sf::Event& ev)
    {
        if ( ev.type == sf::Event::TextEntered )
        {
            switch ( ev.text.unicode )
            {
                case '\x1b':
                    current_command.set_active( false );
                    break;
                case '\n':
                case '\r':
                    current_command.set_active( false );
                    Logger() << "Executing command" << current_command.text();
                    Logger(16) << "Result:" << eval::Parser().evaluate(current_command.text(),context);
            }
        }

        if ( !current_command.active() )
        {
            current_command.set_text("");
        }

        current_command.event(ev);
    }
#endif // DEBUG

};

Game_Manager::Game_Manager()
    : d ( new Game_Manager_Private )
{
    d->context.set_object("game",this);
    d->context.set_object("camera",&d->video.camera());
    d->context.set_object("storage",d->storage);
    d->context.set_object("settings",&d->settings);
    d->context.set_object("video",&d->video);
    d->context.set_object("ui",&d->ui);

    d->context.install(eval::lib::string_accessors);
    d->context.install(eval::lib::array);
    d->context.install(eval::lib::math);
    d->context.install(eval::lib::console);
    d->context.install(eval::lib::cast);
    d->context.install(game_library);

    media::Media_Manager::instance().create_path(
        media::Media_Manager::instance().user_file(""));
    d->settings.set_default_file(
        media::Media_Manager::instance().user_file("settings.xml") );

    if ( boost::filesystem::exists(d->settings.default_file()) )
        d->settings.load();

}

Game_Manager::~Game_Manager()
{
    d->settings.save();
    delete d->current_level;
    delete d;
}

Game_Manager& Game_Manager::instance()
{
    static Game_Manager singleton;
    return singleton;
}

bool Game_Manager::init()
{
    using namespace boost::filesystem;

    // Load game descriptors
    auto gamedirs = Media_Manager::instance().data_files("games");
    d->game_files.clear();
    for ( auto gamedir : gamedirs )
    {
        if ( is_directory(gamedir) )
            for ( auto i = directory_iterator(gamedir); i != directory_iterator(); ++i )
            {
                if ( is_regular(*i) )
                    d->game_files.push_back(i->path().string());
            }
    }
    if ( d->game_files.empty() )
    {
        Logger::error("Unable to find any game data");
        return false;
    }

    // Load Fonts
    auto fontdirs = Media_Manager::instance().data_files("fonts");
    for ( auto fontdir : fontdirs )
    {
        if ( is_directory(fontdir) )
            for ( auto i = directory_iterator(fontdir); i != directory_iterator(); ++i )
            {
                if ( is_regular(*i) && i->path().extension() != ".txt" )
                {
                    media::Font *font = new media::Font();
                    if ( font->loadFromFile(i->path().string()) )
                    {
                        Logger() << "Loaded font" << i->path().stem();
                        media::Media_Manager::instance().register_font(i->path().stem().string(),font);
                    }
                    else
                        delete font;
                }
            }
    }

    d->video.set_title(NAME_NICE);
    d->video.init();

    return true;
}

bool Game_Manager::start_game(const std::string& game_file)
{
    using namespace boost::property_tree;

    d->storage.clear();
    d->game = Game_Data();
    d->object_library.clear();
    d->layer_library.clear();
    d->ui.clear();

#ifdef DEBUG
    d->storage["debug.mouse.status"] = misc::Value("print(\"Object at\",mouse.x,mouse.y); status(mouse_object.name);");
    d->storage["debug.mouse.kill"] = misc::Value("mouse_object.name && kill(mouse_object.name);");
    d->storage["debug.mouse.spawn"] = misc::Value("spawn(storage.debug.mouse.spawn.object,'center.x',mouse.x,'center.y',mouse.y)");
    d->storage["debug.mouse.damage"] = misc::Value("mouse_object.health -= 1");
    d->storage["debug.mouse"] = d->storage["debug.mouse.status"];
#endif

    static int acceptable_file_version = 1;

    ptree pt;
    try{
        read_xml(game_file,pt,xml_parser::trim_whitespace|xml_parser::no_comments);
    } catch (const std::exception& ex)
    {
        Logger::err() << "Error when loading" << game_file << ':' << ex.what();
        return false;
    }

    if ( pt.get("game.<xmlattr>.version",1) != acceptable_file_version )
    {
        Logger::error ("Unsupported game file: "+game_file);
        return false;
    }

    d->game.first_level = pt.get<std::string>("game.firstlevel","");
    if ( d->game.first_level.empty() )
    {
        Logger::error("Missing <firstlevel>: "+game_file);
        return false;
    }

    d->game.license = pt.get<std::string>("game.license","");
    if ( !d->game.valid_license() )
    {
        Logger::error("Unsupported license for "+game_file);
        Logger::error("Please use GPLv3+");
        return false;
    }

    d->game.name = pt.get<std::string>("game.name",boost::filesystem::basename(game_file));

    // load not-critical data
    for ( auto v : pt.get_child("game") )
    {
        if ( v.first == "author" )
            d->game.authors.push_back(v.second.data());
        else if ( v.first == "storage" )
        {
            misc::tree_to_properties(v.second,d->storage);
        }
        else if ( v.first == "objects" )
        {
            for(const auto& layertype : v.second)
            {
                if ( layertype.first == "objectgroup" )
                {
                    d->object_library.load(layertype.second);
                }
                else if ( layertype.first == "layer" )
                {
                    d->layer_library[layertype.second.get("<xmlattr>.name","")]
                        .load(layertype.second);
                }
            }
        }
        else if ( v.first == "camera" )
            d->video.camera().set_properties(misc::tree_to_properties(v.second));
        else if ( v.first == "ui" )
        {
            auto defont = pt.get_optional<std::string>("default-font");
            if ( defont )
                media::Media_Manager::instance().set_sefault_font(*defont);
            for(const auto& ui : v.second)
            {
                if ( ui.first == "object" )
                {
                    auto properties = misc::tree_to_properties(ui.second);
                    if ( properties.count("inherit") )
                        d->object_library.get_properties((std::string)properties["inherit"],properties,false);
                    if ( Object* obj = Object::create(properties) )
                        d->ui.add_object(obj);
                }
                else
                    media::Media_Manager::instance().load(ui.first,ui.second);
            }
        }
        else if ( v.first == "object_library" )
            d->object_library.load(v.second);
        else
            media::Media_Manager::instance().load(v.first,v.second);
    }

    Logger(1,false,false) << "Loaded Game \"" << d->game.name << '"';
    if ( !d->game.authors.empty() )
    {
        Logger() << " by " << d->game.authors;
    }
    d->video.set_title(d->game.name);

    return goto_level(d->game.first_level);;
}

int Game_Manager::run()
{
    /// \todo menu to select which game to start
    for(auto gamefile : d->game_files )
        if ( start_game(gamefile) )
            break;

    unsigned long nframes = 0;

    misc::time past_second = misc::now();

    while(d->current_level)
    {
        misc::time frame_start = misc::now();

        d->handle_events();

        if ( !d->video.window().isOpen() )
            return 0;

        d->render();

        nframes++;
        if ( misc::now() - past_second >= misc::milliseconds(1000) )
        {
            d->fps = nframes;
            past_second = misc::now();
            nframes = 0;
        }

        d->frame_time = misc::now() - frame_start;
    }
    return 1;
}

const misc::duration& Game_Manager::frame_time() const
{
    return d->frame_time;
}

void Game_Manager::save(const std::string& filename)
{
    /// \todo implement
}

void Game_Manager::load(const std::string& filename)
{
    /// \todo implement
}

Level* Game_Manager::current_level()
{
    return d->current_level;
}

bool Game_Manager::goto_level(const std::string& id)
{
    Level* lev = Level::create(id);
    if ( lev )
    {
        std::string from;
        if ( d->current_level )
        {
            from = d->current_level->id();
            d->current_level->unload();
            delete d->current_level;
        }
        d->current_level = lev;
        d->context.set_object("level",d->current_level);
        d->context.set_object_source(d->current_level);
        d->current_level->load(from);


    }

    return false;
}

const Game_Data& Game_Manager::current_game() const
{
    return d->game;
}

Video& Game_Manager::video()
{
    return d->video;
}

const misc::Properties& Game_Manager::storage() const
{
    return d->storage;
}

/// \note unused
void Game_Manager::add_function ( const std::string& name, const eval::Context::Function& cmd )
{
    d->context.set_function(name,cmd);
}

bool Game_Manager::is_paused() const
{
    return d->paused;
}

void Game_Manager::pause(bool pause)
{
    d->pause(pause);
}

void Game_Manager::quit()
{
    d->video.window().close();
}

misc::duration Game_Manager::time() const
{
    return misc::now()-d->start_time;
}

long Game_Manager::fps() const
{
    return d->fps;
}

const eval::Context& Game_Manager::get_context()
{
    return d->context;
}

Object_Library& Game_Manager::object_library()
{
    return d->object_library;
}
Object_Library& Game_Manager::object_library(const std::string& layer_name)
{
    return d->layer_library[layer_name];
}

bool Game_Manager::has_object_library(const std::string& layer_name) const
{
    return d->layer_library.count(layer_name);
}


User_Settings& Game_Manager::settings()
{
    return d->settings;
}

void Game_Manager::schedule(const game::Timer& timer)
{
    d->timers.push_back(timer);
}
