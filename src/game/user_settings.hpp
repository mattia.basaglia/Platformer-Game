/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef USER_SETTINGS_HPP
#define USER_SETTINGS_HPP

#include "misc/mirror.hpp"
#include "misc/boilerplate.hpp"

namespace game {

/**
 * \brief Class managing user settings
 */
class User_Settings : public misc::Mirror
{
    PROPERTY_MAGIC(User_Settings)

private:
    class User_Settings_Private *d;


protected:
	misc::Value get_undeclared_property(const std::string& name) const override;
	bool has_undeclared_property(const std::string& name) const override;
	void set_undeclared_property(const std::string& name,const misc::Value& value) override;
	bool has_writable_undeclared_property(const std::string& name) const override;
	void get_all_undeclared_properties(misc::Properties& out, const std::string& prefix) const override;

public:
    User_Settings();
    DECLARE_STRUCTORS(User_Settings)


    /**
     * \brief Save settings to file
     * \param file  (Absolute) file name
     * \return Whether the settings have successfully been written to the file
     */
    bool save(const std::string& file="") const;

    /**
     * \brief Load from file
     * \param file  (Absolute) file name
     * \return Whether the settings have been successfully read from the file
     */
    bool load(const std::string& file="");


    /**
     * \brief Get a property and cast it to the given type
     *
     * If the property doesn't exist, it's initialized with the provided value
     */
    template<class T>
        T get(const std::string& name, const T& default_value=T())
        {
            if ( has_property(name) )
                return get_property(name).to<T>();
            set(name,default_value);
            return default_value;
        }

    /**
     * \brief Set a property from any value
     */
    template<class T>
        void set(const std::string& name, const T& value)
        {
            set_property(name,misc::Value::from(value));
        }

    /**
     * \brief Remove all settings
     */
    void clear();

    /**
     * \brief Set default (absolute) file path for save and load
     */
    void set_default_file( const std::string& path );


    /**
     * \brief Get default file path for save and load
     */
    const std::string& default_file() const;
};

} // namespace game

#endif // USER_SETTINGS_HPP
