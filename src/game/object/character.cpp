/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "character.hpp"
#include "game/game_manager.hpp"
#include "game/level.hpp"
#include "blocks.hpp"
#include "misc/math_utils.hpp"


using namespace game;
using namespace game::object;

REGISTER_OBJECT(Character,"character")
DECLARE_PROPERTY_ATTRIBUTE_RENAME_EX(Character,"speed.x",speed.x,physics_speed_x)
DECLARE_PROPERTY_ATTRIBUTE_RENAME_EX(Character,"speed.y",speed.y,physics_speed_y)
DECLARE_PROPERTY_ATTRIBUTE(Character,gravity)
DECLARE_PROPERTY_ATTRIBUTE(Character,solid_index)
DECLARE_PROPERTY_ATTRIBUTE(Character,target_index)
DECLARE_PROPERTY_GETTER(Character,air,air)
DECLARE_PROPERTY(Character,health,health,set_health)

void Character::collision_walls(geo::Point& next_pos)
{
    // Horizontal wall collision
    float delta_x = next_pos.x-x();
    geo::Rectangle hit_check = bounding_rect().translated(delta_x,0); // get x-translated bounding box
    geo::Rectangle collided = level()->collision().collision_rectangle_if<Block>(hit_check,
        [this](const Block*b){ return (b->solid_index & solid_index) && b->block_type == Block::SOLID; } ); // get intersecting blocks

    collided.unite( level()->collision().collision_rectangle_if<Dynamic_Block>(hit_check,
        [this](const Dynamic_Block*b){ return (b->solid_index & solid_index) && b != this; } ) );

    if ( collided.width > 0  && !math::float_zero(collided.width) ) // there has been an intersection
    {
        if ( delta_x < 0 || ( math::float_zero(delta_x) && collided.center().x < bounding_rect().center().x ) )
        {
            next_pos.x = collided.right()-local_bounding_rect().left(); // prevent movement towards the left
            hit |= LEFT;
        }
        else
        {
            next_pos.x = collided.left()-local_bounding_rect().right(); // prevent movement towards the right
            hit |= RIGHT;
        }
        speed.x = 0; // cannot move this way, so stop
    }
}
void Character::collision_floor_ceiling(geo::Point& next_pos)
{
    // Vertical wall collision
    float delta_y = next_pos.y-y();
    geo::Rectangle hit_check = bounding_rect().translated(next_pos.x-x(),delta_y); // get tranlsated bounding box
    geo::Rectangle collided = level()->collision().collision_rectangle_if<Block>(hit_check,
        [this](const Block*b){ return (b->solid_index & solid_index) && b->block_type == Block::SOLID; } ); // get intersecting solid blocks

    collided.unite( level()->collision().collision_rectangle_if<Dynamic_Block>(hit_check,
        [this](const Dynamic_Block*b){ return (b->solid_index & solid_index) && b != this; } ) );


    if ( speed.y > 0 ) // when falling
    {
        collided.unite ( level()->collision().collision_rectangle_if<Block>( // get collision with platform blocks
            geo::Rectangle(hit_check.x,hit_check.y+hit_check.height-1,hit_check.width,1 ), // but only check a single pixel in height
            [this](const Block*b){ return (b->solid_index&solid_index) && b->block_type == Block::PLATFORM; } ) );
    }

    if ( collided.height > 0 && !math::float_zero(collided.height) ) // there has been a collision
    {
        if ( delta_y < 0 ) // jumping
        {
            hit |= CEILING;
            next_pos.y = collided.bottom()-local_bounding_rect().top(); // hit your head
        }
        else
        {
            next_pos.y = collided.top()-local_bounding_rect().bottom(); // hit the ground
            hit |= FLOOR;
        }

        speed.y = 0; // no more falling/flying
    }

    if ( !(hit&FLOOR) && level()->collision().has_objects_if<Block>(
            geo::Rectangle(hit_check.x,hit_check.y+hit_check.height-1,hit_check.width,1),
            [this](const Block*b){ return (b->solid_index&solid_index) && b->block_type == Block::NOFALL; }) )
        hit = FLOOR;
}

void Character::collision_slope(geo::Point& next_pos)
{
    // Checkfor slopes

    if ( speed.y < 0 ) // only when falling (ie don't prevent jumping)
        return;

    geo::Rectangle hit_check = bounding_rect().translated(next_pos-position()); // get next bounding box
    auto steps = level()->collision().get_objects_if<Block>( // get slope blocks
        geo::Rectangle(hit_check.x,hit_check.y+hit_check.height-1,hit_check.width,1 ),//  only check a single pixel in height
        [this](const Block*b){ return (b->solid_index&solid_index) && b->block_type == Block::SLOPE; } );
    if ( steps.empty() )
        return;

    auto highest = steps.begin(); // find the highest one
    for ( auto it = highest; it != steps.end(); ++it )
        if ( (*it)->y() < (*highest)->y() )
            highest = it;

    // place the character at the 45 degrees line from the bottom of the block
    next_pos.y = (*highest)->bounding_rect().bottom() - // start from the bottom of the block
        hit_check.intersection((*highest)->bounding_rect()).width - // move up the by same amount as the x intersection
        local_bounding_rect().bottom(); // and don't forget character's bounding box offset

    hit |= FLOOR;
    speed.y = 0; // no more falling
}

void Character::step()
{
    Sprite_Object::step();

    float fps = Game_Manager::instance().fps();
    geo::Point movespeed = speed / fps;
    geo::Point next_pos (x()+movespeed.x,y()+movespeed.y);
    hit = NOTHING;

    if ( solid_index )
    {
        collision_walls(next_pos);
        collision_floor_ceiling(next_pos);
        collision_slope(next_pos);
    }

    set_position ( next_pos ); // the position has been checked for collisions and adjusted accordingly

    /// \todo terminal velocity (?)
    speed.y += gravity / fps; // apply gravity
}


void Character::set_health(long h)
{
    eval::Context ctx(&event_context());
    ctx.set_temporary("health",h);
    trigger_event("health_changing",ctx);
    hp = (long)ctx.get_property("health");
}

long Character::health() const
{
    return hp;
}
