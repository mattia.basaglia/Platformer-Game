/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef GAME_OBJECT_FRAME_HPP
#define GAME_OBJECT_FRAME_HPP

#include "game/object/control.hpp"
#include "media/ui/frame.hpp"

namespace game {
namespace object {

class Frame : public Rectangle
{
    DECLARE_OBJECT(Frame,Rectangle)

protected:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;


private:
    std::string         frame_name;
    media::ui::Frame*   frame_ptr = nullptr;

public:
    using Rectangle::Rectangle;


    void step() override { Object::step(); }

    /**
     * \brief Set frame by name
     */
    void set_frame(const std::string& name);

    /**
     * \brief Get frame name
     */
    const std::string& frame() const;
};

} // namespace object
} // namespace game

#endif // GAME_OBJECT_FRAME_HPP
