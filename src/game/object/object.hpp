/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef OBJECT_HPP
#define OBJECT_HPP

#include <string>
#include <map>
#include <functional>

#include <boost/property_tree/ptree.hpp>

#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Audio/Sound.hpp>

#include "eval/context.hpp"
#include "game/timer.hpp"
#include "media/animation.hpp"
#include "misc/mirror.hpp"
#include "misc/geometry.hpp"
#include "misc/color.hpp"

namespace game {

class Level;
class Object_Layer;

/**
 * \brief Virtual class for dynamic game entities
 * \par Properties
 *      * \c number \b x        X coordinate (left coordinate of the entire object)
 *      * \c number \b y        Y coordinate (top coordinate of the entire object)
 *      * \c number \b width    (read-only) Bounding box width
 *      * \c number \b height   (read-only) Bounding box height
 *      * \c number \b center.x X coordinate of the bounding box center
 *      * \c number \b center.y Y coordinate of the bounding box center
 *      * \c string \b type     (read-only) Object type name
 * \par Events
 *      * \b spawn      Triggered when the object is created
 *      * \b level_end  Triggered when the current level ends
 *      * \b step       Triggered at each frame (Derived classes may disable this)
 * \par Methods
 *      * \b trigger(event) Triggers an event by name
 *      * \b kill()         Removes the object from the level
 */
class Object : public misc::Object_Source, public sf::Drawable
{

private:
    class Object_Private *d; ///< Pimpl
    PROPERTY_MAGIC_INHERIT(Object,Object_Source)

    friend class Level;
    void set_level(Level* level);
    void set_layer(Object_Layer* layer);
    Object_Layer* layer() const;

protected:
	misc::Value get_undeclared_property(const std::string& name) const override;
	bool has_undeclared_property(const std::string& name) const override;
	void set_undeclared_property(const std::string& name,const misc::Value& value) override;
	bool has_writable_undeclared_property(const std::string& name) const override;
	void get_all_undeclared_properties(misc::Properties& out, const std::string& prefix) const override;
	bool has_undeclared_method(const std::string& name) const override;
	misc::Value call_undeclared_method(const std::string& name, const std::vector<misc::Value>& args) override;

    void for_each_object( const std::function<void(const std::string& name,misc::Mirror*)>& func ) const override {}
    misc::Mirror* get_child(const std::string& name) const override { return nullptr; }

	void draw(sf::RenderTarget&, sf::RenderStates) const override{}

	/**
	 * \brief Update the level data structure when the bounding box has changed
	 */
	void update();

    /**
     * \brief Get the context used to evaluate events
     */
    eval::Context& event_context();
public:
/**
 * \name Construction
 * Methods handling object cration and destruction
 * \{
 */
    /**
     * \brief Constructor
     * \param name  Object name, best if unique and matching [a-z][-_.a-z0-9]*
     */
    explicit Object(const std::string& name="");
    Object(const Object&) = delete;
    virtual ~Object();

    /**
     * \brief Whether the object can be destroyed
     */
    virtual bool can_destroy() const { return true; }

    /**
     * \brief Level this object is a part of
     */
    Level* level() const;
///\}

/**
 * \name Attributes
 * Attributes accessible from scripts
 * \{
 */
    /**
     * \brief Name of the object
     */
    std::string name() const;

    /**
     * \brief X position
     */
    float x() const;

    /**
     * \brief Y position
     */
    float y() const;

    /**
     * \brief Get position
     */
    const geo::Point& position() const;

    /**
     * \brief Set position
     */
    void set_position(const geo::Point& p);

    /**
     * \brief Set position
     */
    void set_position(float x, float y);

    /**
     * \brief set X coordinate
     */
    void set_x(float x);

    /**
     * \brief set Y coordinate
     */
    void set_y(float y);

    /**
     * \brief Width of the bounding box
     */
    float width() const;

    /**
     * \brief Height of the bounding box
     */
    float height() const;

    /**
     * \brief Size of the bounding box
     */
    geo::Size size() const;

    /**
     * \brief Get bounding box (disregarding transformations
     */
    virtual geo::Rectangle local_bounding_rect() const = 0;

    /**
     * \brief Get the bound rect
     */
    virtual geo::Rectangle bounding_rect() const;

/// \}


/**
 * \name Logic
 * Methods handling object logic
 * \{
 */

    /**
     * \brief Perform a logic step
     */
    virtual void step();

    /**
     * \brief Trigger an object event
     *
     * If the object has a matching event, it will be executed
     */
    void trigger_event(const std::string& name);

    /**
     * \brief Trigger an object event
     *
     * If the object has a matching event, it will be executed with the given context
     */
    void trigger_event(const std::string& name, const eval::Context& context);


    /**
     * \brief Set the code for a event
     * \param name  Event name
     * \param code  Code to be executed
     */
    void set_event(const std::string& name, const std::string& code);

    /**
     * \brief Check whether the object has some code for the given event
     */
    bool has_event(const std::string& name) const;
/// \}


/**
 * \name Factory
 * Methods managing objects types and factory
 * \{
 */
    /**
     * \brief Register object factory constructor
     * \param name      Identifier for the object type (loaded from files)
     * \param create    Functor which creates an object given its name
     * \return Object type id
     */
    static int register_object_type(const std::string& name,
                                     const std::function<Object*(const std::string&)>& create);

    /**
     * \brief Create an object given its properties
     */
    static Object* create( const misc::Properties& properties );

    /**
     * \brief Cast to a different class of the Object hierarchy
     * \tparam T Pointer type
     * \return A casted object or nullptr if types mismatch
     */
    template<class T>
        T cast()
        {
            if ( !this || !has_type_id(std::remove_pointer<T>::type::static_type_id()) )
                return nullptr;
            return reinterpret_cast<T>(this);
        }


    /**
     * \brief Name of the object type
     * \note Must be unique
     */
    virtual std::string type() const = 0;
    /**
     * \brief Type identifier
     * \note Use the value returned by register_object_type()
     */
    virtual int type_id() const = 0;

    /**
     * \brief Get whether the object has the given type id in the hierarchy above it
     * \param id    Type id
     */
    virtual int has_type_id(int id) const { return id == static_type_id(); }

    /**
     * \brief Get object class type id, define the same method in derived classes (using REGISTER_OBJECT)
     */
    static int static_type_id();
/// \}

};

/**
 * \brief Register object class for object factory
 * \param Class     Class derived from game::Object
 * \param type_name Unique string used to identify the object type
 * \note  Use in the implementation file
 */
#define REGISTER_OBJECT(Class,type_name) \
    int Class::class_type_id = game::Object::register_object_type(type_name,\
            [](const std::string&object_name){return new Class(object_name);});\
    std::string Class::type() const { return type_name; } \
    int Class::static_type_id() { return Class::class_type_id; } \
    int Class::type_id() const { return Class::class_type_id; }

/**
 * \brief Add some boilerplate declarations for Object derived class
 * \param Class     Class derived from game::Object
 * \param parent    Parent class in the Object hierarchy
 */
#define DECLARE_OBJECT(Class,parent) \
    private: static int class_type_id; PROPERTY_MAGIC_INHERIT(Class,parent) \
    public: std::string type() const override; \
    static int static_type_id(); \
    int type_id() const override; \
    int has_type_id(int id) const override { return id == class_type_id || parent::has_type_id(id); }

namespace object {

/**
 * \brief Object displaying a sprite or animation
 * \par Properties
 *      * \c string \b tileset      Only writable at startup, tileset id to get a static image
 *      * \c number \b tileset.tile Tile id
 *      * \c string \b animation    Animation id (overrides tileset and tile)
 * \par Events
 *      * \b animation_end          Triggered when a noloop animation reaches the last frame
 */
class Sprite_Object : public Object
{
    DECLARE_OBJECT(Sprite_Object,Object)

private:
    class Sprite_Object_Private *d; ///< Pimpl

protected:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	virtual void set_properties(const misc::Properties& properties) override;

	/**
	 * \brief Called after the last frame of the animation
	 */
	virtual void animation_end() {}

	/**
	 * \brief Get a reference to the internal sprite
	 */
	sf::Sprite& sprite();

public:
    explicit Sprite_Object(const std::string& name = "");
    ~Sprite_Object();

    bool can_destroy() const override;

    /**
     * \brief Get bounding box (disregarding transformations)
     */
    geo::Rectangle local_bounding_rect() const override;

    /**
     * \brief Set the animation
     * \param animation     Pointer the the animation object
     * \param reset         Whether to restart from the first frame
     */
    void set_animation(const media::Animation* animation, bool reset=true);

    /**
     * \brief Current animation
     */
    const media::Animation* animation() const;

    /**
     * \brief Set the object sprite
     */
    void set_sprite(const sf::Sprite& s);

    void step() override;

    /**
     * \brief Get animation user flags
     */
    long animation_flags() const;
    /**
     * \brief Get whether the animation user flags match at least one of the given flags
     */
    bool animation_flags(long flags) const;

    /**
     * \brief Play a sound by file id
     */
    void play_sound(const std::string& sound);

    /**
     * \brief Get the color modulation
     */
    misc::Color color() const;

    /**
     * \brief Set the color modulation
     */
    void set_color( const misc::Color& col );
};

} // namespace object


/**
 * \brief Library of object properties
 */
class Object_Library
{
private:
    std::map<std::string,misc::Properties> objects; ///< Map library ID/object properties

public:

    /**
     * \brief Get object properties from the library
     * \param library_id        Object library id
     * \param[out] properties   Properties to populate
     * \param overwrite         Whether \c properties should have existing values to be overwritten
     */
    void get_properties(const std::string& library_id, misc::Properties& properties, bool overwrite) const;

    /**
     * \brief Get object properties from the library
     * \param tileset   Tileset name
     * \param tile_id   Tile id
     * \param[out] properties   Properties to populate
     * \param overwrite         Whether \c properties should have existing values to be overwritten
     */
    void get_properties(const std::string& tileset, long tile_id, misc::Properties& properties, bool overwrite) const;

    /**
     * \brief Create a new object from a library entry
     * \return A dynamic object or nullptr if it hasn't been found in the library
     */
    Object* create(const std::string& library_id) const;


    /**
     * \brief Load an object from a file to the library
     * \param file  File id
     */
    void load(const std::string& file);

    /**
     * \brief Register a set of properties as an object
     */
    void load( misc::Properties properties );

    /**
     * \brief Load one or multiple objects from a property tree
     */
    void load ( const boost::property_tree::ptree& ptree );

    /**
     * \brief Clear the library
     */
    void clear();

};

} // namespace game

#endif // OBJECT_HPP
