/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef EFFECT_HPP
#define EFFECT_HPP


#include "object.hpp"

namespace game {
namespace object {

/**
 * \brief Special effect, plays an animation and dies
 * \par Properties
 *      * \c string \b sound    (write-only) sound to play
 */
class Effect : public Sprite_Object
{
    DECLARE_OBJECT(Effect,Sprite_Object)

protected:
    void animation_end() override;

public:
    Effect(const std::string& name) : Sprite_Object(name) {}

    void step() override;
};

/**
 * \brief An effect that moves around
 */
class Debris : public Effect
{
    DECLARE_OBJECT(Debris,Effect)

public:
    float gravity = 0;  ///< y Acceleration

protected:
    geo::Polar_Vector velocity; ///< Moving direction

public:
    Debris(const std::string& name);

    void step() override;

    /**
     * \brief Get speed in pixels per second
     */
    float speed() const;

    /**
     * \brief Set speed in pixels per second
     */
    void set_speed(float speed);

    /**
     * \brief Moving direction in degrees
     */
    long direction() const;

    /**
     * \brief Set moving direction in degrees
     */
    void set_direction(long angle);
};

}} // namespace game::object

#endif // EFFECT_HPP
