/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef GAME_OBJECT_TEXT_HPP
#define GAME_OBJECT_TEXT_HPP

#include "object.hpp"
#include "media/ui/shaded_text.hpp"
#include "media/ui/frame.hpp"

namespace game {
namespace object {

/**
 * \b An object displaying some text surrounded by a frame
 * \par Properties
 *      * \c number \b padding  Distance from the frame border to the text
 *      * \c string \b frame    Identifier for the frame style
 *      * \c string \b text     String to display
 *      * \b text.*             See media::ui::Shaded_Text
 */
class Textbox : public Object
{
    DECLARE_OBJECT(Textbox,Object)


protected:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    void for_each_object( const std::function<void(const std::string& name,misc::Mirror*)>& func ) const override;
    misc::Mirror* get_child(const std::string& name) const override;

private:
    std::string         frame_name;
    media::ui::Frame*   frame_ptr = nullptr;

public:
    media::ui::Shaded_Text  text;
    long                    padding = 16;

    using Object::Object;

    geo::Rectangle local_bounding_rect() const override;

    /**
     * \brief Set frame by name
     */
    void set_frame(const std::string& name);

    /**
     * \brief Get frame name
     */
    const std::string& frame() const;
};

} // namespace object
} // namespace game

#endif // GAME_OBJECT_TEXT_HPP
