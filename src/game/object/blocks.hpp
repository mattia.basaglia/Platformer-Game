/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef SOLID_BLOCKS_HPP
#define SOLID_BLOCKS_HPP

#include "control.hpp"
#include "character.hpp"

namespace game {
namespace object {

/**
 * \brief More or less solid blocks
 */
class Block : public Rectangle
{
    DECLARE_OBJECT(Block,Rectangle)

public:
    /**
     * \brief Type of the block
     * \note Block doesn't perform any kind of logic processing, that's
     *  deferred to the objects that collide with the blocks
     * \see game::object::Character for a class that uses this
     */
    enum Block_Type
    {
        SOLID,      ///< Fully solid block
        PLATFORM,   ///< Only solid from above
        SLOPE,      ///< Allows a smooth diagonal movement
        NOFALL,     ///< Doesn't prevent de user from moving but makes it think it's on the ground
        CUSTOM,     ///< Custom rules
    };

    long solid_index = 1;   ///< Can be used to allow certain objects to pass trhough/be blocked only by certain blocks
    long block_type = SOLID;///< Type of the block

public:
    Block(const std::string& name) : Rectangle(name) {}

    void step() override{}

    /**
     * \brief Convert a string into a Block_Type
     * \param type  One of Block_Type enumerations expressed in lowercase
     *  Unknown values will result in CUSTOM
     */
    void set_block_type_string(const std::string& type);

    /**
     * \brief Get string representing the block type
     */
    std::string block_type_string() const;

};

/**
 * \brief Blocks that have an image and react to the environment
 */
class Dynamic_Block : public Character
{
    DECLARE_OBJECT(Dynamic_Block,Character)

public:
    Dynamic_Block(const std::string& name) : Character(name) {}

    void step() override;

};


}} // namespace game::object

#endif // SOLID_BLOCKS_HPP
