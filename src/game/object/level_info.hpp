/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef LEVEL_INFO_HPP
#define LEVEL_INFO_HPP

#include "object.hpp"
#include "eval/parser.hpp"
#include "eval/script_snippet.hpp"

namespace game {
namespace object {

/**
 * \brief Player spawn location
 * \par Properties
 *      * \c string \b from         ID of the level that you must be coming from to accept spawns
 *      * \c string \b condition    Condition string to check whether the spawn point is active (this)
 *      * \c string \b action       Code to execute when an object is spawned (this,other=spawned object)
 */
class Spawn : public Sprite_Object
{
    DECLARE_OBJECT(Spawn,Sprite_Object)

    mutable eval::Context context;

public:
    eval::Script_Snippet condition; ///< Can spawn here only if this condition is satisfied
    eval::Script_Snippet action;    ///< Snippet to perform on spawn
    std::string from; ///< Only spawn here when coming from this level

    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override{}

public:
    Spawn(const std::string& name);

    /**
     * \brief Whether can spawn from this level
     */
    bool can_spawn(const std::string& from) const;

    void step() override{}

    /**
     * \brief Setup an object to be spawned here
     */
    void setup(Object* o) const;
};

}} // namespace game::object
#endif // LEVEL_INFO_HPP
