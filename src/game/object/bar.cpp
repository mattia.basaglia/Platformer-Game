/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "game/object/bar.hpp"
#include "misc/geometry_convert.hpp"
#include "media/media_manager.hpp"

using namespace game::object;

REGISTER_OBJECT(Bar,"bar")

DECLARE_PROPERTY_ATTRIBUTE(Bar,value)
DECLARE_PROPERTY_ATTRIBUTE(Bar,max)

DECLARE_PROPERTY(Bar,outline,outline,set_outline)
DECLARE_PROPERTY(Bar,fill,fill,set_fill)

void Bar::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform.translate(geo::conv::point(position()));
    if ( fill_ptr )
        fill_ptr->draw(geo::Point(),value,target,states);
    if ( outline_ptr )
        outline_ptr->draw(geo::Point(),max,target,states);
}

geo::Rectangle Bar::local_bounding_rect() const
{
    geo::Size sz;
    if ( outline_ptr && outline_ptr->tileset() )
    {
        sz = outline_ptr->tileset()->tile_size(outline_ptr->center());
        sz.width *= max;
    }
    else if ( fill_ptr && fill_ptr->tileset() )
    {
        sz = fill_ptr->tileset()->tile_size(fill_ptr->center());
        sz.width *= value;
    }
    return geo::Rectangle(position(),sz);
}

void Bar::set_fill(const std::string& name)
{
    fill_name.clear();
    if ( (fill_ptr = media::Media_Manager::instance().bar(name)) )
        fill_name = name;
}

const std::string& Bar::fill() const
{
    return fill_name;
}

void Bar::set_outline(const std::string& name)
{
    outline_name.clear();
    if ( (outline_ptr = media::Media_Manager::instance().bar(name)) )
        outline_name = name;
}

const std::string& Bar::outline() const
{
    return outline_name;
}
