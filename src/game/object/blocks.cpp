/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "blocks.hpp"
#include "game/level.hpp"


using namespace game::object;

REGISTER_OBJECT(Block,"block")

DECLARE_PROPERTY_ATTRIBUTE(Block,solid_index)
DECLARE_PROPERTY_ATTRIBUTE(Block,block_type)
DECLARE_PROPERTY(Block,block_type_string,block_type_string,set_block_type_string)


void Block::set_block_type_string(const std::string& type)
{
    if ( type == "solid" )
        block_type = SOLID;
    else if ( type == "platform" )
        block_type = PLATFORM;
    else if ( type == "slope" )
        block_type = SLOPE;
    else if ( type == "nofall" )
        block_type = NOFALL;
    else
        block_type = CUSTOM;
}

std::string Block::block_type_string() const
{
    switch ( block_type )
    {
        case SOLID:
            return "solid";
        case PLATFORM:
            return "platform";
        case SLOPE:
            return "slope";
        case NOFALL:
            return "nofall";
        default:
            return "custom";
    }
}


REGISTER_OBJECT(Dynamic_Block,"dynamic_block")

void Dynamic_Block::step()
{
    Character::step();
    /// \todo push?
    if ( get_property("health") <= 0 )
        level()->remove_object(this);
}
