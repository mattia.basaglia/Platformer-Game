/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "effect.hpp"
#include "game/level.hpp"
#include "media/media_manager.hpp"
#include "game/game_manager.hpp"
#include "misc/math_utils.hpp"

using namespace game::object;

REGISTER_OBJECT(Effect,"effect")

void Effect::animation_end()
{
    set_animation(nullptr);
    level()->remove_object(this);
}

void Effect::step()
{
    Sprite_Object::step();
    if ( !animation() && can_destroy() )
        level()->remove_object(this);
}

REGISTER_OBJECT(Debris,"debris")
DECLARE_PROPERTY(Debris,speed,speed,set_speed)
DECLARE_PROPERTY(Debris,direction,direction,set_direction)
DECLARE_PROPERTY_ATTRIBUTE(Debris,gravity)

Debris::Debris(const std::string& name) : Effect(name)
{
    sprite().setRotation(math::random(360));
}

void Debris::step()
{
    Effect::step();
    float fps = Game_Manager::instance().fps();
    set_position(position()+velocity.point()/fps);
    velocity += geo::Point(0,gravity/fps);
    if ( !Game_Manager::instance().video().camera().game_area().intersects(bounding_rect()) )
        level()->remove_object(this);

    sprite().setRotation(sprite().getRotation()+360/fps);

}

float Debris::speed() const
{
    return velocity.length;
}

void Debris::set_speed(float speed)
{
    velocity.length = speed;
}

long Debris::direction() const
{
    return math::rad2deg(velocity.angle);
}

void Debris::set_direction(long angle)
{
    velocity.angle = math::deg2rad(angle);
}
