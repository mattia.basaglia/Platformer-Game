/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "game/object/text.hpp"
#include "misc/geometry_convert.hpp"
#include "media/media_manager.hpp"

using namespace game::object;


REGISTER_OBJECT(Textbox,"textbox")

DECLARE_PROPERTY_ATTRIBUTE(Textbox,padding)

DECLARE_PROPERTY(Textbox,frame,frame,set_frame)

DECLARE_PROPERTY_SET_FUNCTOR(Textbox,text,[](Textbox&t,const misc::Value& v) { t.text.set_text((std::string)v); })
DECLARE_PROPERTY_GET_FUNCTOR(Textbox,text,[](const Textbox &t) { return misc::Value(t.text.text()); })


void Textbox::for_each_object( const std::function<void(const std::string& name,misc::Mirror*)>& func ) const
{
    func("text",const_cast<media::ui::Shaded_Text*>(&text));
}
misc::Mirror* Textbox::get_child(const std::string& name) const
{
    if ( name == "text" )
        return const_cast<media::ui::Shaded_Text*>(&text);
    return nullptr;
}

void Textbox::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform.translate(geo::conv::point(position()));

    geo::Rectangle rec = local_bounding_rect();

    if ( frame_ptr )
        rec = frame_ptr->draw(rec,target,states);


    geo::Rectangle textrect = text.bounding_rect();
    states.transform.translate(geo::conv::point( rec.center()-textrect.center() ));

    target.draw(text,states);;
}


geo::Rectangle Textbox::local_bounding_rect() const
{
    return text.bounding_rect();
}

void Textbox::set_frame(const std::string& name)
{
    frame_name = name;
    if ( !(frame_ptr = media::Media_Manager::instance().frame(frame_name)) )
        frame_name.clear();
}

const std::string& Textbox::frame() const
{
    return frame_name;
}
