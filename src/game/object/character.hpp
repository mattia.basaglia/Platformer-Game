/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef CHARACTER_HPP
#define CHARACTER_HPP

#include "object.hpp"
#include "misc/geometry.hpp"

namespace game {
namespace object {

/**
 * \brief Object that moves around and can be killed
 * \par Properties
 *      * \c float  \b speed.x      X speed component (pixels per second)
 *      * \c float  \b speed.y      Y speed component (pixels per second)
 *      * \c number \b gravity      Gravity acceleration (pixels per second squared)
 *      * \c number \b solid_index  Flags to match which blocks are considered solid
 *      * \c number \b target_index Bit set used to identify what can harm this character
 *      * \c boolean \b air         (Read only) if true the character is not touching the ground
 *      * \c number \b health       Object's health
 * \par Events
 *      * \b health_changing(health)       Triggered then the health is going to change.
 *                                          (You can change the parameter health to affect the change)
 */
class Character : public Sprite_Object
{
    DECLARE_OBJECT(Character,Sprite_Object)

private:
    void collision_walls(geo::Point& next_pos);
    void collision_floor_ceiling(geo::Point& next_pos);
    void collision_slope(geo::Point& next_pos);

public:
    long gravity = 0;    ///< Gravity acceleration
    geo::Point speed;    ///< Speed in pixels/second
    long solid_index = 1;///< Only collide with blocks that with matching solid_index ( matching with bitwise AND )
    long target_index =1;///< Only damaged by enemies with matching damage_index ( matching with bitwise AND )

protected:
    enum Hit_Side {
        NOTHING = 0,
        FLOOR   = 1,
        CEILING = 2,
        LEFT    = 4,
        RIGHT   = 8
    };
    long hit = NOTHING; ///< Sides on which a wall collison has occurred
    long hp = 0;        ///< Object's health

public:
    Character(const std::string& name) : Sprite_Object(name) {}

    void step() override;

    /**
     * \brief Whether the character is currently in the air
     */
    bool air() const { return !(hit&FLOOR); }


    /**
     * \brief Set object's health
     */
    void set_health(long h);

    /**
     * \brief Object's health
     */
    long health() const;
};

}} // namespace game::object

#endif // CHARACTER_HPP
