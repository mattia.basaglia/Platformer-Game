/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef PROJECTILE_HPP
#define PROJECTILE_HPP

#include "misc/color.hpp"
#include "object.hpp"
#include "eval/script_snippet.hpp"
#include "game/timer.hpp"

namespace game {
namespace object {

/**
 * \brief A projectile that streches as far as it can go (horizontally)
 */
class Laser : public Object
{
    DECLARE_OBJECT(Laser,Object)
public:
    misc::Color          color =misc::Color("white");///< Color of the laser
    long                 height = 16;    ///< Height of the laser
    long                 solid_index=1;  ///< Which kind of objects flags will block it
    long                 damage_index=1; ///< Which kind of objects will be damaged by this (only those with a matching target_index)
    misc::duration       duration {1000};///< Duration in milliseconds
    eval::Script_Snippet hit_target;
    eval::Script_Snippet hit_wall;
private:
    geo::Size  size;            ///< Actual size of the laser
    Timer      death_time;      ///< Death time

    bool fit_size();

protected:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

public:
    Laser(const std::string& name);

    geo::Rectangle local_bounding_rect() const override { return geo::Rectangle({0,0},size); }

    void step() override;
};

}} // namespace game::object

#endif // PROJECTILE_HPP
