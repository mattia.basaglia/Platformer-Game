/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "game/object/frame.hpp"
#include "media/media_manager.hpp"
#include "misc/geometry_convert.hpp"

using namespace game::object;


REGISTER_OBJECT(Frame,"frame")
DECLARE_PROPERTY(Frame,frame,frame,set_frame)


void Frame::set_frame(const std::string& name)
{
    frame_name = name;
    if ( !(frame_ptr = media::Media_Manager::instance().frame(frame_name)) )
        frame_name.clear();
}

const std::string& Frame::frame() const
{
    return frame_name;
}

void Frame::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform.translate(geo::conv::point(position()));
    frame_ptr->draw(local_bounding_rect(),target,states);
}
