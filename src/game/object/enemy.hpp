/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef GAME_OBJECT_ENEMY_HPP
#define GAME_OBJECT_ENEMY_HPP

#include "character.hpp"
#include "eval/script_snippet.hpp"

namespace game {
namespace object {

/**
 * \brief An enemy
 */
class Enemy : public Character
{
    DECLARE_OBJECT(Enemy,Character)

    eval::Context       context {&event_context()};

public:
    eval::Script_Snippet collision {"other.health -= this.damage"}; ///< Action executed when it hits the player
    eval::Script_Snippet requires;  ///< Spawn condition
    long damage_index = 1024;///< Only damage characters with matching target_index ( matching with bitwise AND )

    using Character::Character;

    void step() override;


    /**
     * \brief Perform the hit action
     */
    void collide(Object* victim);
};

} // namespace object
} // namespace game

#endif // GAME_OBJECT_ENEMY_HPP
