/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "playable.hpp"
#include "media/media_manager.hpp"
#include "level_info.hpp"
#include "misc/math_utils.hpp"
#include "collectible.hpp"
#include "game/level.hpp"
#include "game/game_manager.hpp"

#include <SFML/Window/Keyboard.hpp>

using namespace game::object;

REGISTER_OBJECT(Ability,"ability")
DECLARE_PROPERTY_ATTRIBUTE(Ability,condition)
DECLARE_PROPERTY_GETTER(Ability,active,active)

Ability::Ability(const std::string& name)
    : Object(name), context(&event_context())
{
    context.set_object("ability",this);
}

void Ability::trigger_event_for(const std::string&name, Object* source)
{
    context.set_object("this",source);
    trigger_event(name,context);
}

void Ability::execute(Object* source)
{
    context.set_object("this",source);

    if ( active() )
    {
        trigger_event("execute",context);
    }
}

bool Ability::active() const
{
    return condition.empty() || condition.evaluate(context);
}


REGISTER_OBJECT(Playable,"playable")

DECLARE_PROPERTY_ATTRIBUTE(Playable,active)
DECLARE_PROPERTY_RENAME(Playable,"move.direction",direction,direction,set_direction)
DECLARE_PROPERTY_RENAME(Playable,"animation.prefix",animation_prefix,animation_prefix,set_animation_prefix)
DECLARE_PROPERTY_RENAME(Playable,"animation.suffix",animation_suffix,animation_suffix,set_animation_suffix)
DECLARE_PROPERTY_SETTER(Playable,ability,set_ability)
DECLARE_PROPERTY_GET_FUNCTOR(Playable,ability,
     [](const Playable&o){ return o.ability() ? o.ability()->name() : ""; } )
DECLARE_PROPERTY(Playable,invincibility,invincibility,set_invincibility)


DECLARE_METHOD_VOID_NOARG(Playable,respawn)

namespace {
/**
 * \brief Just some named constants for Playable::jumping_status
 */
enum Jumping_Status
{
    ON_THE_GROUND   = 0x00,
    BUSY            = 0x01,
    IN_THE_AIR      = 0x10,
};
} // namespace

/// \pimpl{Playable}
struct game::object::Playable_Private
{
    std::string animation_prefix;   ///< Animation id prefix
    std::string animation_suffix;   ///< Animation id suffix
    bool        facing_right=true;  ///< Facing direction (for animations)
    std::vector<Ability*>abilities; ///< Character abilities
    unsigned    active_ability = 0; ///< Index inside abilities
    game::Timer invincibility;      ///< Time for which the player will be invicible
};


/// \todo parse "active" as an expression?
Playable::Playable(const std::string& name)
    : Character(name), d(new Playable_Private)
{
    d->animation_suffix = "breath";
    d->invincibility.ring = [this]() { trigger_event("invincibility_end"); };
}

Playable::~Playable()
{
    for ( auto ab : d->abilities )
        delete ab;
    delete d;
}

void Playable::update_animation()
{
    auto anim = media::Media_Manager::instance().animation(d->animation_prefix+d->animation_suffix);
    if ( anim )
        set_animation(anim);
}

void Playable::set_animation_suffix(const std::string&suffix)
{
    d->animation_suffix = suffix;
    update_animation();
}

const std::string& Playable::animation_suffix() const
{
    return d->animation_suffix;
}


void Playable::set_animation_prefix(const std::string&prefix)
{
    d->animation_prefix = prefix;
    update_animation();
}

const std::string& Playable::animation_prefix() const
{
    return d->animation_prefix;
}

void Playable::walk(bool right)
{
    if ( !(hit&(LEFT|RIGHT)) && !animation_flags(IN_THE_AIR) )
        trigger_event("walking");

    if ( right ^ d->facing_right )
        trigger_event("turn");
    else
        trigger_event("move");
}

void Playable::step()
{
    // Ensure we have the correct animation running
    if ( !animation() )
    {
        update_animation();
    }

    // handle collisions and stuff
    Character::step();

    // collect items
    for ( Collectible* c : level()->collision().get_objects<Collectible>(bounding_rect()) )
    {
        c->collect(this);
    }

    if ( animation_flags(IN_THE_AIR) && !animation_flags(BUSY)
            && (hit&FLOOR) && speed.y >= 0 ) // we were falling and hit the ground
    {
        trigger_event("landing");
    }
    else if ( !(hit&FLOOR) && !animation_flags(BUSY|IN_THE_AIR))  // we were on the ground or flapping an now we're in the air
    {
        trigger_event("falling");
    }

    // react to user input
    if ( active )
    {
        if ( !animation_flags(BUSY) ) // can move left/right only if it's not on a jumping animation
        {
            if  ( sf::Keyboard::isKeyPressed(sf::Keyboard::Right) )
                walk(true);
            else if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Left) )
                walk ( false );
            else if ( !math::float_zero(speed.x) )
            {
                trigger_event("stop_moving");
            }


            if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && ability() )
            {
                ability()->execute(this);
            }
            else if ( sf::Keyboard::isKeyPressed(sf::Keyboard::Up) &&
                    !animation_flags(BUSY) && speed.y > 0 )
            {
                trigger_event("jump");
            }
        }

        if ( hit&(RIGHT|LEFT) )
            trigger_event("hit_wall");


        // camera should follow us
        Game_Manager::instance().video().camera().target.x = bounding_rect().center().x;
        Game_Manager::instance().video().camera().target.y = bounding_rect().top();
    }

    if ( ability() )
        ability()->step();

    d->invincibility.step();
}


void Playable::set_direction(const std::string& name)
{
    std::string next_direction;

    if ( name == "right" )
    {
        next_direction = name;
        d->facing_right = true;
    }
    else if ( name == "left" )
    {
        next_direction = name;
        d->facing_right = false;

    }
    else
    {
        next_direction = direction() == "left" ? "right" : "left";
        d->facing_right = !d->facing_right;
    }

    std::string prev_direction = d->facing_right ? "left" : "right";

    std::size_t pos = d->animation_prefix.find(prev_direction);
    if ( pos != std::string::npos )
    {
        d->animation_prefix.replace(pos,prev_direction.size(),next_direction);
        update_animation();
    }
}

std::string Playable::direction() const
{
    return d->facing_right ? "right" : "left";
}


void Playable::respawn()
{
    auto spawns = level()->collision().get_objects_if<object::Spawn>(level()->collision().get_area(),
        [this](const object::Spawn*sp) { return sp->can_spawn(level()->id()); } );

    if ( !spawns.empty() )
    {
        int i = math::random(0,spawns.size()-1);
        spawns[i]->setup(this);
        trigger_event("respawned");
    }
}



Ability* Playable::find_ability(const std::string& name) const
{
    for ( Ability* a : d->abilities )
        if ( a->name() == name )
            return a;
    return nullptr;
}

misc::Value Playable::get_undeclared_property(const std::string& name) const
{
    if ( name == "abilities" )
    {
        std::string names;
        for ( Ability* ab : d->abilities )
        {
            if ( !names.empty() )
                names.push_back(',');
            names += ab->name();
        }
        return names;
    }
    return Character::get_undeclared_property(name);
}
bool Playable::has_undeclared_property(const std::string& name) const
{
    if ( name == "abilities" )
        return true;
    return Character::has_undeclared_property(name);
}
void Playable::set_undeclared_property(const std::string& name,const misc::Value& value)
{
    std::string root, item;
    misc::split_identifier(name,root,item);

    if ( root == "abilities" )
    {
        if ( item.empty() || find_ability(item) )
            return;

        Ability* ability = Game_Manager::instance().object_library().create(item)->cast<Ability*>();
        if ( ability )
            d->abilities.push_back(ability);

        return;
    }

    return Character::set_undeclared_property(name,value);
}
bool Playable::has_writable_undeclared_property(const std::string& name) const
{
    if ( misc::starts_with(name,"abilities") )
        return true;
    return Character::has_writable_undeclared_property(name);
}

void Playable::for_each_object( const std::function<void(const std::string& name,misc::Mirror*)>& func ) const
{
    for ( Ability* a : d->abilities )
        func("ability."+a->name(),a);

}

misc::Mirror* Playable::get_child(const std::string& name) const
{
    std::string root, item;
    misc::split_identifier(name,root,item);
    if ( !item.empty() && root == "ability" )
    {
        misc::split_identifier(item,root,item);
        if ( Ability* selected = find_ability(root) )
            return selected;
    }
    return nullptr;
}
bool Playable::is_container(const std::string& name) const
{
    return name == "ability";
}

Ability* Playable::ability() const
{
    if ( d->active_ability < d->abilities.size() )
        return d->abilities[d->active_ability];
    return nullptr;
}

void Playable::animation_end()
{
    if ( ability() )
        ability()->trigger_event_for("animation_end",this);
}


void Playable::set_ability(const std::string& name)
{
    for ( unsigned i = 0; i < d->abilities.size(); ++i )
        if ( d->abilities[i]->name() == name )
        {
            d->active_ability = i;
            return;
        }
}

const misc::duration& Playable::invincibility() const
{
    return d->invincibility.time_left;
}

void Playable::set_invincibility(const misc::duration& time)
{
    d->invincibility.start(time);
    if ( !d->invincibility.ready() )
        trigger_event("invincibility_begin");
}
