/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef CONTROL_HPP
#define CONTROL_HPP

#include "object.hpp"
#include "eval/script_snippet.hpp"

namespace game {
namespace object {

/**
 * \brief Simple rectangular Object
 * \par Properties
 *      * \c number \b width    Width (unlike Object.width, this is writable)
 *      * \c number \b height   Height (unlike Object.height, this is writable)
 * \note It disables Object::step
 */
class Rectangle : public Object
{
    DECLARE_OBJECT(Rectangle,Object)

private:
    geo::Size size;

protected:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override{}

public:
    Rectangle(const std::string& name) : Object(name), size(16,16) {}

    void step() override{}

    long width() const { return size.width; }
    long height() const { return size.height; }
    void set_width(long w);
    void set_height(long h);

    geo::Rectangle local_bounding_rect() const override { return geo::Rectangle({0,0},size); }
};

/**
 * \brief An object which executes a script continuosly
 * \par Properties
 *      * \c string \b code     Script to execute
 *      * \c string \b source   (Write-only) source file for the script
 */
class Script : public Object
{
    DECLARE_OBJECT(Script,Object)

protected:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override{}

public:
    eval::Script_Snippet code;

    Script(const std::string& name);

    void step() override;

    geo::Rectangle local_bounding_rect() const override { return geo::Rectangle(); }

    /**
     * \brief Load script from file
     * \param file  Script file id (path relative to data)
     */
    void load(const std::string& file);
};

}} // namespace game::object

#endif // CONTROL_HPP
