/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "collectible.hpp"
#include "game/level.hpp"
#include "game/game_manager.hpp"
#include "misc/math_utils.hpp"

using namespace game;
using namespace game::object;

REGISTER_OBJECT(Collectible,"collectible")
DECLARE_PROPERTY_ATTRIBUTE(Collectible,action)
DECLARE_PROPERTY_ATTRIBUTE(Collectible,requires)
DECLARE_PROPERTY_ATTRIBUTE_RENAME_EX(Collectible,"float.speed.x",float_speed.x,float_speed_x)
DECLARE_PROPERTY_ATTRIBUTE_RENAME_EX(Collectible,"float.speed.y",float_speed.y,float_speed_y)
DECLARE_PROPERTY_ATTRIBUTE_RENAME_EX(Collectible,"float.radius",float_radius,float_radius)

Collectible::Collectible(const std::string& name)
    : Sprite_Object(name), context(&event_context())
{}

void Collectible::collect(Object* collector)
{
    context.set_object("other",collector);
    if ( requires.empty() || requires.evaluate(context) )
    {
        action.evaluate(context);
        level()->remove_object(this);
    }
}


void Collectible::step()
{
    Sprite_Object::step();

    if ( float_radius )
    {
        float t = Game_Manager::instance().time().count() / 1000. * math::tau;
        sprite().setPosition(cos(t*float_speed.x)*float_radius/2,sin(t*float_speed.y)*float_radius);
    }
}
