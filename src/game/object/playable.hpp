/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef PLAYABLE_HPP
#define PLAYABLE_HPP

#include "character.hpp"
#include "eval/script_snippet.hpp"

namespace game {
namespace object {

/**
 * \brief Player active ability
 * \par Attributes
 *      * \c string \b condition    Code to evaluate to see if this ability can be used
 *      * \c boolean \b active      (Read only) Whether the condition is satisfied
 */
class Ability : public Object
{
    DECLARE_OBJECT(Ability,Object)

private:
    eval::Context   context;

public:
    eval::Script_Snippet condition;

public:
    Ability(const std::string& name);

    geo::Rectangle local_bounding_rect() const override { return geo::Rectangle(); }

    /**
     * \brief Trigger an event for a given object
     *
     * Will executed an event defined by the ability with \b this == \c source
     * and \b ability == \c this (bold are script symbols)
     */
    void trigger_event_for(const std::string&name, Object* source);

    /**
     * \brief Execute the ability for the given object
     */
    void execute(Object* source);

    /**
     * \brief Whether the condition is satisfied
     */
    bool active() const;

};

/**
 * \brief Character controlled by the player
 * \par Properties
 *      * \c boolean \b active          Whether it reacts to input
 *      * \c string \b move.direction   Facing direction ("left" or "right")
 *      * \c string \b animation.prefix Animation id prefix
 *      * \c string \b animation.suffix Animation id suffix
 *      * \c string \b sound            (Write-only) Sound to be played
 *      * \c string \b ability          Name of the current ability
 *      * \b ability.(name).*           Property for ability (name)
 *      * \c array  \b abilities        List of abilities
 *      * \c number \b invincibility    Number of milliseconds the object is going to be invincible
 * \par Events
 *      * \b landing                Triggered when the character touches the ground
 *      * \b falling                Triggered when the object starts falling
 *      * \b hit_wall               Triggered when the object hits a wall
 *      * \b stop_moving            Triggered when the user stops pressing a movement key
 *      * \b walking                Triggered when the object is walking (moving on the ground)
 *      * \b jump                   Triggered when the object must perform a jump
 *      * \b turn                   Triggered when the object must turn around
 *      * \b respawned              Triggered after the object has respawned
 *      * \b invincibility_begin    Triggered when the object becomes invincible
 *      * \b invincibility_end      Triggered when invinciblity ends
 * \par Methods
 *      * \b respawn()      Moves the object to the closest spawn location and triggers \c respawned
 */
class Playable : public Character
{
    DECLARE_OBJECT(Playable,Character)

private:
    Ability* find_ability(const std::string& name) const;

public:
    bool        active = true;    ///< Whether it reacts to input

protected:
    class Playable_Private *d;

    void walk(bool right);

    /**
     * \brief Set animation from prefix+suffix
     */
    void update_animation();

	misc::Value get_undeclared_property(const std::string& name) const override;
	bool has_undeclared_property(const std::string& name) const override;
	void set_undeclared_property(const std::string& name,const misc::Value& value) override;
	bool has_writable_undeclared_property(const std::string& name) const override;

	void for_each_object( const std::function<void(const std::string& name,misc::Mirror*)>& func ) const override;
	misc::Mirror* get_child(const std::string& name) const override;
	bool is_container(const std::string& name) const override;

	void animation_end() override;

public:
    Playable(const std::string& name);
    ~Playable();

    void step() override;

    /**
     * \brief Set animation suffix (and update animation accordingly)
     */
    void set_animation_suffix(const std::string&suffix);

    /**
     * \brief Get the animation suffix
     */
    const std::string& animation_suffix() const;

    /**
     * \brief Set animation prefix (and update animation accordingly)
     */
    void set_animation_prefix(const std::string&prefix);

    /**
     * \brief Get the animation prefix
     */
    const std::string& animation_prefix() const;

    /**
     * \brief Set facting direction
     * \param name  Direction name (left or right)
     */
    void set_direction(const std::string& name="");

    /**
     * \brief Get facing direction name
     */
    std::string direction() const;

    /**
     * \brief Move the character to the closest matching spawn object
     */
    void respawn();

    /**
     * \brief Get current ability
     */
    Ability* ability() const;

    /**
     * \brief Set current ability by name
     */
    void set_ability(const std::string& name);

    /**
     * \brief Get invincibility time
     */
    const misc::duration& invincibility() const;
    /**
     * \brief Set invincibility time
     */
    void set_invincibility(const misc::duration& time);
};

}} // namespace game::object

#endif // PLAYABLE_HPP
