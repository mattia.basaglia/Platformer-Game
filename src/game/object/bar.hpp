/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef GAME_OBJECT_BAR_HPP
#define GAME_OBJECT_BAR_HPP

#include "game/object/object.hpp"
#include "media/ui/bar.hpp"

namespace game {
namespace object {

/**
 * \b An object displaying a completion bar
 */
class Bar : public Object
{

    DECLARE_OBJECT(Bar,Object)

private:
    std::string     outline_name;
    media::ui::Bar* outline_ptr = nullptr;
    std::string     fill_name;
    media::ui::Bar* fill_ptr = nullptr;

protected:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

public:
    long max = 0;
    long value = 0;

    using Object::Object;

    geo::Rectangle local_bounding_rect() const override;

    /**
     * \brief Set fill bar frame by name
     */
    void set_fill(const std::string& name);

    /**
     * \brief Get fill bar name
     */
    const std::string& fill() const;

    /**
     * \brief Set outline bar frame by name
     */
    void set_outline(const std::string& name);

    /**
     * \brief Get outline bar name
     */
    const std::string& outline() const;
};

} // namespace object
} // namespace game

#endif // GAME_OBJECT_BAR_HPP
