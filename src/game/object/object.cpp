/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "object.hpp"

#include <string>

#include <boost/property_tree/xml_parser.hpp>

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Sprite.hpp>

#include "eval/parser.hpp"
#include "game/game_manager.hpp"
#include "game/level.hpp"
#include "media/media_manager.hpp"
#include "media/tileset.hpp"
#include "misc/game_value.hpp"
#include "misc/logger.hpp"
#include "misc/geometry_convert.hpp"
#include "misc/ptree_utils.hpp"

using namespace game;
using namespace game::object;

namespace {
    DECLARE_PROPERTY(Object,x,x,set_x)
    DECLARE_PROPERTY(Object,y,y,set_y)
    DECLARE_PROPERTY_GETTER(Object,type,type)
    DECLARE_PROPERTY_GETTER(Object,height,height)
    DECLARE_PROPERTY_GETTER(Object,width,width)
    DECLARE_PROPERTY_GETTER(Object,name,name)
    DECLARE_PROPERTY_GET_FUNCTOR_EX(Object,"center.x",center_x,[](const Object& obj){return (long)obj.bounding_rect().center().x;} )
    DECLARE_PROPERTY_GET_FUNCTOR_EX(Object,"center.y",center_y,[](const Object& obj){return (long)obj.bounding_rect().center().y;} )
    DECLARE_PROPERTY_SET_FUNCTOR_EX(Object,"center.x",center_x,[](Object& obj,const misc::Value& v)
        { obj.set_x ( long(v) - obj.local_bounding_rect().center().x );  } )
    DECLARE_PROPERTY_SET_FUNCTOR_EX(Object,"center.y",center_y,[](Object& obj,const misc::Value& v)
        { obj.set_y ( long(v) - obj.local_bounding_rect().center().y );  } )

    DECLARE_METHOD_FUNCTOR(Object,trigger,[](Object&obj,const std::vector<misc::Value>& args){
        if ( !args.empty() ) obj.trigger_event((std::string)args[0]);
        return misc::Value();
    })


    DECLARE_METHOD_FUNCTOR(Object,kill,[](Object&obj,const std::vector<misc::Value>&){
        if ( obj.level() ) obj.level()->remove_object(&obj);
        return misc::Value();
    })
}

/// \pimpl{Object}
struct game::Object_Private
{
    geo::Point       pos;
    std::string      name;
    misc::Properties properties;
    Level*           level = nullptr;
    Object_Layer*    layer = nullptr;
    std::map<std::string,eval::Parse_Tree*> events;
    eval::Context    context = eval::Context(&Game_Manager::instance().get_context());

    typedef std::map<std::string,std::function<Object*(const std::string&)>> Factory;
    static Factory& factory()
    {
        static Factory fac;
        return fac;
    }

    void set_name(const std::string& n)
    {
        name = n;
        std::replace_if(name.begin(),name.end(),
            [](char c){return !std::isalnum(c) && c != '.' && c != '_'; },
            '_');
    }
};

int Object::register_object_type(const std::string& name,
                                  const std::function<Object*(const std::string&)> & create)
{
    static int id = static_type_id();
    Object_Private::factory()[name] = create;
    return ++id;
}

int Object::static_type_id()
{
    return 0;
}

Object* Object::create(const misc::Properties& properties )
{
    std::string type(properties["type"]);
    auto it = Object_Private::factory().find(type);
    if ( it != Object_Private::factory().end() )
    {
        Object* object = it->second((std::string)properties["name"]);
        if ( object )
        {
            object->set_properties(properties);
            object->trigger_event("spawn");
            return object;
        }
    }
    Logger::err() << "Cannot create object of type \""+type+"\"";
    return nullptr;
}

misc::Value Object::get_undeclared_property(const std::string& name) const
{
    if ( misc::Object_Source::has_undeclared_property(name) )
        return misc::Object_Source::get_undeclared_property(name);
    auto it = d->properties.find(name);
    if ( it != d->properties.end() )
        return it->second;
    return misc::Value();
}
bool Object::has_undeclared_property(const std::string& name) const
{
    return misc::Object_Source::has_undeclared_property(name) || d->properties.find(name) != d->properties.end();
}
void Object::get_all_undeclared_properties(misc::Properties& out, const std::string& prefix) const
{
    for ( const auto& p : d->properties )
        out[prefix+p.first] = p.second;
    misc::Object_Source::get_all_undeclared_properties(out,prefix);
}

void Object::set_undeclared_property(const std::string& name, const misc::Value& value)
{
    if ( misc::starts_with(name,"events.") )
        set_event(misc::remove_prefix(name,"events."),(std::string)value);
    else if ( name == "context" )
        eval::Parser().evaluate((std::string)value,d->context);
    else if ( misc::Object_Source::has_writable_undeclared_property(name) )
        misc::Object_Source::set_undeclared_property(name,value);
    else
        d->properties[name] = value;
}
bool Object::has_writable_undeclared_property(const std::string& name) const
{
    if ( misc::starts_with(name,"events.") )
        return has_event(misc::remove_prefix(name,"events."));
    if ( name == "context" )
        return true;
    if ( misc::Object_Source::has_writable_undeclared_property(name) )
        return true;
    return has_undeclared_property(name);
}


bool Object::has_undeclared_method(const std::string& name) const
{
    return has_event(name);
}
misc::Value Object::call_undeclared_method(const std::string& name, const std::vector<misc::Value>& args )
{
    if ( misc::Object_Source::has_undeclared_method(name) )
        return misc::Object_Source::call_undeclared_method(name,args);
    trigger_event(name);
    return misc::Value();
}


bool Object::has_event(const std::string& name) const
{
    return d->events.count(name);
}

void Object::set_event(const std::string&name, const std::string& code)
{
    auto it = d->events.find(name);
    if ( it != d->events.end() )
        delete it->second;
    d->events[name] = eval::Parser().compile(code);
}

void Object::trigger_event(const std::string& name)
{
    trigger_event(name,d->context);
}

void Object::trigger_event(const std::string& name, const eval::Context& context)
{
    auto it = d->events.find(name);
    if ( it != d->events.end() )
        it->second->evaluate(context);
}


Object::Object(const std::string& name)
 : d(new Object_Private)
{
    d->set_name(name);
    d->context.set_object("this",this);
}

Object::~Object()
{
    for ( auto e : d->events )
        delete e.second;
    delete d;
}

std::string Object::name() const
{
    if ( ! d->name.empty() )
        return d->name;
    std::ostringstream ss;
    ss << type() << "_" << this;
    d->set_name(ss.str());
    return d->name;
}

void Object::update()
{
    if ( d->level )
        d->level->update_object(this);
}

float Object::x() const
{
    return d->pos.x;
}

float Object::y() const
{
    return d->pos.y;
}

const geo::Point & Object::position() const
{
    return d->pos;
}

void Object::set_position(const geo::Point& p)
{
    d->pos = p;
    update();
}

void Object::set_position(float x, float y)
{
    set_position(geo::Point(x,y));
}

void Object::set_x(float x)
{
    set_position(geo::Point(x,d->pos.y));
}

void Object::set_y(float y)
{
    set_position(geo::Point(d->pos.x,y));
}

float Object::width() const
{
    return local_bounding_rect().width;
}

float Object::height() const
{
    return local_bounding_rect().height;
}

geo::Size Object::size() const
{
    return local_bounding_rect().size();
}

geo::Rectangle Object::bounding_rect() const
{
    return local_bounding_rect().translated(d->pos);
}

void Object::set_level(Level* level)
{
    d->level = level;
}

Level* Object::level() const
{
    return d->level;
}

void Object::set_layer(Object_Layer* layer)
{
    d->layer = layer;
}

Object_Layer* Object::layer() const
{
    return d->layer;
}

eval::Context& Object::event_context()
{
    return d->context;
}

void Object::step()
{
    trigger_event("step");
}

REGISTER_OBJECT(Sprite_Object,"sprite")

DECLARE_PROPERTY_GET_FUNCTOR(Sprite_Object,animation,
     [](const Sprite_Object&o){ return o.animation() ? o.animation()->id() : ""; } )
DECLARE_PROPERTY_SET_FUNCTOR(Sprite_Object,animation,
     [](Sprite_Object&o, const misc::Value& v)
         { o.set_animation( media::Media_Manager::instance().animation((std::string)v) );} )

DECLARE_PROPERTY_SETTER(Sprite_Object,sound,play_sound)

DECLARE_PROPERTY_SET_FUNCTOR(Sprite_Object,color,[](Sprite_Object&o, const misc::Value& v)
         { o.set_color( misc::Color((std::string)v) ); } )
DECLARE_PROPERTY_GET_FUNCTOR(Sprite_Object,color, [](const Sprite_Object&o)
         { return o.color().name(); } )


void Sprite_Object::set_properties(const misc::Properties& properties)
{
    Object::set_properties(properties);
    if ( !properties.count("animation") && properties.count("tileset") )
    {
        media::Tileset* ts = media::Media_Manager::instance().tileset((std::string)properties["tileset"]);
        if ( ts ) set_sprite(ts->get_tile((long)properties["tileset.tile"]));
    }

}

/// \pimpl{Sprite_Object}
struct game::object::Sprite_Object_Private
{
    sf::Sprite              sprite;             ///< Sprite
    const media::Animation* animation = nullptr;///< Animation
    Timer                   frame_time;         ///< Time when the next frame has to be rendered
    int                     frame = -1;         ///< Animation frame
    sf::Sound               sound;              ///< Current object sound
    bool                    play = false;       ///< Whether to play the sound

    void next_frame()
    {
        if ( animation )
        {
            frame = animation->next_frame(frame);
            animation->get_sprite(frame,sprite);
            frame_time.start(animation->frame_time());
        }
    }
};

Sprite_Object::Sprite_Object(const std::string& name)
    : Object(name), d ( new Sprite_Object_Private )
{}

Sprite_Object::~Sprite_Object()
{
    delete d;
}

geo::Rectangle Sprite_Object::local_bounding_rect() const
{
    if ( d->animation )
        return d->animation->bounding_rect(d->frame);
    return geo::conv::rect(d->sprite.getLocalBounds());
}

void Sprite_Object::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    //states.transform.translate(position());
    states.transform.translate(x(),y());
    target.draw(d->sprite,states);
}

void Sprite_Object::step()
{
    Object::step();

    if ( d->animation && d->animation->fps() > 0 )
    {
        d->frame_time.step();
        if ( d->frame_time.ready() )
        {
            if ( d->animation->last_frame(d->frame) )
            {
                trigger_event("animation_end");
                animation_end();
            }
            /// \todo skip frames when low fps but not when paused
            d->next_frame();
        }
    }

    if ( d->play )
    {
        d->play = false;
        /// \todo user setting to toggle positional sounds
        d->sound.setPosition(bounding_rect().center().x,bounding_rect().center().y,0);
        d->sound.play();
    }
}

void Sprite_Object::set_animation(const media::Animation* animation, bool reset)
{
    d->animation = animation;
    if ( reset )
        d->frame = -1;
    d->next_frame();
    update();

}

const media::Animation* Sprite_Object::animation() const
{
    return d->animation;
}

sf::Sprite& Sprite_Object::sprite()
{
    return d->sprite;
}

void Sprite_Object::set_sprite(const sf::Sprite& s)
{
    d->animation = nullptr;
    d->sprite = s;
    d->sprite.setPosition(0,0);/// \todo keep sprite offset?
    update();
}

long Sprite_Object::animation_flags() const
{
    return d->animation ? d->animation->user_flags() : 0;
}


bool Sprite_Object::animation_flags(long flags) const
{
    return animation_flags() & flags;
}

void Sprite_Object::play_sound(const std::string& sound_file)
{
    d->sound.setBuffer(media::Media_Manager::instance().sound(sound_file));
    /// \todo Game setting (deduce from camera?) for distance/attenuation
    d->sound.setMinDistance(256);
    d->sound.setAttenuation(1);
    d->play = true;
}

bool Sprite_Object::can_destroy() const
{
    return d->sound.getStatus() != sf::Sound::Playing;
}

misc::Color Sprite_Object::color() const
{
    return d->sprite.getColor();
}

void Sprite_Object::set_color( const misc::Color& col )
{
    d->sprite.setColor(col);
}


Object* Object_Library::create(const std::string& library_id) const
{
    misc::Properties p;
    get_properties(library_id,p,true);
    return Object::create(p);
}

void Object_Library::get_properties(const std::string& library_id, misc::Properties& properties, bool overwrite) const
{
    auto it = objects.find(library_id);
    if ( it != objects.end() )
        for ( const auto&p : it->second )
            if ( overwrite || !properties.count(p.first) )
                properties[p.first] = p.second;

    auto inhit = properties.find("inherit");
    if ( inhit != properties.end() )
    {
        properties.erase("inherit");
        get_properties((std::string)inhit->second, properties, false);
    }
}


void Object_Library::get_properties(const std::string& tileset, long tile_id, misc::Properties& properties, bool overwrite) const
{
    for ( const auto&o : objects )
    {
        if ( (std::string)o.second["tileset"] == tileset && (long)o.second["tileset.tile"] == tile_id )
        {
            for ( const auto&p : o.second )
                if ( overwrite || !properties.count(p.first) )
                    properties[p.first] = p.second;
        }
    }

    auto inhit = properties.find("inherit");
    if ( inhit != properties.end() )
    {
        properties.erase("inherit");
        get_properties((std::string)inhit->second, properties, false);
    }
}

void Object_Library::load(const std::string& file)
{
    using namespace boost::property_tree;

    std::string file_path = media::Media_Manager::instance().data_file(file);
    if ( file_path.empty() )
    {
        Logger::err() << "File" << file << "not found";
        return;
    }

    try {
        ptree pt;
        read_xml(file_path,pt,xml_parser::trim_whitespace|xml_parser::no_comments);
        load(pt);
    } catch ( const std::exception& ex )
    {
        Logger::err() << "Error loading object" << file << ":" << ex.what();
    }
}
void Object_Library::load(misc::Properties properties )
{
    if ( properties.count("tileset") && properties.count("tile") && ! properties.count("tileset.tile") )
    {
        properties["tileset.tile"] = properties["tile"];
        properties.erase("tile");
    }

    std::string id;
    if ( properties.count("library_id") )
    {
        id = (std::string)properties["library_id"];
        properties.erase("library_id");
    }
    else if ( properties.count("name") )
        id = (std::string)properties["name"];
    else
        id = "#"+std::to_string(objects.size());

    objects[id] = properties;
}

void Object_Library::clear()
{
    objects.clear();
}

void Object_Library::load ( const boost::property_tree::ptree& ptree )
{
    for ( auto c : ptree )
    {
        if ( c.first == "object" )
        {
            std::string source = c.second.get("<xmlattr>.source","");
            if ( !source.empty() )
                load(source);
            else
                load(misc::tree_to_properties(c.second));
        }
        else if ( c.first == "objects" )
        {
            std::string source = c.second.get("<xmlattr>.source","");
            if ( !source.empty() )
                load(source);
            else
                load(c.second);
        }
    }
}
