/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "game/object/enemy.hpp"
#include "game/level.hpp"

using namespace game::object;

REGISTER_OBJECT(Enemy,"enemy")
DECLARE_PROPERTY_ATTRIBUTE(Enemy,collision)
DECLARE_PROPERTY_ATTRIBUTE(Enemy,requires)
DECLARE_PROPERTY_ATTRIBUTE(Enemy,damage_index)


void Enemy::step()
{
    if ( requires.empty() || requires.evaluate(context) )
    {
        Character::step();

        auto targets = level()->collision().get_objects_if<Character>(bounding_rect(),
            [this](const Character*b){ return b != this && (b->target_index & damage_index); } );

        for ( auto target : targets )
            collide(target);

        if ( hit&LEFT )
            trigger_event("hit_left");
        if ( hit&RIGHT )
            trigger_event("hit_right");
    }
}


void Enemy::collide(Object* victim)
{
    context.set_object("other",victim);
    collision.evaluate(context);
}
