/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "level_info.hpp"
#include "eval/parser.hpp"


using namespace game;
using namespace game::object;

REGISTER_OBJECT(Spawn,"spawn")
DECLARE_PROPERTY_ATTRIBUTE(Spawn,from)
DECLARE_PROPERTY_ATTRIBUTE(Spawn,condition)
DECLARE_PROPERTY_ATTRIBUTE(Spawn,action)



Spawn::Spawn(const std::string& name)
    : Sprite_Object(name), context(&event_context())
{}


bool Spawn::can_spawn(const std::string& lev_from) const
{
    if ( !from.empty() && from != lev_from )
        return false;
    if ( !condition.empty() )
    {
        return (bool)condition.evaluate(context);
    }
    return true;
}

void Spawn::setup(Object* o) const
{
    o->set_position(position());
    context.set_object("other",o);
    action.evaluate(context);
}
