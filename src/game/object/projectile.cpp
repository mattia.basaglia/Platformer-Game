/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "projectile.hpp"
#include "misc/geometry_convert.hpp"
#include "game/level.hpp"
#include "game/game_manager.hpp"
#include "blocks.hpp"

using namespace game::object;

REGISTER_OBJECT(Laser,"laser")

DECLARE_PROPERTY_ATTRIBUTE(Laser,color)
DECLARE_PROPERTY_ATTRIBUTE(Laser,height)
DECLARE_PROPERTY_ATTRIBUTE(Laser,solid_index)
DECLARE_PROPERTY_ATTRIBUTE(Laser,damage_index)
DECLARE_PROPERTY_ATTRIBUTE(Laser,duration)
DECLARE_PROPERTY_ATTRIBUTE(Laser,hit_target)
DECLARE_PROPERTY_ATTRIBUTE(Laser,hit_wall)

Laser::Laser(const std::string& name)
    : Object(name), size(-1,-1)
{
    death_time.ring = [this](){level()->remove_object(this);};
}

void Laser::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.blendMode = sf::BlendAdd;
    sf::Vertex rect[4];
    float cy = bounding_rect().center().y;
    float dy = height * 0.5 *
        ( death_time.time_left.count() / float(duration.count()) );

    if ( dy > 0 )
    {
        rect[0] = sf::Vertex(sf::Vector2f(bounding_rect().left(),cy-dy),color);
        rect[1] = sf::Vertex(sf::Vector2f(bounding_rect().left(),cy+dy),color);
        rect[2] = sf::Vertex(sf::Vector2f(bounding_rect().right(),cy+dy),color);
        rect[3] = sf::Vertex(sf::Vector2f(bounding_rect().right(),cy-dy),color);
        target.draw(rect,4,sf::Quads,states);
    }
}

bool Laser::fit_size()
{
    if ( duration <= misc::duration::zero() )
        return false;

    size.width = level()->size().width - x();
    bool left = false;
    if ( get_property("direction") == "left" )
    {
        left = true;
        size.width = x();
        set_x(0);
    }
    size.height = height;

    // block laser where appropriate
    geo::Rectangle hit_check ( position(), size );

    geo::Rectangle collided = level()->collision().collision_rectangle_if<Block>(hit_check,
        [this](const Block*b){ return (b->solid_index & solid_index) && b->block_type == Block::SOLID; } );
    collided.unite( level()->collision().collision_rectangle_if<Dynamic_Block>(hit_check,
        [this](const Dynamic_Block*b){ return b->solid_index & solid_index; } ) );


    if ( collided.width > 0 )
    {
        if ( left && collided.right() > hit_check.left() )
        {
            hit_check.width = hit_check.right() - collided.right();
            hit_check.x = collided.right();

            eval::Context context(&event_context());
            context.set_temporary("collision.x",(long)collided.right());
            context.set_temporary("collision.y",(long)(y()+height/2));
            hit_wall.evaluate(context);

        }
        else if ( !left && collided.left() < hit_check.right() )
        {
            hit_check.width = collided.left() - hit_check.x;

            eval::Context context(&event_context());
            context.set_temporary("collision.x",(long)collided.left());
            context.set_temporary("collision.y",(long)(y()+height/2));
            hit_wall.evaluate(context);
        }
    }

    // empty laser
    if ( !hit_check.is_valid() )
        return false;

    // adjust bounding box
    set_position(hit_check.top_left());
    size = hit_check.size();


    // ensure we hit the dynamic blocks
    hit_check.x --;
    hit_check.width+=2;

    // hit targets
    eval::Context context(&event_context());
    for ( auto o : level()->collision().get_objects_if<Object>(hit_check,[this](const Object*obj){
            return obj != this && (((long)obj->get_property("target_index"))&damage_index); }) )
    {
        context.set_object("other",o);
        hit_target.evaluate(context);
    }

    death_time.start(duration);

    return true;
}

void Laser::step()
{
    if ( size.width < 0 && !fit_size() )
        level()->remove_object(this);

    death_time.step();
}
