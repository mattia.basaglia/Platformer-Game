/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef COLLECTIBLE_HPP
#define COLLECTIBLE_HPP

#include "object.hpp"
#include "eval/parser.hpp"
#include "eval/script_snippet.hpp"

namespace game {
/**
 * \brief Namespace to hold specific Object types
 */
namespace object {

/**
 * \brief Item that can be collected by the player
 * \par Properties
 *      * \c string \b action       Command to be execute upon collection (this,other=collector)
 *      * \c string \b requires     Condition string to evaluate whether it can be collected
 *      * \c float \b float.speed.x Floating speed
 *      * \c float \b float.speed.y Floating speed
 *      * \c number \b float.radius Max distance from the starting position when floating
 */
class Collectible : public Sprite_Object
{
    DECLARE_OBJECT(Collectible,Sprite_Object)

    eval::Context       context;

public:
    eval::Script_Snippet action;
    eval::Script_Snippet requires;
    geo::Point           float_speed;
    long                 float_radius = 0;

    Collectible(const std::string& name);

    /**
     * \brief Perform the collect action
     * \param collector Object collecting the item
     */
    void collect(Object* collector);

    void step() override;
};

}} // namespace game::object
#endif // COLLECTIBLE_HPP
