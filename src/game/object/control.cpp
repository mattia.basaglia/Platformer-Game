/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "control.hpp"
#include "media/media_manager.hpp"

using namespace game::object;

REGISTER_OBJECT(Rectangle,"rectangle")
DECLARE_PROPERTY(Rectangle,width,width,set_width)
DECLARE_PROPERTY(Rectangle,height,height,set_height)

void Rectangle::set_width(long w)
{
    size.width = w;
    update();
}
void Rectangle::set_height(long h)
{
    size.height = h;
    update();
}


REGISTER_OBJECT(Script,"script")
DECLARE_PROPERTY_ATTRIBUTE(Script,code)

DECLARE_PROPERTY_SETTER(Script,source,load)

Script::Script(const std::string& name)
    : Object(name)
{}


void Script::step()
{
    code.evaluate(event_context());
}

void Script::load(const std::string& file)
{
    code.load_file(media::Media_Manager::instance().data_file(file));
}
