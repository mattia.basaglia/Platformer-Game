/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "user_settings.hpp"
#include "misc/logger.hpp"
#include "misc/ptree_utils.hpp"

#include <fstream>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/filesystem.hpp>

using namespace game;

DECLARE_METHOD_FUNCTOR(User_Settings,save,[](User_Settings&obj,const std::vector<misc::Value>& args) -> misc::Value {
    return (long) ( args.empty() ? obj.save() : obj.save((std::string)args[0]) );
})
DECLARE_METHOD_FUNCTOR(User_Settings,load,[](User_Settings&obj,const std::vector<misc::Value>& args) -> misc::Value {
    return (long) ( args.empty() ? obj.load() : obj.load((std::string)args[0]) );
})
DECLARE_METHOD_VOID_NOARG(User_Settings,clear)

/// \pimpl{User_Settings}
struct game::User_Settings_Private
{
    misc::Properties properties;
    std::string      default_path;
};


User_Settings::User_Settings()
    : d ( new User_Settings_Private() )
{}
PIMPL_STRUCTORS_INH(User_Settings,misc::Mirror)

misc::Value User_Settings::get_undeclared_property(const std::string& name) const
{
    auto it = d->properties.find(name);
    if ( it != d->properties.end() )
        return it->second;
    return misc::Value();
}
bool User_Settings::has_undeclared_property(const std::string& name) const
{
    return d->properties.find(name) != d->properties.end();
}
void User_Settings::get_all_undeclared_properties(misc::Properties& out, const std::string& prefix) const
{
    for ( const auto& p : d->properties )
        out[prefix+p.first] = p.second;
}

void User_Settings::set_undeclared_property(const std::string& name, const misc::Value& value)
{
    d->properties[name] = value;
}
bool User_Settings::has_writable_undeclared_property(const std::string& name) const
{
    return has_undeclared_property(name);
}

bool User_Settings::save(const std::string& file) const
try
{
    using namespace boost::property_tree;

    ptree pt;
    for ( const auto& p : get_properties() )
        pt.put(p.first,(std::string)p.second);

    ptree root;
    if ( pt.empty() )
        root.put("settings"," ");
    else
        root.put_child("settings",pt);

    std::string save_path = file.empty() ? d->default_path : file;
    std::string tmp_path = save_path + ".temp";
    boost::filesystem::remove(tmp_path);
    write_xml(tmp_path, root, std::locale());//, xml_writer_settings<char>(' ',2) );

    boost::filesystem::rename(tmp_path,save_path);
    boost::filesystem::remove(tmp_path);

    return true;
}
catch ( const std::exception& ex )
{
    Logger::err() << "Error saving settings:" << ex.what();
    return false;
}


bool User_Settings::load(const std::string& file)
try
{
    using namespace boost::property_tree;
    ptree pt;
    read_xml(file.empty() ? d->default_path : file, pt,xml_parser::trim_whitespace|xml_parser::no_comments);
    misc::Properties prop = misc::tree_to_properties(pt.get_child("settings"));
    for ( const auto& p : prop )
        set_property(p.first,p.second);
    return true;
}
catch ( const std::exception& ex )
{
    Logger::err() << "Error loading settings:" << ex.what();
    return false;
}

void User_Settings::set_default_file( const std::string& path )
{
    d->default_path = path;
}

const std::string& User_Settings::default_file() const
{
    return d->default_path;
}

void User_Settings::clear()
{
    d->properties.clear();
}
