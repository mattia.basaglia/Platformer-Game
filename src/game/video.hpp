/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef VIDEO_HPP
#define VIDEO_HPP
#include "game/camera.hpp"
#include "misc/geometry.hpp"
#include <SFML/Graphics.hpp>

namespace game {

class Level;

/**
 * \brief Class managing the game display
 * \par Properties
 *      * \c string \b title    Window title
 *      * \c boolean \b shader  (read only) Whether shaders can be used
 * \section Methods
 * \par restart()
 * Reload config and apply changes to the video settings
 * \par shader_param ( shader, name, value... )
 * Set a shader parameter, avaliable overloads:
 *      * <tt> \b shader_param ( shader, name, number ) </tt> (float)
 *      * <tt> \b shader_param ( shader, name, number, number ) </tt> (vec2)
 *      * <tt> \b shader_param ( shader, name, number, number, number ) </tt> (vec3)
 *      * <tt> \b shader_param ( shader, name, number, number, number, number ) </tt> (vec4)
 *      * <tt> \b shader_param ( shader, name, "#color" ) </tt> (vec4)
 * \par shader()
 * Remove all shaders
 * \par shader(shader...)
 * Set a list of active shaders
 * \par screen_x(x,y)
 * Get X pixel coordinate of the given scene position
 * \par screen_y(x,y)
 * Get Y pixel coordinate of the given scene position
 * \par screen_length(len)
 * Convert a distance from scene to screen
 */
class Video : public misc::Mirror
{
    PROPERTY_MAGIC(Video)
private:
    class Video_Private *d;

public:
    Video();
    ~Video();

    /**
     * \brief Create the window and other resources
     */
    void init();

    /**
     * \brief Reload the configuration and reset the window accordingly
     */
    void restart();

    /**
     * \brief Render the level and apply the shaders
     */
    void render(Level* current_level);

	/**
	 * \brief Get camera object
	 */
	Camera& camera() const;

	/**
	 * \brief Get the display window
	 */
	sf::RenderWindow& window() const;

	/**
	 * \brief Map window pixel to scene coordinate
	 */
	geo::Point map_from_pixel(long x, long y) const;

	/**
	 * \brief Scene coordinate to window pixel
	 */
	geo::Point map_to_pixel(const geo::Point& p) const;

	/**
	 * \brief Scene coordinate to window pixel
	 */
	geo::Point map_to_pixel(long x, long y) const;

	/**
	 * \brief Move the camera
	 */
	void step();

	/**
	 * \brief Window title
	 */
	const std::string& title() const;

	/**
	 * \brief Set window title
	 */
	void set_title(const std::string& title);

	/**
	 * \brief Whether shaders are anabled
	 */
    bool shaders() const;

    /**
     * \brief Setup for rendering the UI
     */
    void begin_ui();


    /**
     * \brief Finish rendering the UI
     */
    void end_ui();

};

} // namespace game

#endif // VIDEO_HPP
