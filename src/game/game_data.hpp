/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef GAME_DATA_HPP
#define GAME_DATA_HPP

#include <vector>
#include <string>
#include <algorithm>
#include "misc/mirror.hpp"

namespace game {

/**
 * \brief Info and metadata regarding a game
 */
struct Game_Data : public misc::Mirror
{
private:
    PROPERTY_MAGIC(Game_Data)

public:
    std::string name;               ///< Name of the game
    std::string first_level;        ///< Identifier of the level to start on
    std::vector<std::string>authors;///< People who made the game
    std::string license;            ///< License

    /**
     * \brief Get if the license is ethical enough
     */
    bool valid_license() const
    {
        static std::vector<std::string> acceptable_licenses = {
            "CC0",
            "CC-BY",
            "CC-BY-SA",
            "MIT",
            "GPLv2",
            "GPLv2+",
            "GPLv3",
            "GPLv3+",
            "MPLv2",
        };
        return std::find(acceptable_licenses.begin(),acceptable_licenses.end(),license) != acceptable_licenses.end();
    }
};

} // namespace game

#endif // GAME_DATA_HPP
