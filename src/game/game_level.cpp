/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "game_level.hpp"
#include "eval/parser.hpp"
#include "game/object/level_info.hpp"
#include "misc/logger.hpp"
#include "game/game_manager.hpp"
#include "media/media_manager.hpp"
#include "misc/math_utils.hpp"

using namespace game;

REGISTER_LEVEL(Game_Level,"game")


Game_Level::Game_Level(const std::string& id)
    : Level(id)
{}


void Game_Level::load(const std::string& from)
{
    Level::load(from);

    auto spawns = collision().get_objects_if<object::Spawn>(collision().get_area(),
        [from](const object::Spawn*sp) { return sp->can_spawn(from); } );


    if ( !spawns.empty() )
    {
        int i = math::random(0,spawns.size()-1);
        Object *spawned_object = Game_Manager::instance().object_library().create(
            (std::string)Game_Manager::instance().storage()["characters.spawn"]);
        if ( spawned_object )
        {
            spawned_object->set_x(spawns[i]->x());
            spawned_object->set_y(spawns[i]->y());
            add_object(spawned_object,spawns[i]);
            spawns[i]->setup(spawned_object);
            Game_Manager::instance().video().camera().set_starting_position( {
                spawned_object->bounding_rect().center().x,
                spawned_object->bounding_rect().top()
            });
        }
        else
            Logger::err() << "Cannot create spawn object\n";
        return;
    }

    Logger::err() << "Cannot find suitable spawn location";
}

REGISTER_LEVEL(Map_Level,"map")

Map_Level::Map_Level(const std::string& id)
    : Game_Level(id)
{
    /// \todo implement
}

void Map_Level::step()
{
    Level::step();
    /// \todo implement
}

void Map_Level::load(const std::string& from)
{
    Level::load(from);
    /// \todo implement
}
