/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
 * \file
 * \note This header shall only be included by game_manager.cpp
 */


#include "eval/library.hpp"
#include "eval/parser.hpp"
#include "media/media_manager.hpp"
#include <iomanip>

namespace game {

/**
 * \brief Library of functions interacting with the game engine
 *
 * \section Status
 * \par status()
 * Show game status and storage values
 * \todo add stuff in here as it gets implemented
 *
 * \par status("objects")
 * Show a list of game objects
 * \par status("object_name")
 * Show all the properties of said object
 *
 * \section Game
 * \par quit()
 * Shorthand for <tt>game.quit()</tt>
 *
 * \section Objects
 * \par spawn(type,property_1,value_1...)
 * Spawn an object if \c type begins with "library." an instance from the object library will be created
 * \par kill(name)
 * Remove the object with the given name
 *
 * \section Media
 * \par load ( type, id... )
 * \b type is the kind of resource, \b id the file id.
 * \par Supported resource types
 *      * \b graphics   XML file containing animation and tileset definitions
 *      * \b object     XML file contating an object library entry
 * \todo More resource types
 *
 * \todo Reload
 */
const eval::Library game_library = eval::Library({

	{"status", [](const eval::Context& context, const eval::Context::Arguments& arg) -> misc::Value
    {
        ///
        auto&  gm = Game_Manager::instance();
        Level* lev = gm.current_level();
        Logger log(16, false, false);

        if ( lev && !arg.empty() )
        {
            if ( arg[0] == "objects" )
            {
                log << "Objects:\n";
                for(auto o : lev->objects())
                    log << "\t" <<  std::setw(32) << std::setfill(' ') << std::left << o->name() << " : " << o->type() << '\n';
                return (long)lev->objects().size();
            }
            else
            {
                log << "Properties for " << arg[0] << ":\n";
                auto props = context.object_properties((std::string)arg[0]);
                if ( props.empty() )
                {
                    if ( auto o = context.find_nested_object((std::string)arg[0]) )
                        props = o->get_properties();
                }
                log.table(props);
                return misc::Value();
            }
        }

        log << "\nCurrent Game: " << gm.current_game().name;
        if ( !gm.current_game().authors.empty() )
            log << " by " << gm.current_game().authors << std::endl;

        log << "Current Level: " << ( lev ? lev->id() : "(No Level)" ) << std::endl;

        log.table(gm.get_properties());

        log << "Storage: " << std::endl;
        log.table(gm.storage());

        log << "\n";

        return misc::Value();
    }},

	{"quit", [](const eval::Context& context, const eval::Context::Arguments& args) -> misc::Value {
        Game_Manager::instance().quit();
        return misc::Value();
	}},

	{"spawn", [](const eval::Context& context, const eval::Context::Arguments& args) -> misc::Value {

        if ( ! Game_Manager::instance().current_level() )
        {
            Logger::err() << "Cannot create object without a level";
            return misc::Value();
        }

        if ( args.empty() )
        {
            Logger::err() << "Missing object type";
            return misc::Value();
        }

        misc::Properties props;
        std::string type = (std::string)args.front();
        if ( misc::starts_with(type,"library.") )
            Game_Manager::instance().object_library().get_properties(
                misc::remove_prefix(type,"library."),props,true );
        else
            props["type"] = type;
        for ( auto it = args.begin()+1; it+1 < args.end(); it+=2 )
            props[(std::string)*it] = *(it+1);

        auto it = props.find("parent");
        Object* parent = nullptr;
        if ( it != props.end() )
        {
            if ( (parent = Game_Manager::instance().current_level()->find_object((std::string)it->second)) )
            {
                if ( !props.count("x") && !props.count("center.x") )
                    props["center.x"] = parent->bounding_rect().center().x;
                if ( !props.count("y") && !props.count("center.y") )
                    props["center.y"] = parent->bounding_rect().center().y;
                props.erase(it);
            }
        }

        if ( Object* obj = Object::create(props) )
        {
            Game_Manager::instance().current_level()->add_object(obj,parent);
            return obj->name();
        }

        Logger::err() << "Unable to create object of type" << args.front();
        return misc::Value();
	}},
	{"kill", [](const eval::Context& context, const eval::Context::Arguments& args) -> misc::Value {
        Level* lev = Game_Manager::instance().current_level();
        if ( !lev || args.empty() )
            return misc::Value();

        if ( Object* obj = lev->find_object((std::string)args.front()) )
            lev->remove_object(obj);
        else
            Logger::err() << "Unable to kill object" << args.front();

        return misc::Value();
	}},

	{"load", [](const eval::Context& context, const eval::Context::Arguments& args) -> misc::Value {
        if ( !args.empty() )
        {
            if ( args[0] == "graphics" )
            {
                for ( auto it = args.begin()+1; it != args.end(); ++it )
                    media::Media_Manager::instance().load_graphics((std::string)*it);
            }
            else if ( args[0] == "object" )
                for ( auto it = args.begin()+1; it != args.end(); ++it )
                    Game_Manager::instance().object_library().load((std::string)*it);
        }
        return misc::Value();
	}},

	{"schedule", [](const eval::Context& context, const eval::Context::Arguments& args) -> misc::Value {
	    if ( args.size() >= 2 )
        {
            std::shared_ptr<eval::Parse_Tree> code (eval::Parser().compile((std::string)args[1]));
            Game_Manager::instance().schedule(game::Timer(
                args[0].to<misc::milliseconds>(),
                [code](){ code->evaluate(game::Game_Manager::instance().get_context()); }
            ));
        }
        return misc::Value();
	}},

});

} // namespace game
