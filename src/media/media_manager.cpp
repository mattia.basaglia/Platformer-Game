/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "media/media_manager.hpp"
#include "config.hpp"
#include "media/tileset.hpp"
#include "media/animation.hpp"
#include "media/font.hpp"
#include "misc/logger.hpp"
#include "misc/string_utils.hpp"
#include "misc/ptree_utils.hpp"

#include <boost/filesystem.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include <vector>
#include <string>
#include <map>

using namespace media;

/// \pimpl{Media_Manager}
struct media::Media_Manager_Private
{
    std::map<std::string,Tileset*>      tilesets;
    std::map<std::string,sf::Texture*>  images;
    std::map<std::string,media::Font*>  fonts;
    media::Font                         default_font;
    std::map<std::string,Animation*>    animations;
    std::map<std::string,sf::SoundBuffer>sounds;
    std::map<std::string,sf::Shader*>   shaders;
    std::vector<sf::Shader*>            active_shaders;
    std::map<std::string,ui::Frame*>    frames;
    std::map<std::string,ui::Bar*>      bars;

    std::map<std::string,void(Media_Manager::*)(const boost::property_tree::ptree&)> load_methods;

    Tileset* load_tileset(const std::string&name,
                            const boost::property_tree::ptree& pt,
                            const std::string& dir);
    Tileset* load_tileset(const std::string&file_name);

    std::string data_or_relative(const std::string&file, const std::string& dire);
};


Media_Manager::Media_Manager()
    : d(new Media_Manager_Private)
{
    d->load_methods["tileset"] = &media::Media_Manager::load_tileset;

    d->load_methods["animation"] = &media::Media_Manager::load_animation;
    d->load_methods["bar"] = &media::Media_Manager::load_bar;
    d->load_methods["frame"] = &media::Media_Manager::load_frame;
    d->load_methods["graphics"] = &media::Media_Manager::load_graphics;
}

Media_Manager::~Media_Manager()
{
    for ( auto p : d->tilesets )
        delete p.second;
    for ( auto p : d->images )
        delete p.second;
    for ( auto p : d->fonts )
        delete p.second;
    for ( auto p : d->animations )
        delete p.second;
    for ( auto p : d->shaders )
        delete p.second;
    for ( auto p : d->frames )
        delete p.second;
    for ( auto p : d->bars )
        delete p.second;
    delete d;
}

Media_Manager& Media_Manager::instance()
{
    static Media_Manager singleton;
    return singleton;
}

sf::Texture& Media_Manager::image(const std::string& id)
{
    auto it = d->images.find(id);
    if ( it != d->images.end() )
        return *it->second;
    auto tex = new sf::Texture();
    std::string file = data_file(id);
    if ( file.empty() )
        Logger::err() << "Texture not found :" << id;
    if ( !tex->loadFromFile(file) )
        Logger::error("Error while loading texture "+id);
    d->images[id] = tex;
    return *tex;
}

Tileset* Media_Manager::tileset(const std::string& id)
{
    auto it = d->tilesets.find(id);
    if ( it != d->tilesets.end() )
        return it->second;
    return load_tileset(id);
}

void Media_Manager::register_tileset(Tileset* tileset)
{
    delete d->tilesets[tileset->id()];
    d->tilesets[tileset->id()] = tileset;
}

Tileset* Media_Manager::unregister_tileset(const std::string& id)
{
    if ( d->tilesets.count(id) )
    {
        auto t = d->tilesets[id];
        d->tilesets.erase(id);
        return t;
    }
    return nullptr;
}

void Media_Manager::delete_tileset(const std::string& id)
{
    delete unregister_tileset(id);
}

Tileset* Media_Manager::load_tileset(const std::string& file_id)
{
    return d->load_tileset(data_file(file_id));
}

Tileset* Media_Manager_Private::load_tileset(const std::string&file_name)
{
    using namespace boost::property_tree;
    using path = boost::filesystem::path;
    path source_path = file_name;

    if ( !exists(source_path) )
    {
        Logger::error("Unable to open "+file_name);
        return nullptr;
    }
    ptree pt;
    try {
        read_xml(file_name,pt,xml_parser::trim_whitespace|xml_parser::no_comments);
    } catch ( const std::exception& ex )
    {
        Logger::err() << "Error loading tileset" << file_name << ":" << ex.what();
    }

    if ( pt.empty() )
    {
        Logger::error("Invalid tileset "+file_name);
        return nullptr;
    }
    return load_tileset(Media_Manager::instance().data_id(file_name),
                          pt.begin()->second,
                          source_path.parent_path().string());
}

std::string Media_Manager_Private::data_or_relative(const std::string&file, const std::string& dir)
{
    using namespace boost::filesystem;
    std::string result = Media_Manager::instance().data_file(file);
    if ( ( result.empty() || !exists(result) ) && !dir.empty() )
        result = ( path(dir)/file ).string();
    if ( !exists(result) )
        result = file;
    return result;
}

Tileset* Media_Manager_Private::load_tileset(const std::string&name,
                                               const boost::property_tree::ptree& pt,
                                               const std::string& dir)
{
    using namespace boost::property_tree;
    std::string source = pt.get("<xmlattr>.source","");

    if ( !source.empty() )
        return load_tileset(data_or_relative(source,dir));

    // Don't re-create an already existing tileset
    if ( tilesets.count(name) )
        return tilesets[name];

    source = pt.get("image.<xmlattr>.source","");
    Tileset* tileset = nullptr;

    if ( source.empty() )
    {
        Image_Collection* ic = new Image_Collection(name);
        tileset = ic;
        for(auto t : pt)
            if ( t.first == "tile" )
            {
                int id = t.second.get("<xmlattr>.id",-1);
                std::string img = t.second.get("image.<xmlattr>.source","");
                if ( id >= 0 && !img.empty() )
                {
                    ic->set_tile(id,Media_Manager::instance().data_id(data_or_relative(img,dir)));
                }
            }
    }
    else
    {
        std::string source_path = data_or_relative(source,dir);
        if ( !boost::filesystem::exists(source_path) )
        {
            Logger::error("Unable to open "+source_path);
            return nullptr;
        }

        tileset = new Grid_Tileset(name,Media_Manager::instance().data_id(source_path),
                                   pt.get("<xmlattr>.tilewidth",0),
                                   pt.get("<xmlattr>.tileheight",0),
                                   pt.get("<xmlattr>.margin",0),
                                   pt.get("<xmlattr>.spacing",0)
        );
    }

    /// \note doesn't support tiled animations and tile collisions

    if ( tileset )
    {
        tilesets[name] = tileset;
    }

    return tileset;
}

Tileset* Media_Manager::load_tileset(const boost::property_tree::ptree& pt, const std::string& dir)
{
    return d->load_tileset(pt.get("<xmlattr>.name",""),pt,dir);
}

void Media_Manager::load_tileset(const boost::property_tree::ptree& pt)
{
    load_tileset(pt,"");
}

Animation* Media_Manager::animation(const std::string& id)
{
    auto it = d->animations.find(id);
    if ( it != d->animations.end() )
        return it->second;
    return nullptr;
}

void Media_Manager::register_animation(Animation* animation)
{
    delete d->animations[animation->id()];
    d->animations[animation->id()] = animation;
}

Animation* Media_Manager::unregister_animation(const std::string& id)
{
    if ( d->animations.count(id) )
    {
        auto t = d->animations[id];
        d->animations.erase(id);
        return t;
    }
    return nullptr;
}

void Media_Manager::delete_animation(const std::string& id)
{
    delete unregister_animation(id);
}

std::string Media_Manager::data_file(const std::string& id)
{
    auto files = data_files(id);
    if ( files.empty() )
        return "";
    return files[0];
}

static boost::filesystem::path data_path_user = getenv("HOME")+std::string("/")+PATH_USER_DATA;
static std::vector<boost::filesystem::path> data_path_roots = {
    PATH_DATA,
    PATH_SOURCE,
    data_path_user
};

std::vector<std::string> Media_Manager::data_files(const std::string& id)
{
    using namespace boost::filesystem;

    std::vector<std::string> paths;

    for ( auto root : data_path_roots )
    {
        path check = root/"data"/id;
        if ( exists(check) )
            paths.push_back(canonical(check).string());
    }

    return paths;
}

std::string Media_Manager::data_id(const std::string& file)
{
    using namespace boost::filesystem;

    for ( auto dir : data_path_roots )
    {
        path check = dir/"data";
        if ( file_in_directory(file,check.string()) )
            return relative_path(file,check.string());
    }
    return file;
}

std::string Media_Manager::relative_path(const std::string& file, const std::string& directory)
{
    using namespace boost::filesystem;

	path pfile = absolute(file);
	path pdir = absolute(directory);
	if ( exists(pfile) )
        pfile = canonical(pfile);
	if ( exists(pdir) )
        pdir = canonical(pdir);

	auto it_file = pfile.begin(), it_dir = pdir.begin();
	while ( it_file != pfile.end() && it_dir != pdir.end() && *it_file == *it_dir)
	{
		++it_file;
		++it_dir;
	}

	path ret;

	for( ; it_dir != pdir.end(); ++it_dir )
		ret /= "..";

	if ( it_dir == pdir.end() )
		for ( ; it_file != pfile.end(); ++it_file )
			ret /= *it_file;

    return ret.string();
}

bool Media_Manager::file_in_directory(const std::string& file, const std::string& directory)
{

    using namespace boost::filesystem;

	path pfile = absolute(file);
	path pdir = absolute(directory);
	if ( exists(pfile) )
        pfile = canonical(pfile);
	if ( exists(pdir) )
        pdir = canonical(pdir);

	auto it_file = pfile.begin(), it_dir = pdir.begin();
	while ( it_file != pfile.end() && it_dir != pdir.end() && *it_file == *it_dir)
	{
		++it_file;
		++it_dir;
	}

	return it_dir == pdir.end();
}


std::string Media_Manager::user_file(const std::string& id)
{
    return (boost::filesystem::path(data_path_user)/id).string();
}

bool Media_Manager::create_path(const std::string& path)
{
    using namespace boost::filesystem;
    if ( is_directory(path) )
        return true;
    try
    {
        create_directories(path);
    }
    catch( const std::exception& ex )
    {
        Logger::err() << "Cannot create directory" << ex.what();
        return false;
    }
    return true;
}

media::Font* Media_Manager::font(const std::string& name)
{
    if ( name == "" )
        return &d->default_font;

    auto it = d->fonts.find(name);
    if ( it != d->fonts.end() )
        return it->second;

    Logger::err() << "Font \""+name+"\" not found, falling back to default";

    return &d->default_font;
}

std::string Media_Manager::font_name(const media::Font* font)
{
    for ( const auto& f : d->fonts )
        if ( f.second == font )
            return f.first;
    return "";
}

void Media_Manager::register_font(const std::string& name, media::Font* font)
{
    if ( d->fonts.empty() )
        d->default_font = *font;
    if ( d->fonts.count(name) == 0 )
        d->fonts[name] = font;
}

void Media_Manager::set_sefault_font(const std::string& name)
{
    d->default_font = *font(name);
}

void Media_Manager::load_graphics(const std::string& file_id)
{
    if ( file_id.empty() ) return;

    using namespace boost::property_tree;

    ptree pt;
    try{
        read_xml(data_file(file_id),pt,xml_parser::trim_whitespace|xml_parser::no_comments);
        load_graphics(pt);
    } catch (const std::exception& ex)
    {
        Logger::err() << "Error when loading" << file_id << ':' << ex.what();
    }
}

void Media_Manager::load_graphics(const boost::property_tree::ptree& pt)
{
    for( const auto& ts : pt )
        load( ts.first, ts.second );
}

bool Media_Manager::load(const std::string& element, const boost::property_tree::ptree& pt)
{
    auto it = d->load_methods.find(element);
    if ( it == d->load_methods.end() )
        return false;
    (this->*(it->second))(pt);
    return true;
}

void Media_Manager::load_animation(const boost::property_tree::ptree& pt)
{
    if ( media::Animation* animation = Animation::from_properties(
            misc::tree_to_properties(pt) ) )
        register_animation(animation);
}

sf::SoundBuffer& Media_Manager::sound(const std::string& id)
{
    auto it = d->sounds.find(id);
    if ( it != d->sounds.end() )
        return it->second;
    std::string file = data_file(id);
    if ( !file.empty() )
        d->sounds[id].loadFromFile(file);
    return d->sounds[id];
}


sf::Shader* Media_Manager::shader(const std::string& name)
{
    if ( !sf::Shader::isAvailable() )
        return nullptr;

    auto it = d->shaders.find(name);
    if ( it != d->shaders.end() )
        return it->second;
    std::string file = data_file(name);
    if ( !file.empty() && boost::filesystem::is_regular(file) )
    {
        sf::Shader* shader = new sf::Shader;
        if ( shader->loadFromFile(file,sf::Shader::Fragment) )
        {
            return d->shaders[name] = shader;
        }
        delete shader;
    }

    Logger::err() << "Unable to load shader" << name;
    return nullptr;

}

const std::vector<sf::Shader*>& Media_Manager::active_shaders() const
{
    return d->active_shaders;
}

void Media_Manager::set_shaders(const std::vector<std::string>& names)
{
    d->active_shaders.clear();
    for ( const auto& name : names )
    {
        auto shad = shader(name);
        if ( shad )
            d->active_shaders.push_back(shad);
    }
}

void Media_Manager::load_frame(const boost::property_tree::ptree& pt)
{
    auto name = pt.get_optional<std::string>("<xmlattr>.name");
    if ( !name )
        return;
    ui::Frame *frame = new ui::Frame;
    frame->set_properties(misc::tree_to_properties(pt));
    register_frame(*name,frame);
}

void Media_Manager::register_frame(const std::string& name, ui::Frame* frame)
{
    auto it = d->frames.find(name);
    if ( it != d->frames.end() )
        delete it->second;
    d->frames[name] = frame;
}

ui::Frame* Media_Manager::frame(const std::string&name) const
{
    auto it = d->frames.find(name);
    if ( it != d->frames.end() )
        return it->second;
    return nullptr;
}


void Media_Manager::load_bar(const boost::property_tree::ptree& pt)
{
    auto name = pt.get_optional<std::string>("<xmlattr>.name");
    if ( !name )
        return;
    ui::Bar *bar = new ui::Bar;
    bar->set_properties(misc::tree_to_properties(pt));
    register_bar(*name,bar);
}


void Media_Manager::register_bar(const std::string& name, ui::Bar* bar)
{
    auto it = d->bars.find(name);
    if ( it != d->bars.end() )
        delete it->second;
    d->bars[name] = bar;
}

ui::Bar* Media_Manager::bar(const std::string&name) const
{
    auto it = d->bars.find(name);
    if ( it != d->bars.end() )
        return it->second;
    return nullptr;
}

