/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "animation.hpp"
#include "media/tileset.hpp"
#include "media/media_manager.hpp"
#include "misc/math_utils.hpp"
#include <random>
#include "misc/logger.hpp"
#include "eval/parser.hpp"

using namespace media;

/// \pimpl{Animation}
struct media::Animation_Private
{
    std::string     id;             ///< Animation unique identifier
    Tileset*        tileset;        ///< Source tileset
    int             start_tile;     ///< First tile in the animation
    int             n_frames;       ///< Number of frames in the animation
    int             speed;          ///< Animation speed (fps)
    long            flags;          ///< Animation flags
    long            user_flags = 0; ///< General purpose user-defined flags
    geo::Rectangle  bounding_rect;  ///< Fixed bounding rect
    geo::Point      offset;         ///< Offset of the image

    Animation_Private(const std::string& id, const std::string& tileset,
                      int start_tile, int n_frames, int speed, long flags)
        : id(id), tileset(media::Media_Manager::instance().tileset(tileset)),
            start_tile(start_tile), n_frames(n_frames), speed(speed), flags(flags)
    {
    }
};


Animation::Animation(const std::string& id, const std::string& tileset,
                     int start_tile, int n_frames, int speed, long flags)
    : d (new Animation_Private(id,tileset,start_tile,n_frames,speed,flags))
{}

PIMPL_STRUCTORS(Animation)

const std::string& Animation::id() const
{
    return d->id;
}

void Animation::get_sprite(int frame, sf::Sprite& sprite) const
{
    if ( d->tileset )
    {
        d->tileset->get_tile(d->start_tile+frame,sprite);
        sf::IntRect ir = sprite.getTextureRect();
        ir.left += d->offset.x;
        ir.top += d->offset.y;
        sprite.setTextureRect(ir);
    }
}

int Animation::next_frame(int frame, int nth) const
{
    frame += nth-1;
    if ( frame < 0 )
    {
        if ( d->flags & RANDOMIZED )
            frame = math::random(d->n_frames-1);
        else
            frame = 0;
    }
    else if ( d->speed > 0 )
    {
        frame++;
        if ( frame >= d->n_frames )
            frame = ( d->flags & NOLOOP ) ? d->n_frames-1 : frame%d->n_frames;
    }
    return frame;
}

int Animation::fps() const
{
    return d->speed;
}

misc::duration Animation::frame_time() const
{
    if ( !d->speed )
        return misc::duration::zero();
    return misc::milliseconds(1000/d->speed);
}

Animation* Animation::from_properties(const misc::Properties& properties)
{
    std::string name = (std::string) properties["id"] ;
    if ( name.empty() )
    {
        Logger::err() << "Attempting to create an animation without ID";
        return nullptr;
    }

    long flags = NOFLAG;
    if ( properties["random"] )
        flags |= RANDOMIZED;
    if ( properties["noloop"] )
        flags |= NOLOOP;

    Animation* anim = new Animation(name,
                         (std::string)properties["tileset"],
                         (long)properties["first"],
                         (long)properties["length"],
                         (long)properties["fps"],
                         flags);
    anim->d->bounding_rect.x = (long)properties["bounds.x"];
    anim->d->bounding_rect.y = (long)properties["bounds.y"];
    anim->d->bounding_rect.width = (long)properties["bounds.width"];
    anim->d->bounding_rect.height = (long)properties["bounds.height"];
    anim->d->offset.x = (long)properties["offset.x"];
    anim->d->offset.y = (long)properties["offset.y"];
    anim->d->user_flags = (long)eval::Parser().evaluate((std::string)properties["flags"],eval::Context());
    return anim;
}


geo::Rectangle Animation::bounding_rect(int frame) const
{
    if ( d->bounding_rect.is_valid() )
        return d->bounding_rect;

    if ( ! d->tileset || frame >= d->n_frames )
        return geo::Rectangle();
    if ( frame < 0 )
        frame = 0;
    geo::Size sz = d->tileset->tile_size(frame - d->n_frames);
    return geo::Rectangle(geo::Point(),sz);
}



void Animation::set_bounding_rect(const geo::Rectangle & rect)
{
    d->bounding_rect = rect;
}

bool Animation::last_frame(int index) const
{
    return (d->flags&NOLOOP) && index >= d->n_frames-1;
}

const geo::Point& Animation::offset() const
{
    return d->offset;
}

void Animation::set_offset(const geo::Point& p)
{
    d->offset = p;
}


long Animation::user_flags() const
{
    return d->user_flags;
}
