/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef MEDIA_MANAGER_HPP
#define MEDIA_MANAGER_HPP

#include <string>
#include <SFML/Graphics.hpp>
#include <boost/property_tree/ptree.hpp>
#include <SFML/Audio.hpp>
#include "media/ui/frame.hpp"
#include "media/ui/bar.hpp"

/**
 * \brief Namespace for media and data management
 */
namespace media{

class Tileset;
class Animation;
class Font;

/**
 * \brief Manage media files
 *
 * This singleton class will manage paths, load resources and clean up at the end
 *
 * \note Media file identifiers are the name of the files, relative to the data directory
 */
class Media_Manager
{
private:
    class Media_Manager_Private *d;

    Media_Manager();
    Media_Manager(const Media_Manager&) = delete;
    ~Media_Manager();

public:
    /**
     * \brief Get singleton instance
     */
    static Media_Manager& instance();

/** \name Graphics
 * Functions handling miscellaneous graphical resources
 */
///\{
    /**
     * \brief Get an image texture
     */
    sf::Texture& image(const std::string& id);

	/**
	 * \brief Load animations and tilesets defined in a data file
	 * \param file_id   File ID (path relative to data)
	 */
    void load_graphics(const std::string& file_id);

    /**
     * \brief Load animations and tilesets defined in a property tree
     */
    void load_graphics(const boost::property_tree::ptree& pt);

    /**
     * \brief Load any supported tree element
     * \param element   Name of the element from which \c pt comes from (eg: animation, tileset...)
     * \param pt        Tree structure
     * \return \b true on success
     */
    bool load(const std::string& element, const boost::property_tree::ptree& pt);

///\}

/** \name Tileset
* Functions handling tilesets
*/
///\{
    /**
     * \brief Retrieve a stored tileset
     */
    Tileset* tileset(const std::string& id);

    /**
     * \brief register a tileset
     * \param tileset Tileset to register (takes ownership)
     */
    void register_tileset(Tileset* tileset);

    /**
     * \brief remove registered tileset
     * \param id Tileset ID
     * \return The tileset matching the given ID (nullptr if none match)
     * \note Releases ownership but doesn't free memory
     * \see delete_tileset();
     */
    Tileset* unregister_tileset(const std::string& id);

    /**
     * \brief Unregister and delete tileset
     */
    void delete_tileset(const std::string& id);

    /**
     * \brief Create and register a tileset from its XML description
     * \param pt    Property tree pointing to a map.tileset
     * \param dir   Directory to base the file searches on
     * \return The created tileset
     */
    Tileset* load_tileset(const boost::property_tree::ptree& pt, const std::string& dir);

    /**
     * \brief Create and register a tileset from its XML description
     */
    void load_tileset(const boost::property_tree::ptree& pt);
    /**
     * \brief Create and register a tileset from its XML description
     * \param file_id    Tileset data file id
     */
    Tileset* load_tileset(const std::string& file_id);
///\}

/** \name Animation
 * Functions handling animations
 */
///\{
    /**
     * \brief Retrieve a stored animation
     */
    Animation* animation(const std::string& id);

    /**
     * \brief register a animation
     * \param animation Animation to register (takes ownership)
     */
    void register_animation(Animation* animation);

    /**
     * \brief remove registered animation
     * \param id Animation ID
     * \return The animation matching the given ID (nullptr if none match)
     * \note Releases ownership but doesn't free memory
     * \see delete_animation();
     */
    Animation* unregister_animation(const std::string& id);

    /**
     * \brief Unregister and delete animation
     */
    void delete_animation(const std::string& id);

    /**
     * \brief Load an animation from property tree
     */
    void load_animation(const boost::property_tree::ptree& pt);
///\}

/** \name User interface
 * Functions managing user interface elements
 */
///\{
    /**
     * \brief Get font from name
     */
	media::Font* font ( const std::string& name="");

	/**
	 * \brief Get name for the given font
	 */
	std::string font_name(const media::Font* font);

	/**
	 * \brief Register a font
	 * \param name  Font name, used to access the font later on
	 * \param font  Dynamic font object, takes ownership
	 */
	void register_font( const std::string& name, media::Font* font );

	/**
	 * \brief Set the default font
	 * \param name  Name of a registered font
	 * \note The font must be already registered for this to work
	 */
	void set_sefault_font ( const std::string& name );

	/**
	 * \brief Load a frame definition from a property tree
	 */
	void load_frame(const boost::property_tree::ptree& pt);

	/**
	 * \brief Register a named frame (takes ownership)
	 */
	void register_frame(const std::string& name, ui::Frame* frame);

	/**
	 * \brief Get a regitered frame
	 * \return the found frame or nullptr if it has not been found
	 */
	ui::Frame* frame(const std::string&name) const;


	/**
	 * \brief Load a bar definition from a property tree
	 */
	void load_bar(const boost::property_tree::ptree& pt);

	/**
	 * \brief Register a named bar (takes ownership)
	 */
	void register_bar(const std::string& name, ui::Bar* bar);

	/**
	 * \brief Get a regitered bar
	 * \return the found bar or nullptr if it has not been found
	 */
	ui::Bar* bar(const std::string&name) const;
///\}

/** \name Shaders
 * Functions managing shaders
 */
///\{
    /**
     * \brief Get (fragment) shader by file name
     * \return A shader instance or nullptr if it couldn't be loaded
     */
    sf::Shader* shader(const std::string& name);

    /**
     * \brief Get the currently active shaders (apply them in order)
     */
    const std::vector<sf::Shader*>& active_shaders() const;

    /**
     * \brief Set active shaders
     */
    void set_shaders(const std::vector<std::string>& names);
///\}

/** \name Audio
 * Functions which handle audio resources
 */
///\{
    /**
     * \brief Get sound buffer from file id
     */
    sf::SoundBuffer& sound(const std::string& id);
///\}

/** \name Filesystem
 * Functions which handle files and paths
 */
///\{
	/**
	 * \brief Get absolute path of a given data file
	 *
	 * It will detect which of the available data paths contains the given file.
	 *
	 * \param id	File id (relative path)
	 * \return The full path.
	 */
	std::string data_file(const std::string& id);


	/**
	 * \brief Get absolute paths of a given data file
	 *
	 * It will detect all the available data paths contains the given file.
	 *
	 * \param id	File id (relative path)
	 * \return List of paths.
	 */
	std::vector<std::string> data_files(const std::string& id);

	/**
	 * \brief Get the id of a given data file
	 *
	 * Reverse of data_file
	 *
	 * \param file  File path
	 * \return id (path relative the the containing data directory)
	 */
	std::string data_id(const std::string& file);

    /**
     * \brief Make a path relative to a given directory
     * \param file      Target file
     * \param directory Directory to base the resulting path on
     * \return A path from \c directory to \c file
     */
	std::string relative_path(const std::string &file, const std::string& directory);

	/**
	 * \brief Whether a file is cointained in a given directory
	 * \param file      Path to a file
	 * \param directory Directory
	 * \note This function doesn't check the contents of \c directory to see if
	 *  there's a file with a given name, but checks that the path to \c file
	 *  contains the directory
	 * \note If the given paths really exist, symbolic links and dots are expanded
	 */
	bool file_in_directory(const std::string &file, const std::string& directory);

	/**
	 * \brief Get absolute path of a given user data file
	 *
	 * It will return a path on the user's writable directory
	 *
	 * \param id	File id (relative path)
	 * \return The full path.
	 * \note This function doesn't check if the file exists
	 */
	std::string user_file(const std::string& id);

	/**
	 * \brief Ensure that a given path exists
	 * \return \c true if the path exists as a directory or has been created as such
	 */
	bool create_path(const std::string& path);
///\}

};

} // namespace media

#endif // MEDIA_MANAGER_HPP
