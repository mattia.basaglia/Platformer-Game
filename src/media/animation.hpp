/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef ANIMATION_HPP
#define ANIMATION_HPP

#include <SFML/Graphics/Sprite.hpp>
#include "misc/time.hpp"
#include <map>
#include <string>
#include "misc/game_value.hpp"
#include "misc/geometry.hpp"
#include "misc/boilerplate.hpp"


namespace media {

class Animation
{
public:
    enum Animation_Flags
    {
        NOFLAG      = 0x0,
        NOLOOP      = 0x1, ///< Don't go back to first frame once finished
        RANDOMIZED  = 0x2, ///< First frame is random
    };

private:
    class Animation_Private *d;///< Pimpl

public:
    /**
     * \brief Constructor
     * \param id            Animation id
     * \param tileset       Tileset id
     * \param start_tile    Tile index representing the first frame
     * \param n_frames      Number of frames in the animation
     * \param fps           Animation speed in frames per second, if 0 it'll act as a static image
     * \param flags         Animation flags
     */
    Animation( const std::string& id, const std::string& tileset, int start_tile, int n_frames, int fps, long flags = NOFLAG );
    DECLARE_STRUCTORS(Animation)

    /**
     * \brief Get id
     */
     const std::string& id() const;

    /**
     * \brief Set a sprite to point to a given frame
     * \param frame     Valid frame index, use next_frame() to obtain proper values
     * \param sprite    Target sprite
     */
    void get_sprite(int frame,sf::Sprite& sprite) const;

    /**
     * \brief Get next frame in the animation
     * \param frame     Previous frame number, if negative returns the first frame
     * \param nth       Get the next nth frame
     */
    int next_frame(int frame, int nth=1) const;

    /**
     * \brief Animation speed in frames per second
     */
    int fps() const;

    /**
     * \brief Get the time a frame shall last for
     * \note Returns a zero time if the animation has zero speed
     */
    misc::duration frame_time() const;

    /**
     * \brief Get bounding box of the given frame
     */
    geo::Rectangle bounding_rect(int frame) const;

    /**
     * \brief Set a fixed bounding rect instead of using the frame size
     */
    void set_bounding_rect(const geo::Rectangle & rect);

    /**
     * \brief Create an object based on a set of properties
     * \note The properties need to contain the key "id" corresponding to a
     * non-empty string
     */
    static Animation* from_properties(const misc::Properties& properties);

    /**
     * \brief Get whether the given frame is the last of the animation
     */
    bool last_frame(int index) const;

    /**
     * \brief Get offset for this animation
     */
    const geo::Point& offset() const;
    /**
     * \brief Set offset for this animation
     */
    void set_offset(const geo::Point&);

    /**
     * \brief User-defined flags
     */
    long user_flags() const;
};

} // namespace media

#endif // ANIMATION_HPP
