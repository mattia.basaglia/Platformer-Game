/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef MEDIA_FONT_HPP
#define MEDIA_FONT_HPP

#include <SFML/Graphics/Font.hpp>

namespace media {

class Font : public sf::Font
{
public:
    using sf::Font::Font;

    const sf::Texture& getTexture (unsigned int characterSize) const
    {
        sf::Texture&  tex = const_cast<sf::Texture&>(sf::Font::getTexture(characterSize));
        tex.setSmooth(false);
        return tex;
    }

    /**
     * \brief Get the distance of an ascender from the baseline
     */
    float ascender (unsigned int size) const
    {
        return -getGlyph('h', size, false).bounds.top;
    }
};

} // namespace media


#endif // MEDIA_FONT_HPP
