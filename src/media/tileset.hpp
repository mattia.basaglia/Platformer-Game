/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef TILESET_HPP
#define TILESET_HPP
#include <cstdint>
#include <string>
#include <SFML/Graphics.hpp>
#include "misc/geometry.hpp"
#include "misc/boilerplate.hpp"

namespace media {

typedef uint32_t Tile_Position;
typedef uint16_t Tile_Coordinate;
inline constexpr Tile_Position tile_pos(Tile_Coordinate x, Tile_Coordinate y)
{
    return (Tile_Position(y)<<16) | x;
}
inline constexpr Tile_Coordinate tile_y(Tile_Position p)
{
    return (p >> 16) & 0xffff;
}
inline constexpr Tile_Coordinate tile_x(Tile_Position p)
{
    return p & 0xffff;
}

/**
 * \brief Abstract base class for tileset
 */
class Tileset
{
    std::string m_id;

public:
    Tileset(const std::string& id);
    virtual ~Tileset() {}

    /**
     * \brief Get the index^th tile in the tileset
     * \param index         Tile index (1-based)
     * \param [out] sprite  Sprite to modify to represent the tile
     */
    virtual void get_tile(int index, sf::Sprite& sprite) = 0;

    /**
     * \brief Get the index^th tile in the tileset
     */
    sf::Sprite get_tile(int index);

    /**
     * \brief Number of tiles in the tileset
     */
    virtual int tile_number() const = 0;

    /**
     * \brief Get the tileset id
     */
    const std::string& id() const;

    /**
     * \brief Size of a single tile
     */
    virtual geo::Size tile_size(int index) const = 0;

};

/**
 * \brief A tileset created from a single image, divided by a sqare grid
 */
class Grid_Tileset : public Tileset
{
    class Grid_Tileset_Private* d; ///< Pimpl
public:
    /**
     * \brief Constructor
     * \param id            Tileset id
     * \param image         Source image file id (path relative to data)
     * \param tile_width    Width of a tile
     * \param tile_height   Height of a tile. If zero same as \c tile_width
     * \param grid_margin   Space between the image margin and the first cell
     * \param grid_spacing  Space between adjacent cells
     */
    Grid_Tileset(const std::string& id,
                 const std::string& image,
                 int tile_width = 16,
                 int tile_height = 0, // <= 0 -> tile_width
                 int grid_margin = 0,
                 int grid_spacing = 0
                 );
    DECLARE_STRUCTORS(Grid_Tileset)
    int tile_number() const override;
    void get_tile(int index, sf::Sprite& sprite) override;

    geo::Size tile_size(int index) const override;
};

/**
 * \brief A tileset created from a collection of images
 */
class Image_Collection : public Tileset
{
    class Image_Collection_Private* d; ///< Pimpl
public:
    Image_Collection(const std::string& id);
    DECLARE_STRUCTORS(Image_Collection)
    /**
     * \brief Set the image for a given tile index
     * \param index Tile index
     * \param image Image file id (path relative to data)
     */
    void set_tile(int index, const std::string& image);
    int tile_number() const override;
    void get_tile(int index, sf::Sprite& sprite) override;

    geo::Size tile_size(int index) const override;
};

} // namepsace media


#endif // TILESET_HPP
