/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "media/ui/frame.hpp"
#include "media/media_manager.hpp"
#include "misc/geometry_convert.hpp"

using namespace media::ui;

DECLARE_PROPERTY_SET_FUNCTOR(Frame,tileset,[](Frame&f,const misc::Value&v){
    f.set_tileset(media::Media_Manager::instance().tileset((std::string)v));
})
DECLARE_PROPERTY_GET_FUNCTOR(Frame,tileset,[](const Frame&f){
    return f.tileset() ? f.tileset()->id() : "";
})
DECLARE_PROPERTY(Frame,top_left,top_left,set_top_left)
DECLARE_PROPERTY(Frame,left,left,set_left)
DECLARE_PROPERTY(Frame,bottom_left,bottom_left,set_bottom_left)
DECLARE_PROPERTY(Frame,bottom,bottom,set_bottom)
DECLARE_PROPERTY(Frame,bottom_right,bottom_right,set_bottom_right)
DECLARE_PROPERTY(Frame,right,right,set_right)
DECLARE_PROPERTY(Frame,top_right,top_right,set_top_right)
DECLARE_PROPERTY(Frame,top,top,set_top)
DECLARE_PROPERTY(Frame,center,center,set_center)

struct media::ui::Frame_Private
{
    media::Tileset* tileset = nullptr;

    long top_left = -1;
    long left = -1;
    long bottom_left = -1;
    long bottom = -1;
    long bottom_right = -1;
    long right = -1;
    long top_right = -1;
    long top = -1;
    long center = -1;

};

Frame::Frame() : d ( new Frame_Private ) {}
PIMPL_STRUCTORS_INH(Frame,misc::Mirror)

media::Tileset* Frame::tileset() const
{
    return d->tileset;
}
void Frame::set_tileset ( media::Tileset* tileset )
{
    d->tileset = tileset;
}

geo::Rectangle Frame::draw(const geo::Rectangle&area, sf::RenderTarget& target, sf::RenderStates states) const
{
    if ( !d->tileset )
        return geo::Rectangle();

    sf::Sprite sprite;
    int left = d->top_left;
    int mid = d->top;
    int right = d->top_right;
    float dx = std::max(1.f,d->tileset->tile_size(left).width);
    float dy = std::max(1.f,d->tileset->tile_size(center()).height);
    for ( float y = area.top(); y < area.bottom(); y += dy )
    {
        for ( float x = area.left(); x < area.right(); x += dx )
        {
            if ( x < area.left() + dx )
                d->tileset->get_tile(left,sprite);
            else if ( x >= area.right() - dx )
                d->tileset->get_tile(right,sprite);
            else
                d->tileset->get_tile(mid,sprite);
            sprite.setPosition(x,y);
            target.draw(sprite,states);
            dx = std::max(1.f,sprite.getLocalBounds().width);
        }

        dy = std::max(1.f,sprite.getLocalBounds().height);

        if ( y >= area.bottom() - 2*dy )
        {
            left = d->bottom_left;
            mid = d->bottom;
            right = d->bottom_right;
        }
        else
        {
            left = d->left;
            mid = d->center;
            right = d->right;
        }
    }
    return geo::Rectangle(area.top_left(),geo::conv::rect(sprite.getGlobalBounds()).bottom_right());
}

void Frame::set_top_left(long n)
{
    d->top_left = n;
}
long Frame::top_left() const
{
    return d->top_left;
}
void Frame::set_left(long n)
{
    d->left = n;
}
long Frame::left() const
{
    return d->left;
}
void Frame::set_top(long n)
{
    d->top = n;
}
long Frame::top() const
{
    return d->top;
}
void Frame::set_bottom_right(long n)
{
    d->bottom_right = n;
}
long Frame::bottom_right() const
{
    return d->bottom_right;
}
void Frame::set_bottom(long n)
{
    d->bottom = n;
}
long Frame::bottom() const
{
    return d->bottom;
}
void Frame::set_right(long n)
{
    d->right = n;
}
long Frame::right() const
{
    return d->right;
}
void Frame::set_bottom_left(long n)
{
    d->bottom_left = n;
}
long Frame::bottom_left() const
{
    return d->bottom_left;
}
void Frame::set_top_right(long n)
{
    d->top_right = n;
}
long Frame::top_right() const
{
    return d->top_right;
}
void Frame::set_center(long n)
{
    d->center = n;
}
long Frame::center() const
{
    return d->center;
}
