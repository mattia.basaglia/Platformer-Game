/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef SHADED_TEXT_HPP
#define SHADED_TEXT_HPP

#include <SFML/Window/Event.hpp>
#include <SFML/Graphics.hpp>
#include "media/font.hpp"
#include "misc/mirror.hpp"
#include "misc/geometry.hpp"
#include "misc/color.hpp"

namespace media {
/**
 * \brief Namespace for user interface elements
 */
namespace ui {

/**
 * \brief Draw text with a shadow underneath
 * \todo Italic/underline
 * \todo Alignment
 * \par Properties
 *      * \c string \b text         Displayed text
 *      * \c string \b font         Font name
 *      * \c number \b font.size    Character size
 *      * \c string \b color        Foreground color
 *      * \c string \b shadow.color Shadow color
 *      * \c number \b shadow.offset Shadow offset
 *      * \c number \b x            X Coordinate
 *      * \c number \c y            Y Coordinate
 *      * \c float  \b align        0 = left, 1 = right
 *      * \c boolean \b bold        Bold
 *      * \c boolean \b italic      Italic
 */
class Shaded_Text : public sf::Drawable, public sf::Transformable, public misc::Mirror
{
    PROPERTY_MAGIC(Shaded_Text)

private:
    class Shaded_Text_Private *d;

protected:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

public:
    /**
     * \brief Constructor
     * \param text  Text to be displayed
     * \param font  Font name, as registered to Media_Manager
     * \param size  Pixel size of the characters
     */
    explicit Shaded_Text (const std::string &text="", const std::string &font="", unsigned int size=16);
    Shaded_Text(const Shaded_Text&) = delete;
    virtual ~Shaded_Text();

    /**
     * \brief Set the text to be displayed
     */
    void set_text( const std::string& text );
    /**
     * \brief Get the displayed text
     */
    const std::string& text() const;

    /**
     * \brief Set the font used to draw the text
     */
    void set_font(const media::Font* font);
    /**
     * \brief Set the font used to draw the text
     */
    const media::Font* font() const;

    /**
     * \brief Set the character size in pixels
     */
    void set_size(unsigned int size);

    /**
     * \brief Get the character size in pixels
     */
    unsigned int size() const;

    /**
     * \brief Set the foreground color
     */
    void set_color(const misc::Color& color);

    /**
     * \brief Get the foreground color
     */
    const misc::Color& color() const;

    /**
     * \brief Whether to display the text in bold
     */
    bool bold() const;

    /**
     * \brief Set whether to display the text in bold
     */
    void set_bold(bool bold);

    /**
     * \brief Whether to display the text in italic
     */
    bool italic() const;

    /**
     * \brief Set whether to display the text in italic
     */
    void set_italic(bool italic);

    /**
     * \brief Get the position of the index^th character
     */
    geo::Point character_pos(std::string::size_type index) const;

    /**
     * \brief Get the bounding rect
     */
    geo::Rectangle bounding_rect() const;

    /**
     * \brief Get the local bounding rect
     */
    geo::Rectangle local_bounding_rect() const;

    /**
     * \brief Get Shadow color
     */
    const misc::Color& shadow_color() const;
    /**
     * \brief Set Shadow color
     */
    void set_shadow_color(const misc::Color& color) const;

    /**
     * \brief get shadow offset
     */
    long shadow_offset() const;

    /**
     * \brief set shadow offset
     */
    void set_shadow_offset(long offset);

    /**
     * \brief Get horizontal alignment (0 = left, 1 = right)
     */
    float align() const;

    /**
     * \brief Set horizontal alignment
     * \param align     Alignment (0 = left, 1 = right)
     */
    void set_align(float align);
};

/**
 * \brief Graphical element allowing the user to enter some text
 * \par Properties
 *      * \c bool \b editable   Whether the text is currently being edited
 */
class Shaded_Text_Input : public Shaded_Text
{
    PROPERTY_MAGIC_INHERIT(Shaded_Text_Input,Shaded_Text)
private:
    class Shaded_Text_Input_Private* d;///< Pimpl

protected:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

public:
    /**
     * \brief Constructor
     * \param text  Text to be displayed
     * \param font  Font name, as registered to Media_Manager
     * \param size  Pixel size of the characters
     */
    explicit Shaded_Text_Input (const std::string &text="", const std::string &font="", unsigned int size=16);
    ~Shaded_Text_Input();

    /**
     * \brief Whether it should parse events
     */
    bool active() const;

    /**
     * \brief Set whether it should parse events
     */
    void set_active(bool active);

    /**
     * \brief Handle events (when active)
     */
    void event(const sf::Event &ev);

};

}} // namespace media::ui
#endif // SHADED_TEXT_HPP
