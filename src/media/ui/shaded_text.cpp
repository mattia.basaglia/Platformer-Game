/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "shaded_text.hpp"
#include "media/media_manager.hpp"
#include "config.hpp"
#include "misc/color.hpp"
#include "misc/geometry_convert.hpp"

using namespace media::ui;

DECLARE_PROPERTY_SET_FUNCTOR(Shaded_Text,text,[](Shaded_Text&t,const misc::Value&v){
    t.set_text((std::string)v); })
DECLARE_PROPERTY_GET_FUNCTOR(Shaded_Text,text,[](const Shaded_Text&t){
    return misc::Value((std::string)t.text()); })

DECLARE_PROPERTY_SET_FUNCTOR(Shaded_Text,font,[](Shaded_Text&t,const misc::Value&v){
    t.set_font(media::Media_Manager::instance().font((std::string)v)); })
DECLARE_PROPERTY_GET_FUNCTOR(Shaded_Text,font,[](const Shaded_Text&t){
    return misc::Value(media::Media_Manager::instance().font_name(t.font())); })

DECLARE_PROPERTY_RENAME(Shaded_Text,"font.size",font_size,size,set_size)

DECLARE_PROPERTY_GET_FUNCTOR(Shaded_Text,color,[](const Shaded_Text&t){
    return misc::Value(t.color().name()); })
DECLARE_PROPERTY_SET_FUNCTOR(Shaded_Text,color,[](Shaded_Text&t,const misc::Value&v){
    t.set_color((std::string)v); })

DECLARE_PROPERTY_GET_FUNCTOR_EX(Shaded_Text,"shadow.color",shadow_color,[](const Shaded_Text&t){
    return misc::Value(t.shadow_color().name()); })
DECLARE_PROPERTY_SET_FUNCTOR_EX(Shaded_Text,"shadow.color",shadow_color,[](Shaded_Text&t,const misc::Value&v){
    t.set_shadow_color((std::string)v); })

DECLARE_PROPERTY_RENAME(Shaded_Text,"shadow.offset",shadow_offset,shadow_offset,set_shadow_offset)

DECLARE_PROPERTY_GET_FUNCTOR(Shaded_Text,x,[](const Shaded_Text&t){return misc::Value::from(t.getPosition().x);})
DECLARE_PROPERTY_SET_FUNCTOR(Shaded_Text,x,[](Shaded_Text&t,const misc::Value&v){return t.setPosition(v.to<float>(),t.getPosition().y);})
DECLARE_PROPERTY_GET_FUNCTOR(Shaded_Text,y,[](const Shaded_Text&t){return misc::Value::from(t.getPosition().y);})
DECLARE_PROPERTY_SET_FUNCTOR(Shaded_Text,y,[](Shaded_Text&t,const misc::Value&v){return t.setPosition(t.getPosition().x,v.to<float>());})

DECLARE_PROPERTY(Shaded_Text,bold,bold,set_bold)
DECLARE_PROPERTY(Shaded_Text,italic,italic,set_italic)
DECLARE_PROPERTY(Shaded_Text,align,align,set_align)

DECLARE_METHOD_FUNCTOR(Shaded_Text,character_x,[](Shaded_Text& obj, const std::vector<misc::Value>& args) {
    return misc::Value::from(obj.character_pos(args.empty()?0:(long)args[0]).x);    })
DECLARE_METHOD_FUNCTOR(Shaded_Text,character_y,[](Shaded_Text& obj, const std::vector<misc::Value>& args) {
    return misc::Value::from(obj.character_pos(args.empty()?0:(long)args[0]).y);    })

/// \pimpl{Shaded_Text}
struct media::ui::Shaded_Text_Private
{

    enum Style
    {
        REGULAR = 0,
        BOLD = 1,
        ITALIC = 2,
    };

    std::string         text;

    misc::Color         color;
    misc::Color         shadow_color;
    long                shadow_offset;

    const media::Font*  font;
    unsigned int        size;
    long                style = 0;
    float               align = 0;

    std::vector<sf::VertexArray> line_vertices;

    Shaded_Text *       p;

    Shaded_Text_Private(Shaded_Text *p, const std::string& text, const std::string& font, unsigned int size)
        :   text(text),
            color(255,255,255),
            shadow_color(0,0,0),
            shadow_offset(1),
            font(media::Media_Manager::instance().font(font)),
            size(size),
            p(p)
    {}

    void for_each_vertex(const std::function<void(sf::Vertex&)>& func)
    {
        for ( auto& line : line_vertices )
            for ( unsigned i = 0; i < line.getVertexCount(); i++ )
                func(line[i]);
    }

    void dirty_geometry()
    {
        line_vertices.clear();
    }

    float line_height()
    {
        return font->getLineSpacing(size);
    }

    void update_geometry()
    {
        if ( !line_vertices.empty() || !font || text.empty() )
            return;

        dirty_geometry();

        bool bold = style & BOLD;
        float italic = (style & ITALIC) ? 0.208f : 0.f; // 12 degrees

        geo::Point pos(0,font->ascender(size));

        char previous = 0;

        line_vertices.push_back(sf::VertexArray(sf::Quads));

        for ( std::string::size_type i = 0; i < text.size(); i++ )
        {
            char current = text[i];
            pos.x += font->getKerning(previous, current, size);
            previous = current;
            if ( current == '\n' )
            {
                pos.y += line_height();
                pos.x = 0;
                line_vertices.push_back(sf::VertexArray(sf::Quads));

                continue;
            }

            const sf::Glyph& glyph = font->getGlyph(current, size, bold);
            geo::Rectangle glyph_rect = geo::conv::rect(glyph.bounds);


            if ( current == ' ' )
                glyph_rect.width = glyph.advance;
            else if ( current == '\t' )
                glyph_rect.width = size; /// \todo tab width

            float it_top = italic*glyph_rect.top();
            float it_bottom = italic*glyph_rect.bottom();
            glyph_rect.translate(pos);

            float u1 = glyph.textureRect.left;
            float v1 = glyph.textureRect.top;
            float u2 = glyph.textureRect.left + glyph.textureRect.width;
            float v2 = glyph.textureRect.top + glyph.textureRect.height;

            line_vertices.back().append(sf::Vertex({glyph_rect.left() - it_top,    glyph_rect.top()   }, shadow_color, {u1, v1} ));
            line_vertices.back().append(sf::Vertex({glyph_rect.left() - it_bottom, glyph_rect.bottom()}, shadow_color, {u1, v2} ));
            line_vertices.back().append(sf::Vertex({glyph_rect.right()- it_bottom, glyph_rect.bottom()}, shadow_color, {u2, v2} ));
            line_vertices.back().append(sf::Vertex({glyph_rect.right()- it_top,    glyph_rect.top()   }, shadow_color, {u2, v1} ));

            pos.x += glyph.advance;

            if ( current == '\t' )
                pos.x += size; /// \todo tab width
        }
    }

    void draw(sf::RenderTarget& target, sf::RenderStates states)
    {
        auto oldtrans = states.transform;
        for ( auto& line : line_vertices )
        {
            if ( line.getVertexCount() > 0 )
            {
                states.transform.translate(-align*line[line.getVertexCount()-1].position.x,0);
                target.draw(line, states);
                states.transform = oldtrans;
            }
        }
    }
};


void Shaded_Text::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    if (d->font)
    {
        d->update_geometry();
        states.transform *= getTransform();
        states.texture = &d->font->getTexture(d->size);

        d->for_each_vertex([this](sf::Vertex& vert){vert.color = d->shadow_color;});
        states.transform.translate(d->shadow_offset,d->shadow_offset);
        d->draw(target,states);

        d->for_each_vertex([this](sf::Vertex& vert){vert.color = d->color;});
        states.transform.translate(-d->shadow_offset,-d->shadow_offset);
        d->draw(target,states);
    }
}

Shaded_Text::Shaded_Text(const std::string& text, const std::string& font, unsigned int size)
    : d(new Shaded_Text_Private(this,text,font,size))
{}

Shaded_Text::~Shaded_Text()
{
    delete d;
}


void Shaded_Text::set_text( const std::string& text )
{
    d->text = text;
    d->dirty_geometry();
}
const std::string& Shaded_Text::text() const
{
    return d->text;
}


void Shaded_Text::set_font(const media::Font* font)
{
    d->font = font;
    d->dirty_geometry();
}

const media::Font* Shaded_Text::font() const
{
    return d->font;
}

void Shaded_Text::set_size(unsigned int size)
{
    d->size = size;
    d->dirty_geometry();
}

unsigned int Shaded_Text::size() const
{
    return d->size;
}

void Shaded_Text::set_color(const misc::Color& color)
{
    d->color = color;
}

const misc::Color& Shaded_Text::color() const
{
    return d->color;
}


bool Shaded_Text::bold() const
{
    return d->style & Shaded_Text_Private::BOLD;
}

void Shaded_Text::set_bold(bool bold)
{
    if ( bold )
        d->style |= Shaded_Text_Private::BOLD;
    else
        d->style &= ~Shaded_Text_Private::BOLD;
    d->dirty_geometry();
}


bool Shaded_Text::italic() const
{
    return d->style & Shaded_Text_Private::ITALIC;
}

void Shaded_Text::set_italic(bool italic)
{
    if ( italic )
        d->style |= Shaded_Text_Private::ITALIC;
    else
        d->style &= ~Shaded_Text_Private::ITALIC;
    d->dirty_geometry();
}

geo::Point Shaded_Text::character_pos(std::string::size_type index) const
{
    if ( !d->font )
        return geo::Point();

    if ( index > d->text.size() )
        index = d->text.size();

    d->update_geometry();
    unsigned line = 0;
    unsigned pos = 0;
    for ( std::string::size_type i = 0; i < index; i++ )
    {
        if ( d->text[i] == '\n' )
        {
            line++;
            pos = 0;
        }
        else
            pos++;
    }

    sf::Vector2f position;
    if ( !d->line_vertices.empty() )
    {
        if ( line >= d->line_vertices.size() )
            line = d->line_vertices.size()-1;
        const auto& vertex_line = d->line_vertices[line];
        if (  vertex_line.getVertexCount() > 0 )
        {
            pos *= 4;
            if ( pos >= vertex_line.getVertexCount() )
                pos = vertex_line.getVertexCount()-1;
            position.x = vertex_line[pos].position.x - d->align*vertex_line[vertex_line.getVertexCount()-1].position.x;
            position.y = line * d->line_height();
        }
    }
    return geo::conv::point(getTransform().transformPoint(position.x,position.y));
}

geo::Rectangle Shaded_Text::bounding_rect() const
{
    return geo::conv::rect(getTransform().transformRect(geo::conv::rect(local_bounding_rect())));
}

geo::Rectangle Shaded_Text::local_bounding_rect() const
{
    d->update_geometry();

    geo::Rectangle r;

    for ( const auto& line : d->line_vertices )
    {
        geo::Rectangle lr = geo::conv::rect(line.getBounds());
        r.unite(lr.translated(-lr.width*d->align,0));
    }
    return r;
}


const misc::Color& Shaded_Text::shadow_color() const
{
    return d->shadow_color;
}

void Shaded_Text::set_shadow_color(const misc::Color& color) const
{
    d->shadow_color = color;
}

long Shaded_Text::shadow_offset() const
{
    return d->shadow_offset;
}

void Shaded_Text::set_shadow_offset(long offset)
{
    d->shadow_offset = offset;
}


float Shaded_Text::align() const
{
    return d->align;
}

void Shaded_Text::set_align(float align)
{
    d->align = align;
}



DECLARE_PROPERTY(Shaded_Text_Input,editable,active,set_active)

/// \pimpl{Shaded_Text_Input}
struct media::ui::Shaded_Text_Input_Private
{
    std::size_t cursor_position = 0;
    bool active = false;
    std::size_t history_position = 0;
    std::vector<std::string> history;
};

Shaded_Text_Input::Shaded_Text_Input(const std::string& text, const std::string& font, unsigned int size)
    : Shaded_Text(text,font,size), d(new Shaded_Text_Input_Private())
{

}

Shaded_Text_Input::~Shaded_Text_Input()
{
    delete d;
}

bool Shaded_Text_Input::active() const
{
    return d->active;
}

void Shaded_Text_Input::set_active(bool active)
{
    d->active = active;
    d->cursor_position = text().size();
    if ( !text().empty() &&
            ( d->history.empty() || text() != d->history.back() ) )
    {
        d->history.push_back(text());
    }
    d->history_position = d->history.size();
}

void Shaded_Text_Input::event(const sf::Event &ev)
{
    if ( d->active )
    {
        if ( ev.type == sf::Event::TextEntered && ev.text.unicode < 128 )
        {
            char c = ev.text.unicode;
            if ( c == '\x1b' )
            {
                d->active = false;
            }
            else if ( c == '\n' || c == '\r' )
            {
                set_text(text()+'\n');
            }
            else if ( c == '\b' )
            {
                if ( d->cursor_position > 0 )
                {
                    std::string s = text();
                    d->cursor_position--;
                    s.erase(d->cursor_position,1);
                    set_text(s);
                }
            }
            else if ( c == 127 )
            {
                if ( d->cursor_position < text().size() )
                {
                    std::string s = text();
                    s.erase(d->cursor_position,1);
                    set_text(s);
                }
            }
            else if ( ev.text.unicode >= 32 || ev.text.unicode == '\t' )
            {
                std::string s = text();
                s.insert(s.begin()+d->cursor_position,c);
                set_text(s);
                d->cursor_position++;
            }
        }
        else if ( ev.type == sf::Event::KeyPressed )
        {
            if ( ev.key.code == sf::Keyboard::Left && d->cursor_position > 0 )
                d->cursor_position--;
            else if ( ev.key.code == sf::Keyboard::Right && d->cursor_position < text().size() )
                d->cursor_position++;
            else if ( ev.key.code == sf::Keyboard::Home )
                d->cursor_position = 0;
            else if ( ev.key.code == sf::Keyboard::End )
                d->cursor_position = text().size();
            else if ( ev.key.code == sf::Keyboard::Up && d->history_position > 0 )
            {
                set_text(d->history[--d->history_position]);
                d->cursor_position = text().size();
            }
            else if ( ev.key.code == sf::Keyboard::Down )
            {
                if ( !d->history.empty() && d->history_position < d->history.size()-1 )
                    set_text(d->history[++d->history_position]);
                else
                {
                    d->history_position = d->history.size();
                    set_text("");
                }
                d->cursor_position = text().size();
            }
        }
    }
}

void Shaded_Text_Input::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    sf::Vector2f cursor = geo::conv::point(character_pos(d->cursor_position));
    if ( cursor.x+1 > target.getSize().x )
        states.transform.translate(target.getSize().x-cursor.x-1,0);
    Shaded_Text::draw(target,states);
    if ( d->active )
    {
        sf::Vertex line[2];
        line[0] = line[1] = cursor;
        line[1].position.y += size();
        for ( auto& l : line )
        {
            l.color = shadow_color();
            l.position.x += shadow_offset();
            l.position.y += shadow_offset();
        }
        target.draw(line,2,sf::Lines,states);
        for ( auto& l : line )
        {
            l.color = color();
            l.position.x -= shadow_offset();
            l.position.y -= shadow_offset();
        }
        target.draw(line,2,sf::Lines,states);
    }
}
