/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef UI_FRAME_HPP
#define UI_FRAME_HPP

#include <SFML/Graphics/RenderTarget.hpp>
#include "misc/mirror.hpp"
#include "media/tileset.hpp"
#include "misc/boilerplate.hpp"

namespace media{
namespace ui {

/**
 * \brief Draws a box surrounded by the given tiles
 */
class Frame : public misc::Mirror
{
    PROPERTY_MAGIC(Frame)

private:
    class Frame_Private *d;

public:
    Frame();
    DECLARE_STRUCTORS(Frame)

    /**
     * \brief Get the tileset from which to get the tiles
     */
    media::Tileset *tileset() const;
    /**
     * \brief Set the tileset from which to get the tiles
     */
    void set_tileset ( media::Tileset* tileset );

    /**
     * \brief Draw the frame covering the given area
     * \return The area covered by the frame
     * (differs from \c area if the tile sizes don't add up perfectly to it)
     */
    geo::Rectangle draw(const geo::Rectangle&area, sf::RenderTarget& target, sf::RenderStates states) const;

//{
/// \name Tiles
/// \{
    /**
     * \brief Set top left corner
     * \param n     Tile id
     */
    void set_top_left(long n);
    /**
     * \brief Get top left tile id
     */
    long top_left() const;
    /**
     * \brief Set left edge
     * \param n     Tile id
     */
    void set_left(long n);
    /**
     * \brief Get left tile id
     */
    long left() const;
    /**
     * \brief Set top edge
     * \param n     Tile id
     */
    void set_top(long n);
    /**
     * \brief Get top tile id
     */
    long top() const;
    /**
     * \brief Set bottom right corner
     * \param n     Tile id
     */
    void set_bottom_right(long n);
    /**
     * \brief Get bottom right tile id
     */
    long bottom_right() const;
    /**
     * \brief Set bottom edge
     * \param n     Tile id
     */
    void set_bottom(long n);
    /**
     * \brief Get bottom tile id
     */
    long bottom() const;
    /**
     * \brief Set right edge
     * \param n     Tile id
     */
    void set_right(long n);
    /**
     * \brief Get right tile id
     */
    long right() const;
    /**
     * \brief Set bottom left corner
     * \param n     Tile id
     */
    void set_bottom_left(long n);
    /**
     * \brief Get bottom left tile id
     */
    long bottom_left() const;
    /**
     * \brief Set top right corner
     * \param n     Tile id
     */
    void set_top_right(long n);
    /**
     * \brief Get top right tile id
     */
    long top_right() const;
    /**
     * \brief Set center
     * \param n     Tile id
     */
    void set_center(long n);
    /**
     * \brief Get center tile id
     */
    long center() const;
/// \}
//}

};


}} // namespace media::ui
#endif // UI_FRAME_HPP
