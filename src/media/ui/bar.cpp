/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "media/ui/bar.hpp"
#include "media/media_manager.hpp"

#include "misc/geometry_convert.hpp"

using namespace media::ui;

DECLARE_PROPERTY_SET_FUNCTOR(Bar,tileset,[](Bar&f,const misc::Value&v){
    f.set_tileset(media::Media_Manager::instance().tileset((std::string)v));
})
DECLARE_PROPERTY_GET_FUNCTOR(Bar,tileset,[](const Bar&f){
    return f.tileset() ? f.tileset()->id() : "";
})
DECLARE_PROPERTY(Bar,left,left,set_left)
DECLARE_PROPERTY(Bar,right,right,set_right)
DECLARE_PROPERTY(Bar,center,center,set_center)
DECLARE_PROPERTY(Bar,single,single,set_single)

struct media::ui::Bar_Private
{
    media::Tileset* tileset = nullptr;

    long left = -1;
    long right = -1;
    long center = -1;
    long single = -1;

};

Bar::Bar() : d ( new Bar_Private ) {}
PIMPL_STRUCTORS_INH(Bar,misc::Mirror)

geo::Rectangle Bar::draw(const geo::Point& pos, long value, sf::RenderTarget& target, sf::RenderStates states) const
{
    if ( value < 1 )
        return geo::Rectangle();

    sf::Sprite sprite;
    sprite.setPosition(pos.x,pos.y);

    if ( value == 1 )
    {
        d->tileset->get_tile(d->single,sprite);
        target.draw(sprite,states);
        return geo::conv::rect(sprite.getGlobalBounds());
    }

    d->tileset->get_tile(d->left,sprite);
    target.draw(sprite,states);

    d->tileset->get_tile(d->center,sprite);
    float w = sprite.getLocalBounds().width;

    for ( long i = 1; i < value-1; i++ )
    {
        sprite.setPosition(pos.x+w*i,pos.y);
        target.draw(sprite,states);
    }

    sprite.setPosition(pos.x+w*(value-1),pos.y);
    d->tileset->get_tile(d->right,sprite);
    target.draw(sprite,states);

    return geo::Rectangle(pos,geo::conv::rect(sprite.getGlobalBounds()).bottom_right());
}


media::Tileset* Bar::tileset() const
{
    return d->tileset;
}
void Bar::set_tileset ( media::Tileset* tileset )
{
    d->tileset = tileset;
}

void Bar::set_left(long n)
{
    d->left = n;
}
long Bar::left() const
{
    return d->left;
}

void Bar::set_right(long n)
{
    d->right = n;
}
long Bar::right() const
{
    return d->right;
}

void Bar::set_center(long n)
{
    d->center = n;
}
long Bar::center() const
{
    return d->center;
}

void Bar::set_single(long n)
{
    d->single = n;
}
long Bar::single() const
{
    return d->single;
}

