/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef MEDIA_UI_BAR_HPP
#define MEDIA_UI_BAR_HPP

#include "media/tileset.hpp"
#include "misc/mirror.hpp"
#include "misc/boilerplate.hpp"

namespace media {
namespace ui {

/**
 * \brief Bar varying length depending on the given value
 */
class Bar : public misc::Mirror
{
    PROPERTY_MAGIC(Bar)
private:
    class Bar_Private *d;

public:
    Bar();
    DECLARE_STRUCTORS(Bar)

    /**
     * \brief Get the tileset from which to get the tiles
     */
    media::Tileset *tileset() const;
    /**
     * \brief Set the tileset from which to get the tiles
     */
    void set_tileset ( media::Tileset* tileset );

    /**
     * \brief Draw the bar for the given value
     * \return The area covered by the bar
     */
    geo::Rectangle draw(const geo::Point& pos, long value, sf::RenderTarget& target, sf::RenderStates states) const;

    /**
     * \brief Set left edge
     * \param n     Tile id
     */
    void set_left(long n);
    /**
     * \brief Get left tile id
     */
    long left() const;
    /**
     * \brief Set right edge
     * \param n     Tile id
     */
    void set_right(long n);
    /**
     * \brief Get right tile id
     */
    long right() const;
    /**
     * \brief Set center
     * \param n     Tile id
     */
    void set_center(long n);
    /**
     * \brief Get center tile id
     */
    long center() const;
    /**
     * \brief Set single
     * \param n     Tile id
     */
    void set_single(long n);
    /**
     * \brief Get single tile id
     */
    long single() const;


};

} // namespace ui
} // namespace media

#endif // MEDIA_UI_BAR_HPP
