/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "media/tileset.hpp"
#include "media/media_manager.hpp"
#include "config.hpp"
#include "misc/geometry_convert.hpp"

using namespace media;


Tileset::Tileset(const std::string& id)
    : m_id(id)
{}

const std::string& Tileset::id() const
{
    return m_id;
}


sf::Sprite Tileset::get_tile(int index)
{
    sf::Sprite s;
    get_tile(index,s);
    return s;
}

/// \pimpl{Grid_Tileset}
struct media::Grid_Tileset_Private
{
    std::string id;
    sf::Texture* texture;
    int tile_width;
    int tile_height;
    int grid_margin;
    int grid_spacing;

    Grid_Tileset_Private(const std::string& id, const std::string& image, int tile_width, int tile_height, int grid_margin, int grid_spacing)
        : id(id), texture(&Media_Manager::instance().image(image)),
          tile_width(tile_width), tile_height(tile_height),
          grid_margin(grid_margin), grid_spacing(grid_spacing)
  {}

  int tiles_x() { return (texture->getSize().x - grid_margin) /  (tile_width+grid_spacing); }
  int tiles_y() { return (texture->getSize().y - grid_margin) /  (tile_height+grid_spacing); }
};



Grid_Tileset::Grid_Tileset(const std::string& id, const std::string& image, int tile_width, int tile_height, int grid_margin, int grid_spacing)
    : Tileset(id), d ( new Grid_Tileset_Private(id,image,tile_width,tile_height,grid_margin,grid_spacing) )
{}
PIMPL_STRUCTORS_INH(Grid_Tileset,Tileset)

int Grid_Tileset::tile_number() const
{
    if ( ! d->texture )
        return 0;
    return d->tiles_x() * d->tiles_y();
}

void Grid_Tileset::get_tile(int index, sf::Sprite& sprite)
{
    if ( !d->texture )
        return;

    if ( sprite.getTexture() != d->texture )
        sprite.setTexture(*d->texture);

    sprite.setTextureRect(sf::IntRect(
        d->grid_spacing+(index%d->tiles_x())*(d->tile_width+d->grid_spacing),
        d->grid_spacing+(index/d->tiles_x())*(d->tile_height+d->grid_spacing),
        d->tile_width,
        d->tile_height
    ));
}

geo::Size Grid_Tileset::tile_size(int) const
{
    return geo::Size(d->tile_width,d->tile_width);
}


/// \pimpl{Image_Collection}
struct media::Image_Collection_Private
{
    std::vector<sf::Texture*> images;
};


Image_Collection::Image_Collection(const std::string& id)
    : Tileset(id), d ( new Image_Collection_Private() )
{}

PIMPL_STRUCTORS_INH(Image_Collection,Tileset)

void Image_Collection::set_tile(int index, const std::string& image)
{
    if ( index < 0 )
        return;

    if ( unsigned(index) >= d->images.size() )
        d->images.resize(index+1);

    d->images[index] = &Media_Manager::instance().image(image);
}

int Image_Collection::tile_number() const
{
    return d->images.size();
}

void Image_Collection::get_tile(int index, sf::Sprite& sprite)
{
    if ( unsigned(index) < d->images.size() && d->images[index] )
        sprite.setTexture(*d->images[index],true);
}

geo::Size Image_Collection::tile_size(int index) const
{
    if ( index < 0 || unsigned(index) >= d->images.size() )
        return geo::Size(0,0);
    return geo::conv::size(d->images[index]->getSize());
}
