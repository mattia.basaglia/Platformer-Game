/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "game/game_manager.hpp"
#include "config.hpp"
#include "game/user_settings.hpp"

int main(int argc, char* argv[])
{
	for ( int i = 1; i < argc-1; i+=2 )
		game::Game_Manager::instance().settings().set(argv[i],argv[i+1]);

	if ( game::Game_Manager::instance().init() )
		return game::Game_Manager::instance().run();

	return 1;
}
