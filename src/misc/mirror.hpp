/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef OBJECT_WITH_PROPERTIES_HPP
#define OBJECT_WITH_PROPERTIES_HPP

#include <iostream>
#include <map>
#include <functional>
#include <sstream>
#include "misc/game_value.hpp"

namespace misc {

/**
 * \brief Abstract class for classes that need some reflection
 *
 * Inheriting this class and using the proper macros, derived classes can access
 * some members via strings.
 *
 * \code {.cpp}
class A : public Mirror
{
	PROPERTY_MAGIC(A)
	DECLARE_PROPERTY(A,string,get_string,set_string)
	DECLARE_PROPERTY_GETTER(A,int,number)
	DECLARE_PROPERTY_GET_FUNCTOR(A,int2,[](const A& self){return std::to_string(self.number()-789);})
	DECLARE_PROPERTY_ATTRIBUTE(A,foo)

	// Allow some properties to be accessed by other means
	misc::Value access_undeclared_property(const std::string& name) const override { return name == "bar" ? "Bar" : ""; }
	bool has_undeclared_property(const std::string& name) const override { return name == "bar"; }
public:
	std::string get_string() const;
	void set_string(const std::string&) const;
	int number() const;
	double foo;
}; \endcode
 * \note DECLARE_PROPERTY_* macros are best if called outside the class,
 *  in an implementation file inside an unnamed namespace.
 * This will avoid unnecessary overhead during object construction.
 */
class Mirror
{
protected:
    /**
     * \brief Magic class used by macros, do not use
     */
	struct PROPERTY_MAGIC_CLASS
	{
		virtual bool has( const std::string& property) const { return false; }
		virtual bool has_writable( const std::string& property) const { return false; }
		virtual misc::Value get(const Mirror& obj, const std::string& property) const {return misc::Value();}
		virtual void set(Mirror& obj, const std::string& property, const misc::Value& value) const {}
		static PROPERTY_MAGIC_CLASS& instance() { static PROPERTY_MAGIC_CLASS s; return s; }
		virtual void get_all(const Mirror& obj,Properties& out, const std::string& prefix) const {}
        virtual bool has_method(const std::string& name) const { return false; }
        virtual misc::Value call(misc::Mirror& obj,
             const std::string& name,const std::vector<misc::Value>& args) const {return misc::Value();}
	protected:
		PROPERTY_MAGIC_CLASS() {}
		PROPERTY_MAGIC_CLASS(const PROPERTY_MAGIC_CLASS &) = delete;
	};

	/**
	 * \brief Method used internally to get the dynamic magic object
	 */
	virtual const PROPERTY_MAGIC_CLASS& get_magic() const = 0;

	/**
	 * \brief Override if you want some extra properties
	 */
	virtual misc::Value get_undeclared_property(const std::string& name) const { return misc::Value(); }
	/**
	 * \brief Override if you want some extra properties
	 */
	virtual bool has_undeclared_property(const std::string& name) const { return false; }

	/**
	 * \brief Override if you want some extra writable properties
	 */
	virtual void set_undeclared_property(const std::string& name, const misc::Value& value) { }
	/**
	 * \brief Override if you want some extra writable properties
	 */
	virtual bool has_writable_undeclared_property(const std::string& name) const { return false; }
	/**
	 * \brief Override if you want some extra readable properties
	 * \param[out] out  Output property map
	 * \param   prefix  Prefix to prepend to property names
	 */
	virtual void get_all_undeclared_properties(Properties& out, const std::string& prefix) const { }
    /**
     * \brief Override if you want some extra callable methods
     */
    virtual misc::Value call_undeclared_method(const std::string&name,const std::vector<misc::Value>&args){return misc::Value();}
    /**
     * \brief Override if you want some extra callable methods
     */
    virtual bool has_undeclared_method(const std::string&name) const { return false; }
public:
    /**
     * \brief Get property by name
     * \param name Name of the property
     * \return The property value or an empty Value if there's no property with that given name
     */
    misc::Value get_property(const std::string& name) const
	{
		return get_magic().has(name) ? get_magic().get(*this,name) : get_undeclared_property(name);
	}

	/**
	 * \brief Set property by name
	 * \param name Name of the property
	 * \param value Value
	 */
	void set_property(const std::string&name, const misc::Value& value)
	{
		return get_magic().has_writable(name) ? get_magic().set(*this,name,value) : set_undeclared_property(name,value);
	}

    /**
     * \brief Whether a property exists
     * \param name Name of the property
     */
	bool has_property(const std::string& name) const
	{
		return get_magic().has(name) || has_undeclared_property(name);
	}


	/**
	 * \brief Whether a writable property exists
     * \param name Name of the property
	 */
	bool has_writable_property(const std::string& name) const
	{
		return get_magic().has_writable(name) || has_writable_undeclared_property(name);
	}

    /**
     * \brief Get all properties as a map
     * \param prefix Prefix top prepend to property names
     */
	Properties get_properties(const std::string& prefix="") const
	{
	    Properties prop;
	    get_properties(prop,prefix);
	    return prop;
	}

    /**
     * \brief Get all properties as a map
     * \param[out] out Property container to be filled
     * \param   prefix Prefix top prepend to property names
     */
	void get_properties(misc::Properties& out, const std::string& prefix="") const
	{
	    get_all_undeclared_properties(out,prefix);
	    get_magic().get_all(*this,out,prefix);
	}

	/**
	 * \brief Set all properties at once
	 */
	virtual void set_properties(const Properties& props)
	{
	    for ( auto p : props )
            set_property(p.first,p.second);
	}


	/**
	 * \brief Whether a callable method exists
	 * \param name Name of the method
	 */
	bool has_method(const std::string& name) const
	{
	    return get_magic().has_method(name) || has_undeclared_method(name);
	}

    /**
     * \brief Call a method by name
     * \param name  Method name
     * \param args  Arguments
     * \return What the method returned
     */
	misc::Value call_method(const std::string& name, const std::vector<misc::Value>& args)
	{
	    return get_magic().has_method(name) ? get_magic().call(*this,name,args) : call_undeclared_method(name,args);
	}


};

/**
 * \brief Enable property magic for classes that don't inherit Mirror directly
 * \param c     Class
 * \param base  Base class, must be derived from Mirror
 */
#define PROPERTY_MAGIC_INHERIT(c,base) \
	public: struct PROPERTY_MAGIC_CLASS : base::PROPERTY_MAGIC_CLASS { \
	std::map<std::string,std::function<misc::Value (const misc::Mirror&)>> getters; \
	std::map<std::string,std::function<void (misc::Mirror&, const misc::Value&)>> setters; \
	std::map<std::string,std::function<misc::Value (misc::Mirror&, const std::vector<misc::Value>&)>> methods; \
	misc::Value get(const misc::Mirror& obj, const std::string& property) const override { \
		auto it = getters.find(property); \
		return it != getters.end() ? it->second(obj) : base::PROPERTY_MAGIC_CLASS::instance().get(obj,property); } \
	bool has(const std::string& property) const override { \
		return getters.find(property) != getters.end() || base::PROPERTY_MAGIC_CLASS::instance().has(property); } \
	virtual void set(misc::Mirror& obj, const std::string& property, const misc::Value& value) const override {\
		auto it = setters.find(property); \
		return it != setters.end() ? it->second(obj,value) : base::PROPERTY_MAGIC_CLASS::instance().set(obj,property,value);  }\
	bool has_writable(const std::string& property) const override { \
		return setters.find(property) != setters.end() || base::PROPERTY_MAGIC_CLASS::instance().has_writable(property); } \
    void get_all(const misc::Mirror& obj,misc::Properties& out, const std::string& prefix) const override { \
        base::PROPERTY_MAGIC_CLASS::instance().get_all(obj,out,prefix); \
        for ( const auto&p : getters ) \
            out[prefix+p.first] = p.second(obj); } \
    bool has_method(const std::string& name) const override { \
        return methods.find(name) != methods.end() || base::PROPERTY_MAGIC_CLASS::instance().has_method(name); } \
    misc::Value call(misc::Mirror& obj, const std::string& name,const std::vector<misc::Value>& args) const override {\
        auto it = methods.find(name); \
		return it != methods.end() ? it->second(obj,args) : base::PROPERTY_MAGIC_CLASS::instance().call(obj,name,args);  }\
	static PROPERTY_MAGIC_CLASS& instance() { static PROPERTY_MAGIC_CLASS s; return s; } \
	template<class T> static typename std::remove_cv<typename std::remove_reference<T>::type>::type \
        setter_argument(const misc::Value& value, void(c::*)(T)) \
		{ return value.to<typename std::remove_cv<typename std::remove_reference<T>::type>::type>(); } \
	template<class T> static T setter_value(const misc::Value& value, const T&) \
		{ return value.to<T>(); } \
	}; \
	const misc::Mirror::PROPERTY_MAGIC_CLASS& get_magic() const override { \
		return PROPERTY_MAGIC_CLASS::instance(); } \
	static PROPERTY_MAGIC_CLASS& get_static_magic() { return PROPERTY_MAGIC_CLASS::instance(); } \
	private:

/**
 * \brief Enable property magic
 * \param c Class
 */
#define PROPERTY_MAGIC(c) PROPERTY_MAGIC_INHERIT(c,misc::Mirror)

/**
 * \brief Declare a property read using a functor
 * \param c         Class
 * \param name      Property name, as accessed via strings
 * \param symbol    Property name as a C++ symbol
 * \param functor   Functor returning a Value
 */
#define DECLARE_PROPERTY_GET_FUNCTOR_EX(c,name,symbol,functor) \
	struct PROPERTY_MAGIC_CLASS_GET_##c##_##symbol { \
		PROPERTY_MAGIC_CLASS_GET_##c##_##symbol() { \
		c::get_static_magic().getters[name] = [](const misc::Mirror& obj){ \
			return ((functor)((c&)obj)); }; }\
	} PROPERTY_MAGIC_OBJECT_GET_##c##_##symbol;


/**
 * \brief Declare a property read using a functor
 * \param c         Class
 * \param name      Property name as a C++ symbol
 * \param functor   Functor returning a Value
 */
#define DECLARE_PROPERTY_GET_FUNCTOR(c,name,functor) \
	DECLARE_PROPERTY_GET_FUNCTOR_EX(c,#name,name,functor)


/**
 * \brief Declare a property read using a getter method
 * \param c         Class
 * \param name      Property name as a C++ symbol
 * \param method    Name of the method used to access the property
 * \note \c c::method must return a type which overload the stream operator <<
 */
#define DECLARE_PROPERTY_GETTER(c,name,method) DECLARE_PROPERTY_GET_FUNCTOR(c,name, \
	[](const c& obj){ return misc::Value::from(obj.method()); } )


/**
 * \brief Declare a property written using a functor
 * \param c         Class
 * \param name      Property name, as accessed via strings
 * \param symbol    Property name as a C++ symbol
 * \param functor   Functor taking a reveference to object and a misc::Value
 */
#define DECLARE_PROPERTY_SET_FUNCTOR_EX(c,name,symbol,functor) \
	struct PROPERTY_MAGIC_CLASS_SET_##c##_##symbol { \
		PROPERTY_MAGIC_CLASS_SET_##c##_##symbol() { \
		c::get_static_magic().setters[name] = [](misc::Mirror& obj, const misc::Value& value){ \
			return ((functor)((c&)obj,value)); }; }\
	} PROPERTY_MAGIC_OBJECT_SET_##c##_##symbol;

/**
 * \brief Declare a property written using a functor
 * \param c         Class
 * \param name      Property name as a C++ symbol
 * \param functor   Functor returning a misc::Value
 */
#define DECLARE_PROPERTY_SET_FUNCTOR(c,name,functor) \
	DECLARE_PROPERTY_SET_FUNCTOR_EX(c,#name,name,functor)

/**
 * \brief Declare a property written using a setter method
 * \param c         Class
 * \param name      Property name as a C++ symbol
 * \param method    Name of the method used to access the property
 * \note \c c::method must take a type which overload the stream operator >> and return void
 */
#define DECLARE_PROPERTY_SETTER(c,name,method) DECLARE_PROPERTY_SET_FUNCTOR(c,name, \
	[](c& obj, const misc::Value& value){ obj.method(c::PROPERTY_MAGIC_CLASS::setter_argument(value,&c::method));} )

/**
 * \brief Declare a property read and written using accessor methods
 * \param c			Class
 * \param name		Property name as a C++ symbol
 * \param getter	Getter method
 * \param setter	Setter method
 */
#define DECLARE_PROPERTY(c,name,getter,setter) DECLARE_PROPERTY_GETTER(c,name,getter) DECLARE_PROPERTY_SETTER(c,name,setter)


/**
 * \brief Declare a property read and written using accessor methods
 * \param c			Class
 * \param string    String used to access the property
 * \param id		Property name as a C++ identifier
 * \param getter	Getter method
 * \param setter	Setter method
 */
#define DECLARE_PROPERTY_RENAME(c,string,id,getter,setter) \
    DECLARE_PROPERTY_SET_FUNCTOR_EX(c,string,id, \
        [](c& obj, const misc::Value& value){ obj.setter(c::PROPERTY_MAGIC_CLASS::setter_argument(value,&c::setter));} ) \
	DECLARE_PROPERTY_GET_FUNCTOR_EX(c,string,id, \
        [](const c& obj){ return misc::Value::from(obj.getter()); } )


/**
 * \brief Declare a property accessible directly with an attribute
 * \param c     Class
 * \param name  property/attribute name
 * \note \c c::name must be of a type which overload the stream operator <<
 */
#define DECLARE_PROPERTY_ATTRIBUTE_READONLY(c,name) DECLARE_PROPERTY_GET_FUNCTOR(c,name, \
	[](const c& obj){ return misc::Value::from(obj.name); } )
/**
 * \brief Declare a property accessible directly with an attribute
 * \param c     Class
 * \param name  property/attribute name
 * \note \c c::name must be of a type which overload the stream operator << and >>
 */
#define DECLARE_PROPERTY_ATTRIBUTE(c,name)  \
	DECLARE_PROPERTY_ATTRIBUTE_READONLY(c,name) \
	DECLARE_PROPERTY_SET_FUNCTOR(c,name,[](c&obj,const misc::Value&value) { \
		obj.name = c::PROPERTY_MAGIC_CLASS::setter_value(value,obj.name); })
/**
 * \brief Declare a property accessible directly with an attribute
 * \param c     Class
 * \param name  property name
 * \param attr  attribute
 * \note \c c::attr must be of a type which overload the stream operator << and >>
 */
#define DECLARE_PROPERTY_ATTRIBUTE_RENAME(c,name,attr)  \
	DECLARE_PROPERTY_GET_FUNCTOR(c,name, \
        [](const c& obj){ return misc::Value::from(obj.attr); } ) \
	DECLARE_PROPERTY_SET_FUNCTOR(c,name,[](c&obj,const misc::Value&value) { \
		obj.attr = c::PROPERTY_MAGIC_CLASS::setter_value(value,obj.attr); })

/**
 * \brief Access a property as an attribute using a completely arbitrary name
 * \param c             Class
 * \param string        String used to access the property
 * \param attr          Attribute used to set and get the value
 * \param identifier    A valid C++ identifier to represent the attribute
 */
#define DECLARE_PROPERTY_ATTRIBUTE_RENAME_EX(c,string,attr,identifier) \
    DECLARE_PROPERTY_GET_FUNCTOR_EX(c,string,identifier, \
        [](const c& obj){ return misc::Value::from(obj.attr); } ) \
    DECLARE_PROPERTY_SET_FUNCTOR_EX(c,string,identifier, \
        [](c& obj, const misc::Value& value){ \
            obj.attr = c::PROPERTY_MAGIC_CLASS::setter_value(value,obj.attr); })


/**
 * \brief Define a method
 * \param c         Class
 * \param name      Method name as string
 * \param symbol    Method name as C++ identifier
 * \param functor   Functor (misc::Mirror& obj, const std::vector<misc::Value>& args) -> misc::Value
 */
#define DECLARE_METHOD_FUNCTOR_EX(c,name,symbol,functor) \
	struct PROPERTY_MAGIC_CLASS_CALL_##c##_##symbol { \
		PROPERTY_MAGIC_CLASS_CALL_##c##_##symbol() { \
		c::get_static_magic().methods[name] = [](misc::Mirror& obj, const std::vector<misc::Value>& args){ \
			return ((functor)((c&)obj,args)); }; }\
	} PROPERTY_MAGIC_OBJECT_CALL_##c##_##symbol;

/**
 * \brief Define a method
 * \param c       Class
 * \param name    Method name as C++ identifier
 * \param functor Functor
 */
#define DECLARE_METHOD_FUNCTOR(c,name,functor) \
    DECLARE_METHOD_FUNCTOR_EX(c,#name,name,functor)

/**
 * \brief Define a method
 * \param c       Class
 * \param method  Method name (must take misc::Properties and not return void)
 */
#define DECLARE_METHOD(c,method) \
    DECLARE_METHOD_FUNCTOR(c,method,[](c& obj, const std::vector<misc::Value>s& args) \
        { return misc::Value::from(obj.method(args)); } )
/**
 * \brief Define a method which will have its return value discarded
 * \param c       Class
 * \param method  Method name (must take std::vector<misc::Value> return void)
 */
#define DECLARE_METHOD_VOID(c,method) \
    DECLARE_METHOD_FUNCTOR(c,method,[](c& obj, const std::vector<misc::Value>& args) \
        { obj.method(args); return misc::Value(); } )
/**
 * \brief Define a method which takes no arguments and will have its return value discarded
 * \param c       Class
 * \param method  Method name
 */
#define DECLARE_METHOD_VOID_NOARG(c,method) \
    DECLARE_METHOD_FUNCTOR(c,method,[](c& obj, const std::vector<misc::Value>&) \
        { obj.method(); return misc::Value(); } )

} // namespace misc

#endif // OBJECT_WITH_PROPERTIES_HPP
