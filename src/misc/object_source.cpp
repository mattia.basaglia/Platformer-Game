/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "misc/object_source.hpp"

using namespace misc;


misc::Mirror* Object_Source::find_child_weak(const std::string& name, std::string& tail) const
{
    std::string root;
    misc::split_identifier(name,root,tail);
    Mirror* o = get_child(root);
    if ( o )
        return o;
    else
    {
        std::string container = root;
        while ( is_container(container) )
        {
            misc::split_identifier(tail,root,tail);
            container += '.'+root;
            o = get_child(container);
            if ( o )
                return o;
        }
    }

    return nullptr;
}


misc::Mirror* Object_Source::find_child(const std::string& name) const
{
    Mirror* o = get_child(name);
    if ( o )
        return o;
    else
    {
        std::string root, item;
        misc::split_identifier(name,root,item);
        std::string container = root;
        while ( is_container(container) )
        {
            misc::split_identifier(item,root,item);
            container += '.'+root;
            o = get_child(container);
            if ( o && item.empty() )
                return o;
        }
    }

    return nullptr;
}


misc::Value Object_Source::get_undeclared_property(const std::string& name) const
{
    std::string property;
    if ( Mirror* o = find_child_weak(name,property) )
        return o->get_property(property);

    return misc::Value();
}
bool Object_Source::has_undeclared_property(const std::string& name) const
{
    std::string property;
    if ( Mirror* o = find_child_weak(name,property) )
        return o->has_property(property);

    return false;
}
void Object_Source::set_undeclared_property(const std::string& name,const misc::Value& value)
{
    std::string property;
    if ( Mirror* o = find_child_weak(name,property) )
        return o->set_property(property,value);

}

bool Object_Source::has_writable_undeclared_property(const std::string& name) const
{
    std::string property;
    if ( Mirror* o = find_child_weak(name,property) )
        return o->has_writable_property(property);
    return false;
}

misc::Value Object_Source::call_undeclared_method(const std::string&name,const std::vector<misc::Value>&args)
{
    std::string property;
    if ( Mirror* o = find_child_weak(name,property) )
        return o->call_method(property,args);
    return misc::Value();
}
bool Object_Source::has_undeclared_method(const std::string&name) const
{
    std::string property;
    if ( Mirror* o = find_child_weak(name,property) )
        return o->has_method(property);
    return false;
}

void Object_Source::get_all_undeclared_properties(Properties& out, const std::string& prefix) const
{
    for_each_object([&out,&prefix](const std::string& name, misc::Mirror* object)
    {
        object->get_properties(out,prefix+name+'.');
    });
}
