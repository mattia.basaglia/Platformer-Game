#ifndef BOILERPLATE_HPP
#define BOILERPLATE_HPP

/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
 * \brief Define simple PIMPL copy, move and destruction operations
 * \note Don't use if the \c d pointer contains dynamic pointers
 * \param Class Class name, the private class will be Class_Private
 */
#define PIMPL_STRUCTORS(Class) \
    Class::Class(const Class& o) : d ( new Class##_Private(*o.d) ) {} \
    Class::Class(Class&& o) : d ( o.d ) { o.d = nullptr; } \
    Class& Class::operator=(const Class& o) { *d = *o.d; return *this; } \
    Class& Class::operator=(Class&& o) { std::swap(d,o.d); return *this; } \
    Class::~Class() { delete d; }

/**
 * \brief Define simple PIMPL copy, move and destruction operations for derived classes
 * \note Don't use if the \c d pointer contains dynamic pointers
 * \param Class Class name, the private class will be Class_Private
 * \param Parent Parent class name
 */
#define PIMPL_STRUCTORS_INH(Class,Parent) \
    Class::Class(const Class& o) : Parent(o), d ( new Class##_Private(*o.d) ) {} \
    Class::Class(Class&& o) : Parent(o), d ( o.d ) { o.d = nullptr; } \
    Class& Class::operator=(const Class& o) { Parent::operator=(o); *d = *o.d; return *this; } \
    Class& Class::operator=(Class&& o) { Parent::operator=(o); std::swap(d,o.d); return *this; } \
    Class::~Class() { delete d; }

/**
 * \brief Add declarations for copy, move and (virtual) destructor for the given class
 * \param Class Class name
 */
#define DECLARE_STRUCTORS(Class) \
    Class(const Class& o); \
    Class(Class&& o); \
    Class& operator=(const Class& o); \
    Class& operator=(Class&& o); \
    virtual ~Class();

#endif // BOILERPLATE_HPP
