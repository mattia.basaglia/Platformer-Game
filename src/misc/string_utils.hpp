/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef STRING_UTILS_HPP
#define STRING_UTILS_HPP

#include <sstream>
#include <vector>
#include <iterator>
#include <algorithm>


namespace misc {

/**
 * \brief String to number
 * \param s     String to be parsed
 * \param value You can use it to deduce the template type, its value has no effect
 *
 * Unlike std::stoi, it doesn't throw exceptions
 */
template<class T>
    T ston ( const std::string& s, T value = T() )
    {
        std::istringstream(s) >> value;
        return value;
    }

/**
 * \brief Check if a strings starts with a given prefix
 * \param string    String to check
 * \param prefix    Prefix
 */
inline bool starts_with(const std::string& string, const std::string& prefix )
{
    return string.size() >= prefix.size() && string.compare(0,prefix.size(),prefix) == 0;
}

/**
 * \brief Get a copy of the string without the given prefix
 */
inline std::string remove_prefix(const std::string& string, const std::string& prefix )
{
    return starts_with(string,prefix) ? string.substr(prefix.size()) : string;
}


template<class Container>
    bool contains(const Container& container, const typename Container::value_type& value )
    {
        return std::find(container.begin(), container.end(), value) != container.end();
    }


/**
 * \brief Hex string to number
 * \param s     String to be parsed
 * \param value You can use it to deduce the template type, its value has no effect
 *
 * Unlike std::stoi, it doesn't throw exceptions
 */
template<class T>
    T xston ( const std::string& s, T value = T() )
    {
        std::istringstream ss(s);
        ss.setf(std::ios_base::hex,std::ios_base::basefield);
        ss >> value;
        return value;
    }


/**
 * \brief Split a string in root . item
 * \param[in] identifier    String to be split
 * \param[out] root         The part of \c identifier up to the first dot
 * \param[out] item         The part of \c identifier after the first dot
 * \pre \code &identifier != & root \endcode
 */
inline std::string::size_type split_identifier(const std::string& identifier, std::string& root, std::string& item)
{
    std::string::size_type dot = identifier.find('.');
    root = identifier.substr(0,dot);
    if ( dot != std::string::npos )
        item = identifier.substr(dot+1);
    else
        item.clear();
    return dot;
}

/**
 * \brief Turn a container into a string
 * \tparam Iterator     An iterator, Container::value_type must overload the stream operator<<
 * \param glue          String separating the items
 * \param begin         Pointer to the first element
 * \param end           Pointer to past the last element
 */
template <class Iterator>
    std::string implode(const std::string& glue, Iterator begin, Iterator end)
    {
        std::ostringstream ss;
        if ( begin != end )
        {
            ss << *begin;
            for(++begin; begin != end; ++begin)
                ss << glue << *begin;
        }
        return ss.str();
    }
/**
 * \brief Turn a container into a string
 * \tparam Container    A container, Container::value_type must overload the stream operator<<
 * \param glue          String separating the items
 * \param container     Container to implode
 */
template <class Container>
    std::string implode(const std::string& glue, const Container& container)
    {
        return implode(glue,container.begin(),container.end());
    }

/**
 * \brief Turn a string into a vector
 * \tparam Element      Vector element type (constructed with strings)
 * \param glue          String separating the items
 * \param string        String to explode
 */
template <class Element>
    std::vector<Element> explode(const std::string& glue, const std::string& string)
    {
        std::vector<Element> out;
        if ( glue.empty() )
            return out;

        std::string::size_type first = 0, next = 0;
        while ( (next = string.find(glue,next)) != std::string::npos )
        {
            out.push_back(Element(string.substr(first,next-first)));
            next += glue.size();
            first = next;
        }
        out.push_back(Element(string.substr(first)));
        return out;
    }


} // namespace misc


/**
 * \brief Output a vector separated by commas
 * \note \c T needs to have an overloaded << stream operator
 */
template<class T, class CharT, class Traits>
    std::basic_ostream<CharT,Traits>& operator<< (
        std::basic_ostream<CharT,Traits>& os,
        const std::vector<T>& vector )
{
    if ( !vector.empty() )
    {
        std::copy(vector.begin(), vector.end()-1,
              std::ostream_iterator<T>(os, ", "));

        os << vector.back();
    }

    return os;
}

#endif // STRING_UTILS_HPP
