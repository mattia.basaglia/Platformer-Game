/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef MISC_OBJECT_SOURCE_HPP
#define MISC_OBJECT_SOURCE_HPP

#include "misc/mirror.hpp"

namespace misc {

/**
 * \brief An object containing other objects
 */
class Object_Source : public Mirror
{
protected:
	misc::Value get_undeclared_property(const std::string& name) const override;
	virtual bool has_undeclared_property(const std::string& name) const override;
	virtual void set_undeclared_property(const std::string& name, const misc::Value& value) override;
	virtual bool has_writable_undeclared_property(const std::string& name) const override;
	virtual void get_all_undeclared_properties(Properties& out, const std::string& prefix) const  override;
    virtual misc::Value call_undeclared_method(const std::string&name,const std::vector<misc::Value>&args) override;
    virtual bool has_undeclared_method(const std::string&name) const  override;

    /**
     * \brief Apply a function to all objects, pass name and pointer
     */
    virtual void for_each_object( const std::function<void(const std::string& name,misc::Mirror*)>& func ) const = 0;

    /**
     * \brief Check if there are objects nested in this.name
     */
    virtual bool is_container(const std::string& name) const { return false; }

    /**
     * \brief Get object by name (implementation)
     */
    virtual misc::Mirror* get_child(const std::string& name) const = 0;


public:
    /**
     * \brief Find child object by name
     * \param name      Object name
     * \param strict    Whether the full name should be matched
     */
    misc::Mirror* find_child(const std::string& name) const;

    /**
     * \brief Find child object by name
     * \param[in]  name  Name to search
     * \param[out] tail  The part of the unmatched object name
     */
    misc::Mirror* find_child_weak(const std::string& name, std::string& tail) const;
};



} // namespace misc
#endif // MISC_OBJECT_SOURCE_HPP
