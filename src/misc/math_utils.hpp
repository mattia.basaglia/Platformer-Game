/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include <cmath>


#ifndef M_PI
#include #include <boost/math/constants/constants.hpp>
#endif

/**
 * \brief Various mathematical functions
 */
namespace math {

#ifdef M_PI
/**
 * \brief tau/2
 */
constexpr float pi = M_PI;
#else
/**
 * \brief tau/2
 */
constexpr float pi = boost::math::constants::pi<double>();
#endif
constexpr float tau = 2*pi;

constexpr float deg2rad(float deg) { return deg/180*pi; }
constexpr float rad2deg(float rad) { return rad/pi*180; }

/**
 * \brief Compare two floats for equality
 * \param a     First value
 * \param b     Second value
 * \param error Maximum accepted relative error
 * \return Whether the two values are equal or close to being equal
 */
inline constexpr bool float_equals ( float a, float b, float error = 0.001 )
{
    return a == b || ( a < error && b < error ? float_equals(a+1,b+1,error) :
                       std::abs(a-b)/std::max(std::abs(a),std::abs(b)) );
}


/**
 * \brief Checks if a value is zero
 * \param v     Value
 * \param error Maximum accepted absolute error
 */
inline constexpr bool float_zero ( float v, float error = 0.001 )
{
    return std::abs(v) < error;
}

/**
 * \brief Get a random number
 */
long random();
/**
 * \brief Get a random number between 0 and max
 */
long random(long max);
/**
 * \brief Get a random number in [min,max]
 */
long random(long min, long max);

} // namespace misc
