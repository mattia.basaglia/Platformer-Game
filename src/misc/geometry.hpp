/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef GEOMETRY_HPP
#define GEOMETRY_HPP


#include <cmath>
#include <algorithm>

/**
 * \brief Namespace for geometric objects and operations
 */
namespace geo {

/**
 * \brief Templated class
 */
namespace generic {

/**
 * \brief A point in a 2D space
 */
template <class T> struct Point
{
    T x = 0; ///< X coordinate
    T y = 0; ///< Y coordinate

    constexpr Point( T x, T y) : x(x), y(y) {}
    constexpr Point() {}

    Point& operator+= ( const Point& p )
    {
        x += p.x;
        y += p.y;
        return *this;
    }
    Point& operator-= ( const Point& p )
    {
        x -= p.x;
        y -= p.y;
        return *this;
    }
    Point& operator*= ( T factor )
    {
        x *= factor;
        y *= factor;
        return *this;
    }
    Point& operator/= ( T factor )
    {
        x /= factor;
        y /= factor;
        return *this;
    }
    constexpr Point operator-() const
    {
        return Point(-x,-y);
    }
    constexpr Point operator+(const Point&p) const
    {
        return Point(p.x+x,p.y+y);
    }
    constexpr Point operator-(const Point&p) const
    {
        return Point(x-p.x,y-p.y);
    }
    constexpr Point operator*(T factor) const
    {
        return Point(x*factor,y*factor);
    }
    friend constexpr Point operator*(T factor, const Point&p)
    {
        return Point(p.x*factor,p.y*factor);
    }
    constexpr Point operator/(T factor) const
    {
        return Point(x/factor,y/factor);
    }

    constexpr bool operator== ( const Point& p ) const
    {
        return x == p.x && y == p.y;
    }

    constexpr bool operator!= ( const Point& p ) const
    {
        return x != p.x || y != p.y;
    }

    /**
     * \brief Distance from the origin
     */
    constexpr auto magnitude() const -> decltype(std::sqrt(x*x+y*y))
    {
        return std::sqrt(x*x+y*y);
    }

    /**
     * \brief Distance to another Point
     */
    constexpr T distance_to(const Point& o) const
    {
        return (*this-o).magnitude();
    }
};

/**
 * \brief A size ( width/height )
 */
template <class T> struct Size
{
    T width = 0;
    T height= 0;

    constexpr Size ( T width, T height ) : width(width), height(height) {}
    constexpr Size(){}
};

/**
 * \brief A rectangle defined by its position (top left corner) and size
 */
template<class T> struct Rectangle
{
    T x = 0;
    T y = 0;
    T width = 0; ///< Width \note If not positive, renders the rectangle invalid
    T height = 0;///< Height \note If not positive, renders the rectangle invalid

    /**
     * \brief Construct an empty rectangle
     */
    constexpr Rectangle() {}

    constexpr Rectangle(T x, T y, T width, T height )
        : x(x),y(y),width(width),height(height) {}

    constexpr Rectangle( const Point<T>& pos, const Size<T>& size )
        : x(pos.x), y(pos.y), width(size.width), height(size.height) {}

    constexpr Rectangle( const Point<T>& top_left, const Point<T>& bottom_right )
        : x(top_left.x), y(top_left.y), width(bottom_right.x-top_left.x), height(bottom_right.y-top_left.y) {}

    constexpr T top() const { return y; }
    constexpr T left() const { return x; }
    constexpr T right() const { return x+width; }
    constexpr T bottom() const { return y+height; }

    constexpr Point<T> top_left() const { return Point<T>(left(),top()); }
    constexpr Point<T> bottom_right() const { return Point<T>(right(),bottom()); }
    constexpr Point<T> top_right() const { return Point<T>(right(),top()); }
    constexpr Point<T> bottom_left() const { return Point<T>(left(),bottom()); }
    constexpr Point<T> center() const { return Point<T>(x+width/2,y+height/2); }

    constexpr T area() const { return width*height; }
    constexpr Size<T> size() const { return Size<T>(width,height); }

    /**
     * \brief Get whether a rectangle contains the given point
     */
    constexpr bool contains(const Point<T>& p) const
    {
        return p.x >= x && p.x <= x+width && p.y >= y && p.y <= y+height;
    }
    /**
     * \brief Get whether a rectangle contains the given point
     */
    constexpr bool contains(T x, T y) const
    {
        return contains(Point<T>(x,y));
    }

    /**
     * \brief Whether the intersection between this and \c rect is not empty
     */
    constexpr bool intersects(const Rectangle& rect) const
    {
        return x < rect.right() && right() > rect.x && y < rect.bottom() && bottom() > rect.y;
    }

    /**
     * \brief Move the rectangle by the given offset
     */
    void translate(const Point<T>& offset)
    {
        x += offset.x;
        y += offset.y;
    }
    /**
     * \brief Move the rectangle by the given offset
     */
    void translate(T dx, T dy)
    {
        translate(Point<T>(dx,dy));
    }

    /**
     * \brief Get a rectangle moved by the given amount
     */
    constexpr Rectangle translated(const Point<T>& offset) const
    {
        return Rectangle(top_left()+offset,size());
    }
    /**
     * \brief Get a rectangle moved by the given amount
     */
    constexpr Rectangle translated(T dx, T dy) const
    {
        return translated(Point<T>(dx,dy));
    }

    /**
     * \brief Get the rectangle corresponding to the overlapping area between the two rectangles
     * \return The overlapping area or an invalid rectangle
     */
    constexpr Rectangle intersection(const Rectangle&rect) const
    {
        return ( !is_valid() || ! rect.is_valid() ) ? Rectangle() :
            Rectangle (
                Point<T>(
                    std::max(x,rect.x),
                    std::max(y,rect.y)
                ),
                Point<T>(
                    std::min(x+width,rect.x+rect.width),
                    std::min(y+height,rect.y+rect.height)
                )
            );
    }

    /**
     * \brief Get a rectangle large enough to contain both rectangles
     * \note If any rectangle is invalid, the other one is returned
     */
    constexpr Rectangle united(const Rectangle&rect) const
    {
        // single return for C++11 constexpr :-P
        return  !rect.is_valid() ? *this : ( !is_valid() ? rect :
            Rectangle(
                Point<T>(
                    std::min(x,rect.x),
                    std::min(y,rect.y)
                ),
                Point<T>(
                    std::max(x+width,rect.x+rect.width),
                    std::max(y+height,rect.y+rect.height)
                 )
            ));
    }

    /**
     * \brief Grow this rectangle enough to contain rect
     */
    void unite(const Rectangle&rect)
    {
        *this = united(rect);
    }

    /**
     * \brief Unite
     */
    Rectangle& operator|= ( const Rectangle& rect )
    {
        return *this = united(rect);
    }

    /**
     * \brief Unite
     */
    constexpr Rectangle operator| ( const Rectangle& rect )
    {
        return united(rect);
    }

    /**
     * \brief Intersect
     */
    Rectangle& operator&= ( const Rectangle& rect )
    {
        return *this = intersection(rect);
    }

    /**
     * \brief Intersect
     */
    constexpr Rectangle operator& ( const Rectangle& rect )
    {
        return intersection(rect);
    }

    /**
     * \brief Whether the rectangle is valid (ie both \c width and \c height are positive)
     */
    constexpr bool is_valid() const
    {
        return width > 0 && height > 0;
    }

    constexpr bool operator==(const Rectangle& rect) const
    {
        return x == rect.x && y == rect.y && width == rect.width && height == rect.height;
    }
    constexpr bool operator!=(const Rectangle& rect) const
    {
        return !(*this == rect);
    }

    /**
     * \brief Get the point within the rectangle that's closest to the given point
     */
    constexpr Point<T> nearest(const Point<T>& p) const
    {
        return Point<T>(
            p.x > right()  ? right()  : ( p.x < left()? left(): p.x ),
            p.y > bottom() ? bottom() : ( p.y < top() ? top() : p.y )
        );
    }

    /**
     * \brief Add (or subtract) the given margin from each edge
     */
    void expand(T margin)
    {
        x -= margin;
        y -= margin;
        width += 2*margin;
        height += 2*margin;
    }

    /**
     * \brief Get a rectangle larger than the current one by the given margin
     */
    constexpr Rectangle expanded(T margin) const
    {
        return Rectangle(x-margin,y-margin,width+2*margin,height+2*margin);
    }
};


/**
 * \brief A circle defined by the origin and radius
 */
template<class T> struct Circle
{
    Point<T> origin;
    T radius = 0;

    constexpr Circle() {}
    constexpr Circle(T x, T y, T radius = 0) : origin(x,y),radius(radius){}
    constexpr Circle(const Point<T>& origin, T radius = 0) : origin(origin),radius(radius){}

    /**
     * \brief Whether a point is inside the circle
     */
    constexpr bool contains(const Point<T>& p) const
    {
        return origin.distance_to(p) <= std::abs(radius);
    }

    /**
     * \brief Whether two circles intersect
     */
    constexpr bool intersects(const Circle& o) const
    {
        return origin.distance_to(o.origin) <= std::abs(radius) + std::abs(o.radius);
    }

    /**
     * \brief Whether a rectangle is fully contained within the circle
     */
    constexpr bool contains(const Rectangle<T>& rect) const
    {
        return rect.is_valid() &&
               contains(rect.top_left())    &&
               contains(rect.top_right())   &&
               contains(rect.bottom_right())&&
               contains(rect.bottom_left()) ;
    }

    /**
     * \brief Whether a rectangle intersects the circle
     */
    constexpr bool intersects(const Rectangle<T>& rect) const
    {
        return rect.nearest(origin).distance_to(origin) < std::abs(radius);
    }
};

/**
 * \brief A line segment between two points
 * \todo test
 */
template<class T>
struct Line
{
    typedef decltype(std::atan2(T(),T())) Angle_Type;

    Point<T> p1;
    Point<T> p2;

    constexpr  Line() {}
    constexpr  Line(const Point<T>&p1, const Point<T>&p2) : p1(p1), p2(p2) {}

    constexpr T dx() const { return p2.x-p1.x; }
    constexpr T dy() const { return p2.y-p1.y; }

    constexpr T length() const { return p1.distance_to(p2); }
    constexpr Angle_Type angle() const { return std::atan2(dy(),dx()); }

    void set_angle(Angle_Type angle)
    {
        p2.x = p1.x + std::cos(angle) * length();
        p2.y = p1.y + std::sin(angle) * length();
    }

    void set_length(T length)
    {

        p2.x = p1.x + std::cos(angle()) * length;
        p2.y = p1.y + std::sin(angle()) * length;
    }
};

/**
 * \brief A Point defined in polar coordinates
 * \todo test
 */
template<class T>
struct Polar_Vector
{
    T length= 0;///< Length
    T angle = 0; ///< Angle in radians

    constexpr Polar_Vector() = default;
    constexpr Polar_Vector(T length, T angle) : length(length), angle(angle) {}
    template<class pT>
        constexpr Polar_Vector( Point<pT> point )
            : length(point.magnitude()), angle(std::atan2(-point.y,point.x))
        {}

    /**
     * \brief Get X,Y coordinates
     */
    template<class pT>
        constexpr Point<pT> point() const
    {
        return Point<pT> ( std::cos(angle) * length, -std::sin(angle) * length );
    }

    /**
     * \brief Get X,Y coordinates
     */
    constexpr Point<T> point() const
    {
        return point<T>();
    }

    template<class pT>
        Polar_Vector& operator+= ( const Point<pT>& p )
        {
            return *this = point() + p;
        }
};



} // namespace generic

/// Type used for sizes and coordinates
typedef float Unit_Type;
/// A point in a 2D space
typedef generic::Point<Unit_Type>       Point;
/// A size ( width/height )
typedef generic::Size<Unit_Type>        Size;
/// A rectangle defined by its position (top left corner) and size
typedef generic::Rectangle<Unit_Type>   Rectangle;
/// A circle defined by the origin and radius
typedef generic::Circle<Unit_Type>      Circle;
/// A line segment between two points
typedef generic::Line<Unit_Type>        Line;

typedef Line::Angle_Type Polar_Coordinate;
/// A point in polar coordinates
typedef generic::Polar_Vector<Polar_Coordinate> Polar_Vector;


} // namespace geo

#endif // GEOMETRY_HPP
