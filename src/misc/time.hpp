/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef TIME_HPP
#define TIME_HPP
#include <chrono>
#include <ratio>

namespace misc {

typedef std::chrono::steady_clock clock;
typedef std::chrono::duration<long,std::milli> milliseconds;
typedef milliseconds duration;
typedef std::chrono::time_point<clock,duration> time;

inline time now() { return std::chrono::time_point_cast<duration>(clock::now()); }

} // namespace misc


#endif
