/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef GAME_VALUE_HPP
#define GAME_VALUE_HPP

#include "misc/string_utils.hpp"
#include "misc/time.hpp"
#include <map>

namespace misc{

/**
 * \brief Class that acts as either a string or an integer
 * \invariant If the type is STRING the number is 0,
 *  if the type is NUMBER the string is empty
 */
class Value
{
private:
    enum Type
    {
        NUMBER,
        STRING
    } type;

    long number;
    std::string string; ///< \internal \note adding a string into a union is more trouble than good

    /**
     * \brief Change the internal type
     */
    void cast(Type t)
    {
        if ( t == type )
            return;
        if ( t == STRING )
        {
            string = std::to_string(number);
            number = 0;
        }
        else
        {
            number = misc::ston<long>(string);
            string.clear();
        }
        type = t;
    }

    /**
     * \brief Get an object with the same value as the current one but a different type
     */
    Value casted(Type t) const
    {
        Value v = *this;
        v.cast(t);
        return v;
    }

public:


    /**
     * \brief Construct a value as a number
     */
    Value(long number)
        : type(NUMBER), number(number)
    {}

    Value(int number)
        : type(NUMBER), number(number)
    {}

    /**
     * \brief Construct a value as a string
     */
    Value(const std::string& string = "")
        : type(STRING), number(0), string(string) {}

    Value(const char* string)
        : type(STRING), number(0), string(string) {}

    /**
     * \brief String assignment
     * \note Changes type of the value to STRING
     */
    Value& operator=(const std::string& string)
    {
        type = STRING;
        this->string = string;
        number = 0;
        return *this;
    }

    /**
     * \brief Numeric assignment
     * \note Changes type of the value to NUMBER
     */
    Value& operator=(long number)
    {
        type = NUMBER;
        this->number = number;
        string.clear();
        return *this;
    }

    Value& operator=(void*) = delete;

    /**
     * \brief Initialize Value as a number when possible
     */
    static Value create(const std::string& string)
    {
        long n = 0;
        std::istringstream ss(string);
        if ( ss >> n && ss.eof() )
            return Value(n);
        return Value(string);
    }

    template<class T> static Value from(const T& value)
    {
        std::ostringstream ss;
        ss << value;
        return create(ss.str());
    }

    template<class T> T to() const
    {
        T val;
        std::istringstream((std::string)*this) >> val;
        return val;
    }

    /**
     * \brief Convert to bool
     * \return \b false on zero number or empty string \b true otherwise
     */
    explicit operator bool() const
    {
        return type == NUMBER ? number : !string.empty();
    }

    /**
     * \brief Interpret the value as a string
     */
    explicit operator std::string() const
    {
        return casted(STRING).string;
    }

    /**
     * \brief Interpret the value as a number
     */
    explicit operator long() const
    {
        return casted(NUMBER).number;
    }

    /**
     * \brief Inger sum or string concatenation
     * \note If types mismatch, it'll be casted to string
     */
    Value& operator+= ( const Value&o )
    {
        if ( o.type == type )
        {
            string += o.string;
            number += o.number;
        }
        else
        {
            cast(STRING);
            string += o.casted(STRING).string;
        }
        return *this;
    }

    Value operator+ ( const Value&o ) const
    {
        Value v = *this;
        v += o;
        return v;
    }

    /**
     * \brief Integer subtraction
     */
    Value& operator-= ( const Value&o )
    {
        cast(NUMBER);
        return *this += -o;
    }

    Value operator- ( const Value&o ) const
    {
        Value v = *this;
        v -= o;
        return v;
    }

    /**
     * \brief Integer negation
     */
    Value operator-() const
    {
        return Value(-number);
    }

    /**
     * \brief Integer multiplication
     */
    Value& operator*= ( const Value&o )
    {
        cast(NUMBER);
        number *= o.casted(NUMBER).number;
        return *this;
    }

    Value operator* ( const Value&o ) const
    {
        Value v = *this;
        v *= o;
        return v;
    }

    /**
     * \brief Integer division
     * \note if \c o is 0, the result is 0
     */
    Value& operator/= ( const Value&o )
    {
        cast(NUMBER);
        long n = o.casted(NUMBER).number;
        if ( n == 0 )
            number = 0;
        else
            number /= n;
        return *this;
    }

    Value operator/ ( const Value&o ) const
    {
        Value v = *this;
        v /= o;
        return v;
    }

    /**
     * \brief Remainder of an integer division
     * \note if \c o is 0, the result is 0
     */
    Value& operator%= ( const Value&o )
    {
        cast(NUMBER);
        long n = o.casted(NUMBER).number;
        if ( n == 0 )
            number = 0;
        else
            number %= n;
        return *this;
    }

    Value operator% ( const Value&o ) const
    {
        Value v = *this;
        v %= o;
        return v;
    }

    /**
     * \brief Equality comparison
     * \note if types mismatch returns \b false
     */
    bool operator == ( const Value &o ) const
    {
        if ( type != o.type )
            return false;
        if ( type == STRING )
            return string == o.string;
        return number == o.number;
    }
    /**
     * \brief Inequality comparison
     * \note if types mismatch returns \b true
     */
    bool operator != ( const Value &o ) const
    {
        if ( type != o.type )
            return true;
        if ( type == STRING )
            return string != o.string;
        return number != o.number;
    }
    /**
     * \brief Integer or lexicographic comparison
     * \note if types mismatch it'll compare against a casted value
     */
    bool operator >= ( const Value &o ) const
    {
        if ( type != o.type )
            return *this >= o.casted(type);
        if ( type == STRING )
            return string >= o.string;
        return number >= o.number;
    }
    /**
     * \brief Integer or lexicographic comparison
     * \note if types mismatch it'll compare against a casted value
     */
    bool operator > ( const Value &o ) const
    {
        if ( type != o.type )
            return *this > o.casted(type);
        if ( type == STRING )
            return string > o.string;
        return number > o.number;
    }
    /**
     * \brief Integer or lexicographic comparison
     * \note if types mismatch it'll compare against a casted value
     */
    bool operator <= ( const Value &o ) const
    {
        if ( type != o.type )
            return *this <= o.casted(type);
        if ( type == STRING )
            return string <= o.string;
        return number <= o.number;
    }
    /**
     * \brief Integer or lexicographic comparison
     * \note if types mismatch it'll compare against a casted value
     */
    bool operator < ( const Value &o ) const
    {
        if ( type != o.type )
            return *this < o.casted(type);
        if ( type == STRING )
            return string < o.string;
        return number < o.number;
    }

    friend std::ostream& operator<< ( std::ostream& os, const Value& v )
    {
        if ( v.type == STRING )
            return os << v.string;
        return os << v.number;
    }

    friend std::istream& operator>> ( std::istream& is, Value& v )
    {
        std::string val; is >> val;
        if ( is ) v = Value::create(val);
        return is;
    }

};

template<> inline Value Value::from(const std::string& value)   { return Value(value); }
template<> inline Value Value::from(const long& value)          { return Value(value); }
template<> inline Value Value::from(const misc::Value& value)   { return value; }
template<> inline Value Value::from(const float& value)         { return Value((long)value); }
template<> inline Value Value::from(const misc::duration&value) { return Value(value.count()); }
template<> inline Value             Value::to() const { return *this; }
template<> inline long              Value::to() const { return number; }
template<> inline std::string       Value::to() const { return casted(STRING).string; }
template<> inline const Value&      Value::to() const { return *this; }
template<> inline misc::duration    Value::to() const { return misc::milliseconds(casted(NUMBER).number); }


class Properties : public std::map<std::string,Value>
{
    typedef std::map<std::string,Value> Parent;
public:
    using Parent::Parent;

    mapped_type operator[] ( const key_type& key ) const
    {
        auto it = find(key);
        if ( it == end() )
            return mapped_type();
        return it->second;
    }
    mapped_type& operator[] ( const key_type& key )
    {
        auto it = find(key);
        if ( it == end() )
            it = insert(it,value_type(key,mapped_type()));
        return it->second;
    }
};

} // namespace misc

#endif // GAME_VALUE_HPP
