/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <random>
#include "misc/math_utils.hpp"

namespace math {

static std::random_device random_device;  ///< RNG device

long random()
{
    return std::uniform_int_distribution<int>()(random_device);
}

long random(long max)
{
    return random(0,max);
}

long random(long min, long max)
{
    return std::uniform_int_distribution<int>(min,max)(random_device);
}

} // namespace math
