/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "misc/color.hpp"
#include "misc/string_utils.hpp"
#include <map>
#include <iomanip>

using namespace misc;

static std::map<std::string,Color> named_colors = {
    { "white",  {255,255,255} },
    { "gray",   {128,128,128} },
	{ "silver", {192,192,192} },
	{ "black",  {  0,  0,  0} },
	{ "maroon", {128,  0,  0} },
	{ "red",    {255,  0,  0} },
	{ "purple", {128,  0,128} },
	{ "fuchsia",{255,  0,255} },
	{ "green",  {  0,128,  0} },
	{ "lime",   {  0,255,  0} },
	{ "olive",  {128,128,  0} },
	{ "yellow", {255,255,  0} },
	{ "navy",   {  0,  0,128} },
	{ "blue",   {  0,  0,255} },
	{ "teal",   {  0,128,128} },
	{ "aqua",   {  0,255,255} },
	{ "orange", {255,165,  0} }
};

Color::Color(const std::string& name)
{
    auto it = named_colors.find(name);
    if ( it != named_colors.end() )
    {
        *this = it->second;
        return;
    }
    if ( name.empty() )
        return;

    int index =  name[0] == '#' ? 1 : 0;
    int size = name.length()-index >= 6 ? 2 : 1 ;
    int chans = ( name.length() - index ) / size;
    if ( chans < 3 )
        return;
    r = misc::xston<int>(name.substr(index,size));
    g = misc::xston<int>(name.substr(index+size,size));
    b = misc::xston<int>(name.substr(index+size*2,size));

    if ( size == 1 )
    {
        r = (r<<4)|r;
        g = (g<<4)|g;
        b = (b<<4)|b;
    }

    if ( chans > 3 )
    {
        a = misc::xston<int>(name.substr(index+size*3,size));
        if ( size == 1 )
            a = (a<<4)|a;
    }
}


std::string Color::name() const
{
    using namespace std;
    ostringstream ss;
    ss << '#' << hex << setfill('0') << setw(2)
       << (int)r << setw(2) << (int)g << setw(2) << (int)b << setw(2) << (int)a;
    return ss.str();
}

std::string Color::html_name() const
{
    using namespace std;
    ostringstream ss;
    ss << '#' << hex << setfill('0') << setw(2)
       << (int)r << setw(2) << (int)g << setw(2) << (int)b;
    return ss.str();
}


namespace misc {

std::istream& operator>>( std::istream& is, Color& col)
{
    std::string s;
    if ( is >> s )
        col = Color(s);
    return is;
}


std::ostream& operator<<( std::ostream& os, const Color& col)
{
    using namespace std;
    auto f = os.flags();
    char c = os.fill();
    os << '#' << hex << setfill('0')
        << setw(2) << (int)col.r
        << setw(2) << (int)col.g
        << setw(2) << (int)col.b
        << setw(2) << (int)col.a;
    os.fill(c);
    os.setf(f);
    return os;
}

} // namespace misc
