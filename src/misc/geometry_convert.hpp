/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef GEOMETRY_CONVERT_HPP
#define GEOMETRY_CONVERT_HPP

#include "misc/geometry.hpp"
#include <SFML/Graphics.hpp>




namespace geo {

/**
 * \brief Conversions from sf:: stuff
 */
namespace conv {

    template<class T>
        inline Rectangle rect( const sf::Rect<T>& rect ) { return Rectangle(rect.left,rect.top,rect.width,rect.height); }
    inline sf::FloatRect rect( const Rectangle& rect ) { return {rect.x,rect.y,rect.width,rect.height}; }
    inline Size size(const sf::Vector2u& sz ) { return Size(sz.x,sz.y); }
    inline sf::Vector2f size(const Size& sz ) { return sf::Vector2f(sz.width,sz.height); }
    template<class T>
        inline Point point(const sf::Vector2<T>& sz ) { return Point(sz.x,sz.y); }
    inline sf::Vector2f point(const Point& sz ) { return sf::Vector2f(sz.x,sz.y); }

} // namespace sf


} // namespace geo

#endif // GEOMETRY_CONVERT_HPP
