/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef PTREE_UTILS_HPP
#define PTREE_UTILS_HPP

#include <boost/property_tree/ptree.hpp>
#include <map>
#include <string>
#include "misc/game_value.hpp"

/**
 * \brief Namespace with miscellaneous utilities
 */
namespace misc {

/**
 * \brief Extract \<properties>\<property name=.. value=... />\</properties>
 * \param pt                Property tree
 * \param [out] properties  Property map to be overwritten
 */
inline void get_properties(const boost::property_tree::ptree& pt,
                           misc::Properties& properties)
{
    auto props = pt.get_child_optional("properties");
    if ( !props )
        return;
    for ( auto p : *props )
        if ( p.first == "property" )
        {
            std::string name = p.second.get("<xmlattr>.name","");
            std::string value = p.second.get("<xmlattr>.value", "");
            if ( !name.empty() )
                properties[name] = misc::Value::create(value);
        }
}

/**
 * \brief Extract \<properties>\<property name=.. value=... />\</properties>
 * \param pt                Property tree
 */
inline misc::Properties get_properties( const boost::property_tree::ptree& pt)
{
    misc::Properties p;
    get_properties(pt,p);
    return p;
}

/**
 * \brief Convert a tree to a map with keys in the form parent.child
 * \param pt                Property tree
 * \param [out] properties  Property map to be overwritten
 * \param prefix            Prefix to prepend to property keys
 */
inline void tree_to_properties(const boost::property_tree::ptree& pt,
                               misc::Properties& properties,
                               const std::string& prefix="")
{

    for ( auto p : pt )
    {
        if ( p.first == "<xmlattr>" )
            tree_to_properties(p.second,properties,prefix);
        else if ( p.first != "<xmlcomment>" )
        {
            std::string name = prefix+p.first;
            auto v = p.second.get_value_optional<std::string>();
            if ( v )
                properties[name] = misc::Value::create(*v);
            tree_to_properties(p.second,properties,name+'.');
        }
    }
}


/**
 * \brief Convert a tree to a map with keys in the form parent.child
 * \param pt                Property tree
 */
inline misc::Properties tree_to_properties( const boost::property_tree::ptree& pt)
{
    misc::Properties p;
    tree_to_properties(pt,p);
    return p;
}

} // namepsace misc
#endif // PTREE_UTILS_HPP
