/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef MISC_COLOR_HPP
#define MISC_COLOR_HPP
#include <cstdint>
#include <string>
#include <iostream>
#include <SFML/Graphics/Color.hpp>

namespace misc {

/**
 * \brief Class to handle colors
 */
struct Color
{
    typedef uint8_t Component;
    Component r = 0; ///< Red
    Component g = 0; ///< Green
    Component b = 0; ///< Blue
    Component a = 255; ///< Alpha

    Color() : r(0),g(0),b(0),a(255) {}
    Color(Component r, Component g, Component b, Component a = 255) : r(r),g(g),b(b),a(a) {}
    Color(const sf::Color& c) :r(c.r),g(c.g),b(c.b),a(c.a) {}
    /**
     * \brief Construct the color from a html name
     */
    Color(const std::string& name);

    operator sf::Color() const { return sf::Color(r,g,b,a); }

    /**
     * \brief Color name
     */
    std::string name() const;

    /**
     * \brief Strictly html-like name (ie without the alpha specification)
     */
    std::string html_name() const;

};

std::istream& operator>>( std::istream& is, misc::Color& col);
std::ostream& operator<<( std::ostream& os, const misc::Color& col);

} // namespace misc



#endif // MISC_COLOR_HPP
