/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef LOGGER_HPP
#define LOGGER_HPP

#include "config.hpp"
#include <string>
#include <iostream>
#include "misc/string_utils.hpp"
#include "misc/game_value.hpp"
#include <iomanip>

/**
 * \brief Class to log messages
 */
class Logger
{
public:
    static const int log_level    = LOG_LEVEL;      ///< Show only messages with this level or higher
    static const bool show_errors = true;   ///< Whether to display error messages

    /**
     * \brief Simple function to log a single string
     */
    static void log(const std::string& s, int level = 1)
    {
        Logger(level) << s;
    }
	/**
	 * \brief Log a message in standard error
	 */
    static void error(const std::string& s)
    {
        Logger(1,true) << s;
    }
    /**
     * \brief Create an error stream
     */
    static Logger err() { return Logger(1,true); }

    int level;  ///< Message level, will display only if not smaller than \c log_level
    bool stderr;///< Whether to log to stderr
    bool spaces;///< Whether to put spaces between items and a newline at the end

    explicit Logger(int level=1, bool stderr=false, bool spaces = true)
        : level(level), stderr(stderr), spaces(spaces)
    {}

    ~Logger()
    {
        if ( spaces && visible() ) stream() << "\n";
    }

    /**
     * \brief Get standard stream
     */
    std::ostream& stream() const
    {
        return stderr ? std::clog : std::cout;
    }

    /**
     * \brief whether it will be displayed
     */
    bool visible() const
    {
        return level >= log_level || (stderr && show_errors);
    }

    /**
     * \brief Display an item
     * \note \c T needs to have an overloaded << stream operator
     */
    template<class T>
        const Logger& operator<< ( const T& it ) const
        {
            if ( visible() )
            {
                stream() << it;
                if ( spaces ) stream() << ' ';
            }
            return *this;
        }

    /**
     * \brief Apply manipulators without space output
     */
    const Logger& operator<< ( std::ostream& (*pf) (std::ostream&) ) const
    {
        if ( visible() )
            stream() << pf;
        return *this;
    }


    /**
     * \brief Output properties as a table
     * \param properties    Properties to be printed
     * \param value_length  Number of character to show for the property value
     */
    void table(const misc::Properties& properties, int value_length = 32 ) const
    {
        if ( visible() )
        {
            for ( const auto& p : properties )
                stream() << std::setw(value_length) << std::setfill(' ') << std::right
                    << ((std::string)p.second).substr(0,value_length)
                    << "  " << p.first << std::endl;
        }
    }
};

#endif // LOGGER_HPP
