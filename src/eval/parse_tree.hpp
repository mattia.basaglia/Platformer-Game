/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef PARSE_TREE_HPP
#define PARSE_TREE_HPP

#include "misc/game_value.hpp"
#include "context.hpp"
#include <memory>

namespace eval {

struct Tree_Value : public misc::Value
{
    bool breaks = false;

    Tree_Value() {};

    Tree_Value ( const misc::Value& v, bool breaks = false )
        : misc::Value(v), breaks(breaks) {}

    Tree_Value(bool v) : misc::Value(v), breaks(false) {}

    using misc::Value::operator long;
};

/**
 * \brief Parse tree node
 *
 * A tree with concrete nodes is created by the parse during the compilation phase.
 * A parse tree node can execute part of an script.
 */
class Parse_Tree
{
public:
    virtual ~Parse_Tree() {}
    Parse_Tree() {}
    Parse_Tree(const Parse_Tree&) = delete;

    /**
     * \brief Evaluate the tree
     * \param context Context providing variables and functions
     * \todo Remove context const-ness
     */
    virtual Tree_Value evaluate(const Context& context) const = 0;

    /**
     * \brief Get lvalue reference (if any)
     */
    virtual std::string lvalue(const Context& context) const { return std::string(); }

    /**
     * \brief Get if the node can be a lvalue
     */
    virtual bool is_lvalue() const { return false; }

    /**
     * \brief Virtual copy constructor (deep copy)
     */
    virtual Parse_Tree* clone() const =0;
};

/**
 * \brief Namespace for all the children of Parse_Tree
 */
namespace tree {

class Value : public Parse_Tree
{
protected:
    misc::Value value;
public:
    explicit Value(const misc::Value& value) : value(value) {}
    Tree_Value evaluate(const Context&) const override { return value; }
    Value* clone() const override { return new tree::Value(value); }
};

class Unary_Operator : public Parse_Tree
{
protected:
    Parse_Tree* operand;
public:
    explicit Unary_Operator(Parse_Tree* operand) : operand(operand) {}
    ~Unary_Operator() { delete operand; }
};

class Binary_Operator : public Parse_Tree
{
protected:
    Parse_Tree* left;
    Parse_Tree* right;
public:
    Binary_Operator(Parse_Tree* left, Parse_Tree* right) : left(left), right(right) {}
    ~Binary_Operator() { delete left; delete right; }
};

class Unary_Minus : public Unary_Operator
{
public:
    using Unary_Operator::Unary_Operator;
    Tree_Value evaluate(const Context& context) const override { return -operand->evaluate(context); }
    Unary_Minus* clone() const override { return new Unary_Minus(operand->clone()); }
};

class Logical_Not : public Unary_Operator
{
public:
    using Unary_Operator::Unary_Operator;
    Tree_Value evaluate(const Context& context) const override { return !operand->evaluate(context); }
    Logical_Not* clone() const override { return new Logical_Not(operand->clone()); }
};

class Multiply : public Binary_Operator
{
public:
    using Binary_Operator::Binary_Operator;
    Multiply* clone() const override { return new Multiply(left->clone(),right->clone()); }
    Tree_Value evaluate(const Context& context) const override
    {
        return left->evaluate(context) * right->evaluate(context);
    }
};

class Divide : public Binary_Operator
{
public:
    using Binary_Operator::Binary_Operator;
    Divide* clone() const override { return new Divide(left->clone(),right->clone()); }
    Tree_Value evaluate(const Context& context) const override
    {
        return left->evaluate(context) / right->evaluate(context);
    }
};

class Modulo : public Binary_Operator
{
public:
    using Binary_Operator::Binary_Operator;
    Modulo* clone() const override { return new Modulo(left->clone(),right->clone()); }
    Tree_Value evaluate(const Context& context) const override
    {
        return left->evaluate(context) % right->evaluate(context);
    }
};

class Add : public Binary_Operator
{
public:
    using Binary_Operator::Binary_Operator;
    Add* clone() const override { return new Add(left->clone(),right->clone()); }
    Tree_Value evaluate(const Context& context) const override
    {
        return left->evaluate(context) + right->evaluate(context);
    }
};

class Subtract : public Binary_Operator
{
public:
    using Binary_Operator::Binary_Operator;
    Subtract* clone() const override { return new Subtract(left->clone(),right->clone()); }
    Tree_Value evaluate(const Context& context) const override
    {
        return left->evaluate(context) - right->evaluate(context);
    }
};

class Cmp_Gt : public Binary_Operator
{
public:
    using Binary_Operator::Binary_Operator;
    Cmp_Gt* clone() const override { return new Cmp_Gt(left->clone(),right->clone()); }
    Tree_Value evaluate(const Context& context) const override
    {
        return left->evaluate(context) > right->evaluate(context);
    }
};

class Cmp_Ge : public Binary_Operator
{
public:
    using Binary_Operator::Binary_Operator;
    Cmp_Ge* clone() const override { return new Cmp_Ge(left->clone(),right->clone()); }
    Tree_Value evaluate(const Context& context) const override
    {
        return left->evaluate(context) >= right->evaluate(context);
    }
};

class Cmp_Eq : public Binary_Operator
{
public:
    using Binary_Operator::Binary_Operator;
    Cmp_Eq* clone() const override { return new Cmp_Eq(left->clone(),right->clone()); }
    Tree_Value evaluate(const Context& context) const override
    {
        return left->evaluate(context) == right->evaluate(context);
    }
};

class Cmp_Ne : public Binary_Operator
{
public:
    using Binary_Operator::Binary_Operator;
    Cmp_Ne* clone() const override { return new Cmp_Ne(left->clone(),right->clone()); }
    Tree_Value evaluate(const Context& context) const override
    {
        return left->evaluate(context) != right->evaluate(context);
    }
};

class Cmp_Le : public Binary_Operator
{
public:
    using Binary_Operator::Binary_Operator;
    Cmp_Le* clone() const override { return new Cmp_Le(left->clone(),right->clone()); }
    Tree_Value evaluate(const Context& context) const override
    {
        return left->evaluate(context) <= right->evaluate(context);
    }
};

class Cmp_Lt : public Binary_Operator
{
public:
    using Binary_Operator::Binary_Operator;
    Cmp_Lt* clone() const override { return new Cmp_Lt(left->clone(),right->clone()); }
    Tree_Value evaluate(const Context& context) const override
    {
        return left->evaluate(context) < right->evaluate(context);
    }
};

class Logical_And : public Binary_Operator
{
public:
    using Binary_Operator::Binary_Operator;
    Logical_And* clone() const override { return new Logical_And(left->clone(),right->clone()); }
    Tree_Value evaluate(const Context& context) const override
    {
        return left->evaluate(context) && right->evaluate(context);
    }
};

class Logical_Or : public Binary_Operator
{
public:
    using Binary_Operator::Binary_Operator;
    Logical_Or* clone() const override { return new Logical_Or(left->clone(),right->clone()); }
    Tree_Value evaluate(const Context& context) const override
    {
        return left->evaluate(context) || right->evaluate(context);
    }
};


class Bit_And : public Binary_Operator
{
public:
    using Binary_Operator::Binary_Operator;
    Bit_And* clone() const override { return new Bit_And(left->clone(),right->clone()); }
    Tree_Value evaluate(const Context& context) const override
    {
        return ((long)left->evaluate(context) ) & ((long)right->evaluate(context));
    }
};

class Bit_Or : public Binary_Operator
{
public:
    using Binary_Operator::Binary_Operator;
    Bit_Or* clone() const override { return new Bit_Or(left->clone(),right->clone()); }
    Tree_Value evaluate(const Context& context) const override
    {
        return ((long)left->evaluate(context) ) | ((long)right->evaluate(context));
    }
};

class Bit_Xor : public Binary_Operator
{
public:
    using Binary_Operator::Binary_Operator;
    Bit_Xor* clone() const override { return new Bit_Xor(left->clone(),right->clone()); }
    Tree_Value evaluate(const Context& context) const override
    {
        return ((long)left->evaluate(context) ) ^ ((long)right->evaluate(context));
    }
};
class Bit_Not : public Unary_Operator
{
public:
    using Unary_Operator::Unary_Operator;
    Bit_Not* clone() const override { return new Bit_Not(operand->clone()); }
    Tree_Value evaluate(const Context& context) const override
    {
        return ~(long)operand->evaluate(context);
    }
};

class If : public Parse_Tree
{
protected:
    Parse_Tree* condition;
    Parse_Tree* if_true;
    Parse_Tree* if_false;
public:
    If( Parse_Tree* condition, Parse_Tree* if_true,Parse_Tree* if_false )
        : condition(condition), if_true(if_true), if_false(if_false) {}
    ~If() { delete condition; delete if_true; delete if_false; }
    If* clone() const override { return new If(condition->clone(),if_true->clone(),if_false->clone()); }

    Tree_Value evaluate(const Context& context) const override
    {
        return condition->evaluate(context) ? if_true->evaluate(context) : if_false->evaluate(context) ;
    }

    std::string lvalue(const Context& context) const override
    {
        return condition->evaluate(context) ? if_true->lvalue(context) : if_false->lvalue(context) ;
    }

    bool is_lvalue() const override { return if_true->is_lvalue() && if_false->is_lvalue(); }
};

class While : public Parse_Tree
{
protected:
    Parse_Tree* condition;
    Parse_Tree* body;
public:
    While( Parse_Tree* condition, Parse_Tree* body )
        : condition(condition), body(body) {}
    ~While() { delete condition; delete body; }
    While* clone() const override { return new While(condition->clone(),body->clone()); }

    Tree_Value evaluate(const Context& context) const override
    {
        /// \todo timeout (?)
        Tree_Value v;
        while ( condition->evaluate(context) )
        {
            v = body->evaluate(context);
            if ( v.breaks )
                break;
        }
        return v;
    }
};

class Assign : public Binary_Operator
{
public:
    using Binary_Operator::Binary_Operator;
    Assign* clone() const override { return new Assign(left->clone(),right->clone()); }
    Tree_Value evaluate(const Context& context) const override
    {
        std::string lval = left->lvalue(context);
        if ( !lval.empty() )
        {
            context.set_property(lval,right->evaluate(context));
            return context.get_property(lval);
        }
        return Tree_Value();
    }

    std::string lvalue(const Context& context) const override
    {
        return left->lvalue(context);
    }
    bool is_lvalue() const override { return true; }
};

class Variable : public Parse_Tree
{
    std::string identifier;

public:
    Variable ( const std::string& identifier ) : identifier(identifier) {}
    Variable* clone() const override { return new Variable(identifier); }
    Tree_Value evaluate(const Context& context) const override
    {
        return context.get_property(identifier);
    }
    std::string lvalue(const Context&) const override { return identifier; }
    bool is_lvalue() const override { return true; }

};

class Sequence : public Parse_Tree
{
    std::vector<Parse_Tree*> items;
public:
    Sequence() {}
    explicit Sequence(Parse_Tree* item) : items(1,item) {}
    Sequence(const std::vector<Parse_Tree*>& items) : items(items) {}
    Sequence* clone() const override
    {
        std::vector<Parse_Tree*> oitems;
        std::transform(items.begin(),items.end(),
                std::inserter(oitems, oitems.begin()),
            [](Parse_Tree* i){return i->clone();});
        return new Sequence(oitems);
    }

    ~Sequence()
    {
        for ( auto it : items )
            delete it;
    }

    Tree_Value evaluate(const Context& context) const override
    {
        Tree_Value v;
        for ( auto it : items )
        {
            v = it->evaluate(context);
            if ( v.breaks )
                break;
        }
        return v;
    }

    std::vector<misc::Value> evaluate_vector(const Context& context) const
    {
        std::vector<misc::Value> v;
        for ( auto it : items )
            v.push_back(it->evaluate(context));
        return v;
    }

    void push ( Parse_Tree* item )
    {
        items.push_back(item);
    }
};

class Function : public Parse_Tree
{
    std::string identifier;
    Sequence* args;

public:
    explicit Function( const std::string& identifier, Sequence* args) : identifier(identifier), args(args) {}
    ~Function() { delete args; }
    Function* clone() const override { return new Function(identifier, args->clone()); }

    Tree_Value evaluate(const Context& context) const override
    {
        return context.call_function(identifier,args->evaluate_vector(context));
    }

};

class Function_Declaration : public Parse_Tree
{
public:
    typedef std::pair<std::string,std::shared_ptr<Parse_Tree>>  Argument;
    typedef std::vector<Argument>                               Parameters;

private:
    std::string                 identifier;
    Parameters                  parameters;
    std::shared_ptr<Parse_Tree> body;

public:
    Function_Declaration(const std::string& identifier, const Parameters& parameters, Parse_Tree* body)
        : identifier(identifier), parameters(parameters), body(body) {}

    Function_Declaration* clone() const override
    {
        Parameters pars;
        std::transform(parameters.begin(),parameters.end(),
                std::inserter(pars, pars.begin()),
            [](const Argument& i)
            {
               return Argument(i.first,std::shared_ptr<Parse_Tree>(i.second->clone()));
            });
        return new Function_Declaration(identifier,pars,body->clone());
    }

    Tree_Value evaluate(const Context& context) const override
    {
        Parameters copy_parameters = parameters;
        std::shared_ptr<Parse_Tree> copy_body = body;
        const_cast<Context&>(context).set_function(identifier,
        [copy_parameters,copy_body](const Context& ctx,const Context::Arguments& args)
        {
            Context inner_context(&ctx);
            for ( unsigned i = 0; i < copy_parameters.size(); i++ )
            {
                if ( i < args.size() )
                    inner_context.set_temporary(copy_parameters[i].first,args[i]);
                else if ( copy_parameters[i].second )
                    inner_context.set_temporary(copy_parameters[i].first,copy_parameters[i].second->evaluate(inner_context));
                else
                    inner_context.set_temporary(copy_parameters[i].first,misc::Value());
            }
            return copy_body->evaluate(inner_context);
        });
        return misc::Value();
    }
};


class Return : public Unary_Operator
{
public:
    using Unary_Operator::Unary_Operator;
    Return* clone() const override { return new Return(operand->clone()); }

    Tree_Value evaluate(const Context& context) const override
    {
        Tree_Value v = operand->evaluate(context);
        v.breaks = true;
        return v;
    }
};


}} // namespace eval::tree
#endif // PARSE_TREE_HPP
