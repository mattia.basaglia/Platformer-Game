/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef EVAL_LIBRARY_HPP
#define EVAL_LIBRARY_HPP

#include "context.hpp"

namespace eval {

/**
 * \brief A collection of functions
 */
struct Library
{
    std::map<std::string,Context::Function> functions;

    Library( const std::map<std::string,Context::Function>& functions )
        : functions ( functions ) {}
};

/**
 * \brief Namespace for built-in scripting libraries
 */
namespace lib {
    extern const Library string_accessors;  ///< Access properties and methods as strings
    extern const Library array;             ///< Array functionality
    extern const Library math;              ///< Mathematical functions
    extern const Library console;           ///< Functions that print to the program output
    extern const Library cast;              ///< Ensure that a value has a specific type
}

} // namespace eval

#endif // EVAL_LIBRARY_HPP
