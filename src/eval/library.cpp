/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "library.hpp"
#include "misc/math_utils.hpp"
#include "misc/logger.hpp"

namespace eval{
namespace lib {
/**
 * \par get
 * Get property by string
 * \code get("foo","bar") := foo.bar \endcode
 *
 * \par set
 * Set property by string
 * \code set("foo","bar",123) := foo.bar = 123 \endcode
 *
 * \par call
 * Call function by string
 * \code call("foo","bar",123) := foo("bar",123) \endcode
 */
const Library string_accessors = Library({

	{"get", [](const eval::Context& context, const eval::Context::Arguments& args)
    {
        return args.size() >= 1 ?  context.get_property(misc::implode(".",args)) : misc::Value();
    }},
	{"set", [](const eval::Context& context, const eval::Context::Arguments& args) -> misc::Value
    {
        if ( args.size() < 2 )
            return misc::Value();
        std::string id = misc::implode(".",args.begin(),args.end()-1);
        context.set_property(id,args.back());
        return context.get_property(id);
    }},
	{"call",[](const eval::Context& context, const eval::Context::Arguments& args) -> misc::Value
    {
        return args.empty() ? misc::Value() :
            context.call_function((std::string)args[0],eval::Context::Arguments(args.begin()+1,args.end()));
    }}
});



/** \var eval::lib::array
 * \par array
 * build an array (csv) from parameters
 * \code array("foo","bar",123) := "foo,bar,123" \endcode
 *
 * \par array_at
 * get an element from a csv array
 * \code array_at("foo,bar",1) := "bar" \endcode
 *
 * \par array_size
 * get the size of a csv array
 * \code array_size("foo,bar") := 2 \endcode
 *
 * \par array_find
 * get the index of an element iside a csv array (uses string comparison)
 * \code array_find("foo,bar","bar") := 1 \endcode
 */
const Library array = Library({
	{"array",[](const eval::Context& context, const eval::Context::Arguments& args) -> misc::Value
    {
        /// \todo escape commas
        return misc::implode(",",args);
    }},
	{"array_at",[](const eval::Context& context, const eval::Context::Arguments& args) -> misc::Value
    {
        if ( args.size() < 2 )
            return misc::Value();
        /// \todo un-escape commas
        auto vect = misc::explode<misc::Value>(",",(std::string)args[0]);
        unsigned long index = (long)args[1];
        if ( index > vect.size() )
            return misc::Value();
        return vect[index];
    }},
	{"array_size",[](const eval::Context& context, const eval::Context::Arguments& args) -> misc::Value
    {
        if ( args.empty() )
            return misc::Value();
        /// \todo un-escape commas
        return (long)misc::explode<misc::Value>(",",(std::string)args[0]).size();
    }},
	{"array_find",[](const eval::Context& context, const eval::Context::Arguments& args) -> misc::Value
    {
        if ( args.size() < 2 )
            return misc::Value();
        /// \todo un-escape commas
        auto vect = misc::explode<misc::Value>(",",(std::string)args[0]);
        return std::find(vect.begin(),vect.end(),(std::string)args[1]) - vect.begin();
    }}
});

/**
 * \par random()
 * Get a random integer
 * \par random(n)
 * Get a random number between 0 and n (inclusive)
 * \par random(a,b)
 * Get a random number between a and b (inclusive)
 */
const Library math = Library({

	{"random", [](const eval::Context& context, const eval::Context::Arguments& args) -> misc::Value
    {
        if ( args.size() == 0 )
            return math::random();
        if ( args.size() == 1 )
            return math::random((long)args[0]);
        return math::random((long)args[0],(long)args[1]);
    }},
});

/**
 * \par help
 * Show a list of free functions
 *
 * \par print
 * Print something to stdout
 * \code print("foo","bar") := (stdout) "foo bar" \endcode
 */
const Library console = Library({

	{"help", [](const eval::Context& context, const eval::Context::Arguments&) -> misc::Value {
        Logger(16) << "Available commands:" << context.functions();
        return misc::Value();
    }},
    {"print", [](const eval::Context& context, const eval::Context::Arguments& args) -> misc::Value {
        Logger log(16);
        for ( const auto& v : args )
            log << v;
        return misc::Value();
    }}
});

/**
 * \par number
 * Cast to number
 * \code number("123") := 123 \endcode
 *
 * \par string
 * Cast to string
 * \code string(123) := "123" \endcode
 */
const Library cast = Library({

	{"number", [](const eval::Context& context, const eval::Context::Arguments& args) -> misc::Value {
        return args.empty() ? misc::Value(0) : (long)args[0];
    }},
    {"string", [](const eval::Context& context, const eval::Context::Arguments& args) -> misc::Value {
        return args.empty() ? misc::Value("") : (std::string)args[0];
    }}
});

}} // namespace eval::lib
