/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "context.hpp"
#include "library.hpp"

using namespace eval;

misc::Value Context::get_property(const std::string& identifier) const
{
    std::string root, item;
    if ( misc::split_identifier(identifier,root,item) != std::string::npos )
    {
        auto it = real_objects.find(root);
        if ( it != real_objects.end() && it->second )
            return it->second->get_property(item);

        auto fit = fake_objects.find(root);
        if ( fit != fake_objects.end() && fit->second )
            return (*fit->second)[item];

        if ( other_objects )
            if ( auto o = other_objects->find_child(root) )
                return o->get_property(item);
    }

    if ( parent && !temporaries.count(identifier) )
        return parent->get_property(identifier);

    return temporaries[identifier];

}

bool Context::set_property_internal(const std::string& identifier, const misc::Value& v) const
{
    std::string root, item;
    if ( misc::split_identifier(identifier,root,item) != std::string::npos )
    {
        auto it = real_objects.find(root);
        if ( it != real_objects.end() )
        {
            it->second->set_property(item,v);
            return true;
        }

        auto fit = fake_objects.find(root);
        if ( fit != fake_objects.end() && fit->second )
        {
            (*fit->second)[item] = v;
            return true;
        }

        if ( other_objects )
            if ( auto o = other_objects->find_child(root) )
            {
                o->set_property(item,v);
                return true;
            }
    }

    if ( parent )
        return parent->set_property_internal(identifier,v);

    return false;
}

void Context::set_property(const std::string& identifier, const misc::Value& v) const
{
    if ( ! set_property_internal(identifier,v) )
        temporaries[identifier] = v;
}

misc::Value Context::call_function(const std::string& identifier, const std::vector<misc::Value>& args) const
{
    return call_function_internal(identifier,args,*this);
}

misc::Value Context::call_function_internal(const std::string& identifier, const Arguments& args, const Context& child) const
{
    std::string root, item;
    if ( misc::split_identifier(identifier,root,item) != std::string::npos )
    {
        auto it = real_objects.find(root);
        if ( it != real_objects.end() )
            return it->second->call_method(item,args);

        if ( other_objects )
            if ( auto o = other_objects->find_child(root) )
                return o->call_method(item,args);
    }

    auto fit = free_functions.find(identifier);
    if ( fit != free_functions.end() )
        return fit->second(child,args);

    if ( parent )
        return parent->call_function_internal(identifier,args,child);

    return misc::Value();
}

void Context::set_object(const std::string& name, misc::Mirror* object)
{
    real_objects[name] = object;
}
void Context::set_object(const std::string& name, misc::Properties& props)
{
    fake_objects[name] = &props;
}
void Context::set_temporary(const std::string& name, const misc::Value & val)
{
    temporaries[name] = val;
}

void Context::set_object_source(misc::Object_Source* src)
{
    other_objects = src;
}

void Context::set_function(const std::string& name, const Function& function)
{
    free_functions[name] = function;
}

std::vector<std::string> Context::functions() const
{
    std::vector<std::string> funs;
    funs.reserve(free_functions.size());
    for ( auto f : free_functions )
        funs.push_back(f.first);

    if ( parent )
    {
        auto pfuns = parent->functions();
        funs.insert( funs.end(), pfuns.begin(), pfuns.end() );
    }

    std::sort(funs.begin(),funs.end());
    auto last = std::unique(funs.begin(), funs.end());
    funs.erase(last, funs.end());

    return funs;
}


void Context::install ( const Library& lib )
{
    for ( auto f : lib.functions )
        set_function(f.first,f.second);
}

misc::Properties Context::object_properties(const std::string&identifier) const
{

    if ( auto o = find_child(identifier) )
        return o->get_properties();

    auto fit = fake_objects.find(identifier);
    if ( fit != fake_objects.end() && fit->second )
        return *fit->second;

    if ( parent )
        return parent->object_properties(identifier);

    return misc::Properties();
}


misc::Mirror* Context::find_child(const std::string&name) const
{
    auto it = real_objects.find(name);
    if ( it != real_objects.end() )
        return it->second;

    if ( other_objects )
        if ( auto o = other_objects->find_child(name) )
            return o;

    if ( parent )
        return parent->find_child(name);

    return nullptr;
}

misc::Mirror* Context::find_nested_object(std::string name) const
{
    std::string root, item;
    misc::split_identifier(name,root,item);
    if ( misc::Object_Source* src = dynamic_cast<misc::Object_Source*>(find_child(root)) )
        return src->find_child(item);

    return nullptr;
}
