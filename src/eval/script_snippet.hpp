/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef SCRIPT_SNIPPET_PROPERTY_HPP
#define SCRIPT_SNIPPET_PROPERTY_HPP

#include "parser.hpp"
#include "misc/game_value.hpp"
#include <memory>
#include <fstream>

namespace eval {

/**
 * \brief A firendly class to make Object_With_Property have a compiled script snippet
 */
class Script_Snippet
{
private:
    std::string       source;
    std::shared_ptr<eval::Parse_Tree> tree = nullptr;

public:
    Script_Snippet() = default;
    explicit Script_Snippet(const std::string& s) { set_source(s); }

    /**
     * \brief Set script source
     * \param s Code to be compiled
     */
    void set_source(const std::string&s)
    {
        tree.reset(eval::Parser().compile(source = s));
    }

    /**
     * \brief Get the script source
     *
     * This may be the actual code or the name of the file from which it was loaded
     */
    const std::string& get_source() const
    {
        return source;
    }

    /**
     * \brief Load source from a file
     * \param s (Absolute) File name
     */
    void load_file(const std::string&s)
    {
        std::ifstream stream(s);
        tree.reset(eval::Parser().compile(stream));
        source = s;
    }

    /**
     * \brief Whether it doesn't have any code to be executed
     */
    bool empty() const
    {
        return !tree || source.empty();
    }

    /**
     * \brief Execute the script and return the result
     */
    misc::Value evaluate(const eval::Context& context) const
    {
        return tree ? tree->evaluate(context) : misc::Value();
    }
};

} // namespace eval

namespace misc {
template<> inline Value Value::from(const eval::Script_Snippet& s) { return Value(s.get_source()); }
template<> inline eval::Script_Snippet Value::to() const { return eval::Script_Snippet((std::string)*this); }
} // misc

#endif // SCRIPT_SNIPPET_PROPERTY_HPP
