/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef CONTEXT_HPP
#define CONTEXT_HPP

#include "misc/game_value.hpp"
#include "misc/mirror.hpp"
#include "misc/object_source.hpp"

namespace eval {

class Library;

/**
 * \brief Context for parse tree evaluation
 */
class Context
{
public:
    typedef std::vector<misc::Value> Arguments;
    typedef std::function<misc::Value(const Context&,const Arguments&)> Function;

private:
    std::map<std::string,misc::Mirror*>     real_objects;   ///< Collection of objects
    std::map<std::string,misc::Properties*> fake_objects;   ///< Named collections of properties
    const Context*                          parent =nullptr;///< Parent context
    mutable misc::Properties                temporaries;    ///< Temporary variables, specific to this context
    misc::Object_Source*                    other_objects=nullptr; ///< Sources of objects
    std::map<std::string,Function>          free_functions; ///< Free functions

    /**
     * \brief Set a property (but not a temporary)
     * \return \b true if the property has been set
     */
    bool set_property_internal(const std::string& identifier, const misc::Value& v) const;


    misc::Value call_function_internal(const std::string& identifier, const Arguments& args, const Context& child) const;

public:
    explicit Context(const Context* parent) : parent(parent) {}
    Context() {}

    /**
     * \brief Get a value found in context, it searches internal objects,
     *      variables, and Game_Manager values
     */
    misc::Value get_property(const std::string& identifier) const;
    /**
     * \brief Set a value found context, it searches internal objects,
     *      variables, and Game_Manager values
     */
    void set_property(const std::string& identifier, const misc::Value& v) const;

    /**
     * \brief Call a method or function
     */
    misc::Value call_function(const std::string& identifier, const Arguments& args) const;
    /**
     * \brief Bind an identifier to an object, identifiers following the
     *  pattern name.something will be searched in the object matching \c name
     */
    void set_object(const std::string& name, misc::Mirror* object);
    /**
     * \brief Bind an identifier to a set of values, identifiers following the
     *  pattern name.something will be searched in the object matching \c name
     */
    void set_object(const std::string& name, misc::Properties& props);
    /**
     * \brief Set a context-specific variable
     */
    void set_temporary(const std::string& name, const misc::Value & val);

    /**
     * \brief Set an object that provides objects and properties not explicitly added with set_object
     */
    void set_object_source(misc::Object_Source* src);

    /**
     * \brief Set a named function
     */
    void set_function(const std::string& name, const Function& function);
    /**
     * \brief Get a list of free function names (not object methods)
     */
    std::vector<std::string> functions() const;

    /**
     * \brief Install a library
     */
    void install ( const Library& lib );

    /**
     * \brief Get all the properties properties of a named object declared in this context
     */
    misc::Properties object_properties(const std::string&name) const;

    /**
     * \brief Find a real object with the given name
     */
    misc::Mirror* find_child(const std::string&name) const;

    /**
     * \brief Find an object which may be found inside an object source
     */
    misc::Mirror* find_nested_object(std::string name) const;
};


} // namespace eval
#endif // CONTEXT_HPP
