/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#include "parser.hpp"
#include <cctype>
#include "misc/string_utils.hpp"

using namespace eval;
using namespace eval::tree;

//{ (Lexer)

std::vector<std::string> Parser::keywords = {
    "else",
    "for",
    "function",
    "if",
    "while",
    "return"
};

void Parser::lexA_id()
{
    lookahead.type = Token::IDENTIFIER;
    char c;
    while(true)
    {
        c = stream.get();
        if ( !stream )
            break;
        if ( std::isalnum(c) || c == '_' || c == '.' )
            lookahead.lexeme += c;
        else
            break;
    }
    if ( stream )
        stream.putback(c);

    if ( lookahead.lexeme == "or" )
    {
        lookahead.lexeme = "|";
        lookahead.type = Token::OPERATOR_BOOL;
    }
    else if ( lookahead.lexeme == "and" )
    {
        lookahead.lexeme = "&";
        lookahead.type = Token::OPERATOR_BOOL;
    }
    else if ( lookahead.lexeme == "true" )
    {
        lookahead.type = Token::VALUE;
        lookahead.value = 1;
    }
    else if ( lookahead.lexeme == "false" )
    {
        lookahead.type = Token::VALUE;
        lookahead.value = 0l;
    }
    else if ( misc::contains(keywords,lookahead.lexeme) )
        lookahead.type = Token::KEYWORD;
}
void Parser::lexA_oper_arith()
{
    char c = stream.get();
    lookahead.lexeme += c;
    lookahead.type = Token::OPERATOR_ARITH;

    if ( stream.peek() == '=' )
        lexA_oper_assign_arith();
    else if ( stream.peek() == c )
    {
        lookahead.lexeme += stream.get();
        lookahead.type = Token::OPERATOR_INCREMENT;
    }
}
void Parser::lexA_zero()
{
    lookahead.type = Token::VALUE;
    lookahead.lexeme += (char) stream.get();
    char c = stream.peek();
    if ( c == 'x' || c == 'X' )
    {
        stream.ignore();
        lookahead.lexeme = "";
        lexA_xnumber();
    }
    else if ( std::isdigit(c) )
        lexA_number();
    else
        lookahead.value = 0l;
}
void Parser::lexA_xnumber()
{
    lookahead.type = Token::VALUE;
    char c;
    while(true)
    {
        c = stream.get();
        if ( !stream )
            break;
        if ( std::isxdigit(c) )
            lookahead.lexeme += c;
        else
            break;
    }
    if ( stream )
        stream.putback(c);
    std::stringstream ss(lookahead.lexeme);
    ss.setf(std::ios_base::hex, std::ios_base::basefield);
    long n = 0;
    ss >> n;
    lookahead.value = n;
    lookahead.lexeme = "0x"+lookahead.lexeme;
}

void Parser::lexA_number()
{
    lookahead.type = Token::VALUE;
    char c;
    while(true)
    {
        c = stream.get();
        if ( !stream )
            break;
        if ( std::isdigit(c) )
            lookahead.lexeme += c;
        else
            break;
    }
    if ( stream )
        stream.putback(c);
    lookahead.value = misc::ston<long>(lookahead.lexeme);
}
void Parser::lexA_oper_assign_arith()
{
    lookahead.lexeme += stream.get();
    lookahead.type = Token::OPERATOR_ASSIGN;
}
void Parser::lexA_oper_mult()
{
    lookahead.lexeme += stream.get();
    lookahead.type = Token::OPERATOR_MULT;
    if ( stream.peek() == '=' )
        lexA_oper_assign_arith();
}

void Parser::lexA_slash()
{
    lookahead.lexeme += stream.get();
    lookahead.type = Token::OPERATOR_MULT;
    switch ( stream.peek() )
    {
        case '=':
            return lexA_oper_assign_arith();
        case '*':
            stream.ignore();
            return lex_comment();
        case '/':
            stream.ignore();
            return lex_comment_line();
    }
}

void Parser::lex_comment()
{
    char c;
    while(true)
    {
        c = stream.get();
        if ( !stream )
            break;
        if ( c == '*' && stream.peek() == '/' )
        {
            stream.ignore();
            return lex();
        }
    }
}

void Parser::lex_comment_line()
{
    char c;
    while(true)
    {
        c = stream.get();
        if ( !stream )
            break;
        if ( c == '\n' )
            return lex();
    }
}

void Parser::lexA_oper_cmp_ltgt()
{
    lookahead.lexeme += stream.get();
    lookahead.type = Token::OPERATOR_CMP;
    if ( stream.peek() == '=' )
        lexA_oper_cmp();

}
void Parser::lexA_oper_cmp()
{
    lookahead.lexeme += stream.get();
    lookahead.type = Token::OPERATOR_CMP;
}
void Parser::lexA_oper_bang()
{
    lookahead.lexeme += stream.get();
    lookahead.type = Token::OPERATOR_UNARY;
    if ( stream.peek() == '=' )
        lexA_oper_cmp();
}
void Parser::lexA_oper_assign()
{
    lookahead.lexeme += stream.get();
    lookahead.type = Token::OPERATOR_ASSIGN;
    if ( stream.peek() == '=' )
        lexA_oper_cmp();
}
void Parser::lex_string(char delimiter)
{
    char c = stream.get(); // skip first "
    while(true)
    {
        c = stream.get();
        if ( !stream || c == delimiter )
            break;
        if ( c == '\\' )
        {
            c = stream.get();
            if ( !stream )
                break;
            if ( c == 'n' )
                c = '\n';
        }
        lookahead.lexeme += c;
    }
    lookahead.type = Token::VALUE;
    lookahead.value = lookahead.lexeme;

}


void Parser::lex()
{
    lookahead.type = Token::OTHER;
    lookahead.lexeme = "";
    lookahead.value = 0l;
    char c = stream.peek();

    while ( std::isspace(c) )
    {
        stream.ignore(1);
        c = stream.peek();
    }
    if ( !stream || c == std::char_traits<char>::eof() )
    {
        lookahead.type = Token::END_OF_FILE;
        return;
    }

    if ( std::isalpha(c) )
        return lexA_id();
    else if ( c == '0' )
        return lexA_zero();
    else if ( std::isdigit(c) )
        return lexA_number();
    else switch ( c )
    {
        case '"':
        case '\'':
            return lex_string(c);
        case '+': case '-':
            return lexA_oper_arith();
        case '*': case '%':
            return lexA_oper_mult();
        case '/':
            return lexA_slash();
        case '!':
            return lexA_oper_bang();
        case '=':
            return lexA_oper_assign();
        case '&': case '|':
            lookahead.lexeme += stream.get();
            lookahead.type = Token::OPERATOR_BIT;
            if ( stream.peek() == c )
            {
                stream.ignore(1);
                lookahead.type = Token::OPERATOR_BOOL;
            }
            return;
        case '^':
        case '~':
            lookahead.lexeme += stream.get();
            lookahead.type = Token::OPERATOR_BIT;
            return;
        case '<': case '>':
            return lexA_oper_cmp_ltgt();
        case '{':
        case '[':
        case '(':
            stream.ignore();
            lookahead.type = Token::OTHER;
            lookahead.lexeme += '(';
            return;
        case '}':
        case ']':
        case ')':
            stream.ignore();
            lookahead.type = Token::OTHER;
            lookahead.lexeme += ')';
            return;
        case ',':
        default:
            lookahead.type = Token::OTHER;
            lookahead.lexeme += stream.get();
            return;
    }
}
//}

Parse_Tree* Parser::symbol(const std::string& identifier)
{
    if ( lookahead.type == Token::OTHER && lookahead.lexeme == "(" )
    {
        Sequence* args = new Sequence;
        lex();
        Token tok = lookahead;
        if ( tok.type == Token::OTHER && tok.lexeme == ")" )
            lex();
        else
            while ( tok.type != Token::END_OF_FILE && (tok.type != Token::OTHER || tok.lexeme != ")") )
            {
                args->push(expression());
                tok = lookahead;
                lex();
            }
        return new Function(identifier,args);
    }
    return new Variable(identifier);
}

Parse_Tree* Parser::primary()
{
    Token tok = lookahead;
    lex();
    switch ( tok.type )
    {
        case Token::IDENTIFIER:
            return symbol(tok.lexeme);
        case Token::OPERATOR_ARITH:
        {
            Parse_Tree* v = expression();
            return tok.lexeme == "-" ? new Unary_Minus(v) : v;
        }
        case Token::OPERATOR_UNARY:
            return new Logical_Not ( primary() );
        case Token::OPERATOR_BIT:
            if ( tok.lexeme == "~" )
                return new Bit_Not ( primary() );
            break;
        case Token::OTHER:
            if ( tok.lexeme == "(" )
            {
                Parse_Tree* v = block();
                lex(); // )
                return v;
            }
            break;
        case Token::KEYWORD:
            if ( tok.lexeme == "return" )
                return new Return(expression());
            break;
        default:
            break;
    }
    return new Value(tok.value);
}
Parse_Tree* Parser::multiplication()
{
    Parse_Tree* v = primary();
    while ( lookahead.type == Token::OPERATOR_MULT && !lookahead.lexeme.empty() )
    {
        char oper = lookahead.lexeme[0];
        lex();
        if ( oper == '*' )
            v = new Multiply ( v, primary() );
        else if ( oper == '/' )
            v = new Divide ( v, primary() );
        else if ( oper == '%' )
            v = new Modulo ( v, primary() );
        else
            break;
    }
    return v;
}
Parse_Tree* Parser::arithmetic()
{
    Parse_Tree* v = multiplication();
    while ( lookahead.type == Token::OPERATOR_ARITH && !lookahead.lexeme.empty() )
    {
        char oper = lookahead.lexeme[0];
        lex();
        if ( oper == '+' )
            v = new Add ( v, multiplication() );
        else if ( oper == '-' )
            v = new Subtract( v, multiplication() );
        else
            break;
    }
    return v;
}
Parse_Tree* Parser::bitwise()
{
    Parse_Tree* v = arithmetic();
    while ( lookahead.type == Token::OPERATOR_BIT && !lookahead.lexeme.empty() )
    {
        char oper = lookahead.lexeme[0];
        lex();
        if ( oper == '&' )
            v = new Bit_And ( v, arithmetic() );
        else if ( oper == '|' )
            v = new Bit_Or( v, arithmetic() );
        else if ( oper == '^' )
            v = new Bit_Xor( v, arithmetic() );
        /// \todo bit shift
        else
            break;
    }
    return v;
}
Parse_Tree* Parser::comparison()
{
    Parse_Tree* v = bitwise();
    while ( lookahead.type == Token::OPERATOR_CMP )
    {
        std::string oper = lookahead.lexeme;
        lex();
        if ( oper == ">" )
            v = new Cmp_Gt ( v, bitwise() );
        else if ( oper == ">=" )
            v = new Cmp_Ge ( v, bitwise() );
        else if ( oper == "<" )
            v = new Cmp_Lt ( v, bitwise() );
        else if ( oper == "<=" )
            v = new Cmp_Le ( v, bitwise() );
        else if ( oper == "==" )
            v = new Cmp_Eq ( v, bitwise() );
        else if ( oper == "!=" )
            v = new Cmp_Ne ( v, bitwise() );
        else
            break;
    }
    return v;
}
Parse_Tree* Parser::boolean()
{
    Parse_Tree* v = comparison();
    while ( lookahead.type == Token::OPERATOR_BOOL && !lookahead.lexeme.empty() )
    {
        char oper = lookahead.lexeme[0];
        lex();
        if ( oper == '&' )
            v = new Logical_And( v, comparison());
        else if ( oper == '|' )
            v = new Logical_Or( v, comparison());
        else
            break;
    }
    return v;
}

Parse_Tree* Parser::expression()
{
    Parse_Tree* v = boolean();

    if ( lookahead.type == Token::OTHER && lookahead.lexeme == "?" )
    {
        lex();
        Parse_Tree* if_true = boolean();
        Parse_Tree* if_false;
        if ( lookahead.type == Token::OTHER && lookahead.lexeme == ":" )
        {
            lex();
            if_false = boolean();
        }
        else
            if_false = new Value(misc::Value());

        return new If(v,if_true,if_false);
    }

    return v;
}

Parse_Tree* Parser::assignment()
{
    Parse_Tree* lvalue = expression();

    if ( lvalue->is_lvalue() && !lookahead.lexeme.empty() )
    {
        if ( lookahead.type == Token::OPERATOR_ASSIGN )
        {
            char oper = lookahead.lexeme[0];
            lex();
            Parse_Tree* rvalue = expression();
            if ( oper == '+' )
                rvalue = new Add ( lvalue->clone(), rvalue );
            else if ( oper == '-' )
                rvalue = new Subtract ( lvalue->clone(), rvalue );
            else if ( oper == '*' )
                rvalue = new Multiply ( lvalue->clone(), rvalue );
            else if ( oper == '/' )
                rvalue = new Divide ( lvalue->clone(), rvalue );
            else if ( oper == '%' )
                rvalue = new Modulo ( lvalue->clone(), rvalue );
            /// \todo |= &= <<= >>=

            return new Assign(lvalue,rvalue);
        }
        else if ( lookahead.type == Token::OPERATOR_INCREMENT )
        {
            char oper = lookahead.lexeme[0];
            lex();
            if ( oper == '-' )
                return new Assign(lvalue, new Subtract ( lvalue->clone(), new tree::Value(1) ) );
            return new Assign(lvalue, new Add ( lvalue->clone(), new tree::Value(1) ) );
        }
    }

    return lvalue;
}

Parse_Tree* Parser::statement()
{
    if ( lookahead.type == Token::KEYWORD && lookahead.lexeme == "if" )
    {
        lex(); // remove if
        lex(); // remove (
        Parse_Tree* condition = boolean();
        lex(); // remove )
        Parse_Tree* if_true = statement();
        Parse_Tree* if_false;
        if ( lookahead.type == Token::KEYWORD && lookahead.lexeme == "else" )
        {
            lex(); // remove else
            if_false = statement();
        }
        else
            if_false = new Value(misc::Value());

        return new If(condition,if_true,if_false);
    }
    else if ( lookahead.type == Token::KEYWORD && lookahead.lexeme == "while" )
    {
        lex(); // remove while
        lex(); // remove (
        Parse_Tree* condition = boolean();
        lex(); // remove )
        return new While(condition,statement());
    }
    else if ( lookahead.type == Token::KEYWORD && lookahead.lexeme == "for" )
    {
        lex(); // remove for
        lex(); // remove (
        Parse_Tree* start = assignment();
        lex(); // remove ;
        Parse_Tree* condition = boolean();
        lex(); // remove ;
        Parse_Tree* increment = assignment();
        lex(); // remove )
        return new Sequence( { start, new While(condition, new Sequence({statement(),increment}) ) } );
    }
    else if ( lookahead.type == Token::KEYWORD && lookahead.lexeme == "function" )
    {
        lex(); // remove function
        std::string id = lookahead.lexeme;
        lex(); // remove id
        lex(); // remove (

        Function_Declaration::Parameters params;
        Token tok = lookahead;
        if ( tok.type == Token::OTHER && tok.lexeme == ")" )
            lex();
        else
            while ( tok.type != Token::END_OF_FILE && (tok.type != Token::OTHER || tok.lexeme != ")") )
            {
                Function_Declaration::Argument param;
                param.first = lookahead.lexeme;
                param.second = nullptr;
                lex(); // remove param name
                if ( tok.type == Token::OPERATOR_ASSIGN )
                {
                    lex(); // remove =
                    param.second.reset(expression());
                }
                params.push_back(param);
                tok = lookahead;
                lex();
            }

        return new Function_Declaration(id,params,statement());

    }

    Parse_Tree* v = assignment();
    if ( lookahead.type == Token::OTHER && (lookahead.lexeme == "," || lookahead.lexeme == ";") )
        lex();
    return v;
}

Parse_Tree* Parser::block()
{
    Sequence* v = new Sequence();
    while ( lookahead.type != Token::END_OF_FILE && !lookahead.lexeme.empty() &&
           !(lookahead.type == Token::OTHER && lookahead.lexeme == ")" )
           )
    {
        v->push(statement());
    }
    return v;
}


Parse_Tree* Parser::parse()
{
    lex();
    return block();
}

misc::Value Parser::evaluate(const std::string& expression,const Context& context)
{
    Parse_Tree* tree = compile(expression);
    misc::Value val = tree->evaluate(context);
    delete tree;
    return val;
}

Parse_Tree* Parser::compile(const std::string& expression)
{
    std::istringstream string_stream(expression);
    stream.rdbuf(string_stream.rdbuf());
    return parse();
}


Parse_Tree* Parser::compile(std::istream& input)
{
    stream.rdbuf(input.rdbuf());
    return parse();
}
