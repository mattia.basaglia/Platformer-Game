/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#ifndef EXPRESSION_EVALUATOR_HPP
#define EXPRESSION_EVALUATOR_HPP

#include <string>
#include <map>
#include <sstream>
#include "misc/game_value.hpp"
#include "parse_tree.hpp"

/**
 * \brief Namespace for parsing and executig scripts
 */
namespace eval {

/**
 * \brief A lexer token
 */
struct Token
{

    enum Token_Type
    {
        IDENTIFIER,
        VALUE,
        OPERATOR_CMP,
        OPERATOR_BIT,
        OPERATOR_BOOL,
        OPERATOR_ARITH,
        OPERATOR_MULT,
        OPERATOR_ASSIGN,
        OPERATOR_UNARY,
        OPERATOR_INCREMENT,
        KEYWORD,
        OTHER,
        END_OF_FILE // No eof because of C library :-(
    } type;

    misc::Value value;

    std::string lexeme;

};

/**
 * \brief Compiles and evaluates game scripts
 * \verbatim
 *  BLOCK   ::= STMT  | BLOCK ; STMT | BLOCK , STMT
 *  STMT    ::= ASSIGN| STMT_if | STMT_while | STMT_for
 *  STMT_if ::= if ( BOOL ) ASSIGN | if ( BOOL ) ASSIGN else STMT
 *  STMT_while::= while ( BOOL ) ASSIGN
 *  STMT_for::= for ( ASSIGN ; BOOL ; ASSIGN ) ASSIGN
 *  ASSIGN  ::= EXPR  | id    assign   EXPR
 *  EXPR    ::= BOOL  | BOOL ? BOOL : BOOL
 *  BOOL    ::= CMP   | CMP   bool_op  EXPR
 *  CMP     ::= BIT   | BIT   cmp_op   CMP
 *  BIT     ::= ARITH | ARITH bit_op   CMP
 *  ARITH   ::= MULT  | MULT  arith_op ARITH
 *  MULT    ::= PRIM  | PRIM  mult_op  MULT
 *  PRIM    ::= val | SYM | ( STMT ) | arith_op PRIM | ! PRIM
 *  SYM     ::= id | id ( ARGS ) | id ()
 *  ARGS    ::= EXPR  | EXPR , ARGS
 *  cmp_op  ::= < | > | == | >= | <= | !=
 *  bool_op ::= && | and | "||" | or
 *  bit_op  ::= & | "|" | ^ | ~
 *  arith_op::= + | -
 *  mult_op ::= * | / | %
 *  assign  ::= = | += | -= | *= | /= | %=
 *  id      ::= [a-z][_a-z0-9]*(\.[a-z][_a-z0-9]*)*
 *  val     ::= -?[0-9]+ | "true" | "false" | "([^"]|\\.)*"
 *  ( )     ::= {[( )]}
 * \endverbatim
 */
class Parser
{
    std::istream stream;
    Token lookahead;
    static std::vector<std::string> keywords;


/**
 * \name Lexer
 * \{
 */
//{
    /**
     * \par Input
     *      [a-z]
     * \par Loop
     *      [_.a-z0-9]
     * \par Type
     *  IDENTIFIER
     */
    void lexA_id();
    /**
     * \par Input
     *      +-
     * \par Output
     *      = to lexA_oper_assign_arith
     * \par Type
     *      OPERATOR_ARITH
     */
    void lexA_oper_arith();
    /**
     * \par Input
     *      0
     * \par Output
     *      x to lexA_xnumber
     *      [0-9] to lexA_number
     * \par Type
     *      VALUE
     */
    void lexA_zero();
    /**
     * \par Input
     *      0x
     * \par Loop
     *      [0-9a-f]
     * \par Type
     *      VALUE
     */
    void lexA_xnumber();
    /**
     * \par Input
     *      [1-9]
     * \par Loop
     *      [0-9]
     * \par Type
     *      VALUE
     */
    void lexA_number();
    /**
     * \par Input
     *      = (preceded by + - * / %)
     * \par Type
     *      OPERATOR_ASSIGN
     */
    void lexA_oper_assign_arith();
    /**
     * \par Input
     *      * %
     * \par Output
     *      = to lexA_oper_assign_arith
     * \par Type
     *      OPERATOR_MULT
     */
    void lexA_oper_mult();
    /**
     * \par Input
     *      < >
     * \par Ouput
     *      = to lexA_oper_cmp
     * \par Type
     *      OPERATOR_CMP
     */
    void lexA_oper_cmp_ltgt();
    /**
     * \par Input
     *      = (preceded by ! < > =)
     * \par Type
     *      OPERATOR_CMP
     */
    void lexA_oper_cmp();
    /**
     * \par Input
     *      !
     * \par Ouput
     *      = to lexA_oper_cmp
     * \par Type
     *      OPERATOR_UNARY
     */
    void lexA_oper_bang();
    /**
     * \par Input
     *      =
     * \par Ouput
     *      = to lexA_oper_cmp
     * \par Type
     *      OPERATOR_ASSIGN
     */
    void lexA_oper_assign();
    /**
     * \par Input
     *      delimiter ( " or ' )
     * \par Loop
     *      Anything but delimiter
     * \par Ouput
     *      delimiter
     * \par Type
     *      STRING
     */
    void lex_string(char delimiter);

    /**
     * \par Input
     *      \
     * \par Output
     *      * = to lexA_oper_assign_arith
     *      * * to lex_comment
     *      * / ti lex_comment_line
     * \par Type
     *      OPERATOR_MULT
     */
    void lexA_slash();

    /**
     * \par Input
     *      / *
     * \par Output
     *      * / to lex
     * \par Loop
     *      Anything
     */
    void lex_comment();

    /**
     * \par Input
     *      //
     * \par Output
     *      newline
     * \par Loop
     *      Anything
     */
    void lex_comment_line();


    /**
     * \brief Get next token
     */
    void lex();
//}
/// \}

/**
 * \name Parser
 * \{
 */
    Parse_Tree* symbol(const std::string& identifier);
    Parse_Tree* primary();
    Parse_Tree* multiplication();
    Parse_Tree* arithmetic();
    Parse_Tree* bitwise();
    Parse_Tree* comparison();
    Parse_Tree* boolean();
    Parse_Tree* expression();
    Parse_Tree* assignment();
    Parse_Tree* statement();
    Parse_Tree* block();

    Parse_Tree* parse();
/// \}

public:
    Parser() : stream(nullptr) {}

    misc::Value evaluate(const std::string& expression, const Context& context);
    Parse_Tree* compile(const std::string& expression);
    Parse_Tree* compile(std::istream& input);
};

} // namespace eval

#endif // EXPRESSION_EVALUATOR_HPP
