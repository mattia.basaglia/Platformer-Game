cmake_minimum_required (VERSION 2.6)

# Config
project (PonyPlatform)

set (VERSION_MAJOR 0)
set (VERSION_MINOR 1)
set (MAINTAINER "Mattia Basaglia <mattia.basaglia@gmail.com>")

set (NAME_NICE "Pony Platformer")
set (NAME_LOWER "pony_platformer")
set (EXECUTABLE ${NAME_LOWER})

set (INSTALL_DATADIR "share/games/${NAME_LOWER}")
set (INSTALL_BINDIR "games/bin")

set(LOG_LEVEL 1)

# Flags
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Wall -pedantic")
include_directories("${CMAKE_SOURCE_DIR}/src")

# Enable Debug by default, can be changed with -D CMAKE_BUILD_TYPE=Release
if(CMAKE_BUILD_TYPE STREQUAL "")
    set(CMAKE_BUILD_TYPE Debug)
    set(DEBUG 1)
else()
    set(DEBUG 0)
endif()


# Sources
file(GLOB_RECURSE SOURCES src/*.cpp)
file(GLOB_RECURSE HEADERS src/*.hpp)
add_executable(${EXECUTABLE} ${SOURCES})


# Check Libraries
set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/lib/cmake_modules" ${CMAKE_MODULE_PATH})
find_package(SFML 2 REQUIRED system window graphics network audio)
if(SFML_FOUND)
  include_directories(${SFML_INCLUDE_DIR})
  target_link_libraries(${EXECUTABLE} ${SFML_LIBRARIES})
endif()

find_package(Boost 1.54 COMPONENTS filesystem system REQUIRED)
if(Boost_FOUND)
  include_directories(${Boost_INCLUDE_DIRS})
  target_link_libraries(${EXECUTABLE} ${Boost_LIBRARIES})
  include(CheckIncludeFileCXX)
  CHECK_INCLUDE_FILE_CXX("boost/property_tree/ptree.hpp" BOOST_PTREE)
  if (NOT BOOST_PTREE )
    message(SEND_ERROR "Missing required boost libraries")
    unset(BOOST_PTREE CACHE)
  endif()
endif()

# Check C++11
if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang" OR CMAKE_COMPILER_IS_GNUCXX)
    include(CheckCXXCompilerFlag)
    check_cxx_compiler_flag(--std=c++11 STD_CXX11)
    if(STD_CXX11)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} --std=c++11")
        set(HAS_STD_CXX11 1)
    else()
        set(HAS_STD_CXX11 0)
    endif()
endif()


# Generate files
configure_file (
  "${PROJECT_SOURCE_DIR}/src/config.hpp.in"
  "${PROJECT_BINARY_DIR}/config.hpp"
)
include_directories("${PROJECT_BINARY_DIR}")

find_package(Doxygen)
if(DOXYGEN_FOUND)
	configure_file(${CMAKE_SOURCE_DIR}/Doxyfile.in ${PROJECT_BINARY_DIR}/Doxyfile)
	add_custom_target(doc
		${DOXYGEN_EXECUTABLE} ${PROJECT_BINARY_DIR}/Doxyfile
		DEPENDS ${CMAKE_SOURCE_DIR}/Doxyfile.in ${SOURCES} ${HEADERS}
		WORKING_DIRECTORY ${PROJECT_BINARY_DIR}
		COMMENT "Generating Doxygen Documentation" VERBATIM
	)
	add_custom_target(doc-view
		xdg-open doxy/html/index.html
		DEPENDS doc
		COMMENT "Showing Doxygen documentation"
	)
endif(DOXYGEN_FOUND)

add_custom_target(cppcheck
    cppcheck --enable=all -q -v --template="{file}:{line}: {severity}: [1m{message}[0m" --suppress=missingIncludeSystem ${CMAKE_SOURCE_DIR}/src/
    VERBATIM
)

# Installation
install (DIRECTORY data DESTINATION ${INSTALL_DATADIR})
install (TARGETS ${EXECUTABLE} DESTINATION ${INSTALL_BINDIR})


include (InstallRequiredSystemLibraries)
set (CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/COPYING")
set (CPACK_PACKAGE_VERSION_MAJOR "${VERSION_MAJOR}")
set (CPACK_PACKAGE_VERSION_MINOR "${VERSION_MINOR}")
set (CPACK_PACKAGE_CONTACT "${MAINTAINER}")
include (CPack)



add_custom_target(test
	echo ${CMAKE_INSTALL_PREFIX}
	)
# Notes

# Create a C::B project:
# cmake . -G "CodeBlocks - Unix Makefiles"

# Create a package DEB, RPM, etc (see man cpack)
# cpack -C CPackConfig.cmake -G DEB
# to make a source package, use CPackSourceConfig.cmake

# To change installation directory
# cmake -D CMAKE_INSTALL_PREFIX=/path
# or
# make install DESTDIR=/path
