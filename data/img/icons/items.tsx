<?xml version="1.0" encoding="UTF-8"?>
<tileset name="items" tilewidth="64" tileheight="48">
 <tile id="0">
  <image width="16" height="16" source="coin-copper.png"/>
 </tile>
 <tile id="1">
  <image width="16" height="16" source="coin-silver.png"/>
 </tile>
 <tile id="2">
  <image width="16" height="16" source="coin-gold.png"/>
 </tile>
 <tile id="3">
  <image width="16" height="16" source="apple.png"/>
 </tile>
 <tile id="4">
  <image width="16" height="16" source="apple-zap.png"/>
 </tile>
 <tile id="5">
  <image width="16" height="16" source="apple-rotten.png"/>
 </tile>
 <tile id="6">
  <image width="32" height="32" source="scroll.png"/>
 </tile>
 <tile id="7">
  <image width="64" height="48" source="twilight.png"/>
 </tile>
 <tile id="8">
  <image width="48" height="48" source="mouse.png"/>
 </tile>
 <tile id="9">
  <image width="48" height="48" source="squirrel.png"/>
 </tile>
 <tile id="10">
  <image width="48" height="48" source="bunny.png"/>
 </tile>
 <tile id="11">
  <image width="48" height="48" source="beaver.png"/>
 </tile>
 <tile id="12">
  <image width="48" height="48" source="butterfly.png"/>
 </tile>
 <tile id="13">
  <image width="48" height="48" source="parasprite.png"/>
 </tile>
 <tile id="14">
  <image width="16" height="16" source="../items/crate-small.png"/>
 </tile>
 <tile id="15">
  <image width="32" height="32" source="../items/crate-medium.png"/>
 </tile>
 <tile id="16">
  <image width="48" height="30" source="../items/crate-large.png"/>
 </tile>
 <tile id="17">
  <image width="16" height="16" source="poison-joke.png"/>
 </tile>
</tileset>
