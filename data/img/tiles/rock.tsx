<?xml version="1.0" encoding="UTF-8"?>
<tileset name="rock" tilewidth="16" tileheight="16">
 <image source="rock.png" width="192" height="192"/>
 <terraintypes>
  <terrain name="back rock" tile="-1"/>
  <terrain name="ground" tile="-1"/>
 </terraintypes>
 <tile id="0" terrain=",,,0"/>
 <tile id="1" terrain=",,0,0"/>
 <tile id="2" terrain=",,0,"/>
 <tile id="3" terrain="0,0,0,"/>
 <tile id="4" terrain="0,0,,0"/>
 <tile id="12" terrain=",0,,0"/>
 <tile id="13" terrain="0,0,0,0"/>
 <tile id="14" terrain="0,,0,"/>
 <tile id="15" terrain="0,,0,0"/>
 <tile id="16" terrain=",0,0,0"/>
 <tile id="24" terrain=",0,,"/>
 <tile id="25" terrain="0,0,,"/>
 <tile id="26" terrain="0,,,"/>
 <tile id="27" terrain=",0,0,"/>
 <tile id="28" terrain="0,,,0"/>
 <tile id="39" terrain="1,1,1,"/>
 <tile id="40" terrain="1,1,,1"/>
 <tile id="41" terrain="1,,1,"/>
 <tile id="42" terrain=",1,,1"/>
 <tile id="48" terrain=",1,,1"/>
 <tile id="49" terrain="1,1,1,1"/>
 <tile id="50" terrain="1,,1,"/>
 <tile id="51" terrain="1,,1,1"/>
 <tile id="52" terrain=",1,1,1"/>
 <tile id="53" terrain="1,,1,"/>
 <tile id="54" terrain=",1,,1"/>
 <tile id="55" terrain=",,1,1"/>
 <tile id="60" terrain=",1,,"/>
 <tile id="61" terrain="1,1,,"/>
 <tile id="62" terrain="1,,,"/>
 <tile id="63" terrain="1,1,,"/>
 <tile id="64" terrain="1,1,,"/>
 <tile id="65" terrain="1,1,,"/>
 <tile id="66" terrain=",,,1"/>
 <tile id="67" terrain=",,1,"/>
</tileset>
