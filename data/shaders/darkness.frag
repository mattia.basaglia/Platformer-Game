#ifdef DOXYGEN
/**
 * \file
 * \ingroup shader
 */
/// \ingroup shader
/// \{
namespace shader {
/**
 * \brief Show all black except within a certain area
 */
struct darkness  {
#endif
/*
Example setup:
on_load =
    video.shader("shaders/darkness.frag");
    video.shader_param("shaders/darkness.frag","outer_radius",256);
    video.shader_param("shaders/darkness.frag","inner_radius",64);
script.code =
    video.shader_param("shaders/darkness.frag","light",
        video.screen_x(object.center.x,object.center.y),
        video.height-video.screen_y(object.center.x,object.center.y)
    );
*/

#define TAU 6.2831853071795864769252
#define PI  3.1415926535897932384626

vec4 hue_rgb(float hue)
{
    return clamp(vec4(
        abs(hue * 6.0 - 3.0) - 1.0,
        2.0 - abs(hue * 6.0 - 2.0),
        2.0 - abs(hue * 6.0 - 4.0),
        1.0
    ),0.0,1.0);
}

vec4 hsv2rgb(vec4 hsv)
{
    return vec4( ( hue_rgb(hsv.x).rgb - 1.0 ) * hsv.y * hsv.z + hsv.z, hsv.w );
}

vec4 rgb2hsv(vec4 rgb)
{
    float v = max(rgb.x,max(rgb.y,rgb.z));
    float c = v - min(rgb.x,min(rgb.y,rgb.z));
    float h = mod(-atan(-sqrt(3.0)*(rgb.y-rgb.z)/2.0,(2.0*rgb.x-rgb.y-rgb.z)/2.0) / TAU, 1.0);
    return vec4(h,v > 0.0 ? c / v : 0.0 ,v, rgb.w);
}


uniform sampler2D texture; ///< SFML input

/**
 *
 * x = 2*(object.center.x-camera.x+camera.width/2)
 * y = 2*(camera.y-twilight.center.y+camera.height/2)
 * (2* means that the camera is showing a 2x zoom)
 */
uniform vec2 light; ///< Color used to multiply the input

uniform float outer_radius; ///< Outer light radius, everything past this is black
uniform float inner_radius; ///< Inner light radius, everything within this is fully lit
uniform float shades;       ///< Number of discrete blackness shades (use 0.0 for a continuous blending)

void main()
{

    vec4 hsv = rgb2hsv(texture2D(texture, gl_TexCoord[0].xy));

    float pix_dist = distance(gl_FragCoord.xy,light);
    float mult = clamp((pix_dist-inner_radius)/(outer_radius-inner_radius),0.0,1.0);

    if ( shades > 0.0 )
        mult = floor ( mult*shades+0.5 ) / shades;

    hsv.z *= cos(mult*PI)/2.0+0.5;
    hsv.y *= 1.0-mult*mult;

    gl_FragColor = gl_Color * hsv2rgb(hsv);
}


#ifdef DOXYGEN
};}
/// \}
#endif

