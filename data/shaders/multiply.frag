#ifdef DOXYGEN
/**
 * \file
 * \ingroup shader
 */
/// \ingroup shader
/// \{
namespace shader {
/**
 * \brief Simple recoloring shader
 */
struct multiply  {
#endif

uniform sampler2D texture; ///< SFML input

uniform vec4 color; ///< Color used to multiply the input

void main()
{
    // lookup the pixel in the texture
    vec4 pixel = texture2D(texture, gl_TexCoord[0].xy);

    // multiply it by the color
    gl_FragColor = gl_Color * pixel * color;
}


#ifdef DOXYGEN
};}
/// \}
#endif
