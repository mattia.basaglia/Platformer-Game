#ifdef DOXYGEN
/**
 * \file
 * \ingroup shader
 */
/// \ingroup shader
/// \{
namespace shader {
/**
 * \brief Shift hue by the given amount
 */
struct hue_shift {
#endif

#define TAU 6.2831853071795864769252

vec4 hue_rgb(float hue)
{
    return clamp(vec4(
        abs(hue * 6.0 - 3.0) - 1.0,
        2.0 - abs(hue * 6.0 - 2.0),
        2.0 - abs(hue * 6.0 - 4.0),
        1.0
    ),0.0,1.0);
}

vec4 hsv2rgb(vec4 hsv)
{
    return vec4( ( hue_rgb(hsv.x).rgb - 1.0 ) * hsv.y * hsv.z + hsv.z, hsv.w );
}

vec4 rgb2hsv(vec4 rgb)
{
    float v = max(rgb.x,max(rgb.y,rgb.z));
    float c = v - min(rgb.x,min(rgb.y,rgb.z));
    float h = mod(-atan(-sqrt(3.0)*(rgb.y-rgb.z)/2.0,(2.0*rgb.x-rgb.y-rgb.z)/2.0) / TAU, 1.0);
    return vec4(h,v > 0.0 ? c / v : 0.0 ,v, rgb.w);
}

uniform sampler2D texture; ///< SFML input

uniform float offset; ///< hue offset in degrees

void main()
{
    vec4 pixel = texture2D(texture, gl_TexCoord[0].xy);

    vec4 color = rgb2hsv(pixel);
    color.x = mod(color.x+offset/360.0,1.0);

    gl_FragColor = gl_Color * hsv2rgb(color);
}


#ifdef DOXYGEN
};}
/// \}
#endif
