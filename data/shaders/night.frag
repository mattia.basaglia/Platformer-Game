#ifdef DOXYGEN
/**
 * \file
 * \ingroup shader
 */
/// \ingroup shader
/// \{
namespace shader {
/**
 * \brief Make darker colors even darker
 */
struct night {
#endif

uniform sampler2D texture; ///< SFML input

void main()
{
    // lookup the pixel in the texture
    vec4 pixel = texture2D(texture, gl_TexCoord[0].xy);

    float luma = 0.3*pixel.x+0.59*pixel.y+0.11*pixel.z;

    pixel.x *= luma;
    pixel.y *= luma;
    pixel.z *= luma;

    // multiply it by the color
    gl_FragColor = gl_Color * pixel;
}


#ifdef DOXYGEN
};}
/// \}
#endif


