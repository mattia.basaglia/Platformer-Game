/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#define BOOST_TEST_MODULE Test_Geometry
#include "../src/misc/geometry.hpp"
#include <boost/test/unit_test.hpp>

template<int n> int ensure_constexpr() { return n; }

using namespace geo::generic;

BOOST_AUTO_TEST_CASE( test_point )
{
	BOOST_CHECK_EQUAL( 
		ensure_constexpr<( (Point<int>(7,7)+-Point<int>(-1,1)) / 2 ).magnitude()>(),
		5
	);
} 

BOOST_AUTO_TEST_CASE( test_rectangle )
{
	BOOST_CHECK( !Rectangle<int>().is_valid() );
	
	Rectangle<int> r(10,20,100,100);
	
	BOOST_CHECK( r.top() == 20 );
	BOOST_CHECK( r.left() == 10 );
	BOOST_CHECK( r.right() == 100+10 );
	BOOST_CHECK( r.bottom() == 100+20 );
	
	BOOST_CHECK( r.top_left() == Point<int>(10,20) );
	BOOST_CHECK( r.top_right() == Point<int>(100+10,20) );
	BOOST_CHECK( r.bottom_left() == Point<int>(10,20+100) );
	BOOST_CHECK( r.bottom_right() == Point<int>(100+10,20+100) );
	BOOST_CHECK( r.center() == Point<int>(10+50,20+50) );
	
	BOOST_CHECK( r.area() == 100*100 );
	
	BOOST_CHECK( r.contains(30,40) );
	BOOST_CHECK( r.contains(10,40) );
	BOOST_CHECK( !r.contains(50,10) );
	
	BOOST_CHECK(r.nearest(Point<int>(40,40)) == Point<int>(40,40));
	BOOST_CHECK(r.nearest(Point<int>(40,400)) == Point<int>(40,120));
	
	
	Rectangle<int> r2(30,10,100,100);
	
	BOOST_CHECK(r.intersects(r2));
	BOOST_CHECK(!r.intersects(r.translated(100,0)));
	BOOST_CHECK(r.intersection(r2) == Rectangle<int>(Point<int>(30,20),Point<int>(110,110)) );
	BOOST_CHECK(!r.intersection(r.translated(100,0)).is_valid());
	
	BOOST_CHECK(r.united(r2) == Rectangle<int>(Point<int>(10,10),Point<int>(130,120)) );
	BOOST_CHECK(r.united(Rectangle<int>()) == r);
	BOOST_CHECK(Rectangle<int>().united(r) == r);

}

BOOST_AUTO_TEST_CASE( test_circle )
{
	Circle<float> c ( 0, 0, 100 );
	
	BOOST_CHECK ( c.contains(Point<float>(10,10)) );
	BOOST_CHECK ( c.contains(Point<float>(0,100)) );
	BOOST_CHECK ( !c.contains(Point<float>(1,100)) );
	
	BOOST_CHECK ( c.intersects(Circle<float>(10,10,20)) );
	BOOST_CHECK ( c.intersects(Circle<float>(10,10,200)) );
	BOOST_CHECK ( c.intersects(Circle<float>(130,0,50)) );
	BOOST_CHECK ( c.intersects(Circle<float>(130,0,30)) );
	BOOST_CHECK ( !c.intersects(Circle<float>(130,0,20)) );
	
	BOOST_CHECK ( c.contains(Rectangle<float>(-5,-5,10,10)) );
	BOOST_CHECK ( !c.contains(Rectangle<float>(-100,-100,100,100)) );
	BOOST_CHECK ( !c.contains(Rectangle<float>(95,0,10,10)) );
	
	BOOST_CHECK ( c.intersects(Rectangle<float>(-5,-5,10,10)) );
	BOOST_CHECK ( c.intersects(Rectangle<float>(-100,-100,100,100)) );
	BOOST_CHECK ( c.intersects(Rectangle<float>(95,0,10,10)) );
	BOOST_CHECK ( !c.intersects(Rectangle<float>(101,0,10,10)) );
	
}